 
<?php

class HFV_Slider_Block_Widget_Slider extends Mage_Core_Block_Text_List

{
    
    protected function _construct()
    {
		//Mage::log(__METHOD__);
        parent::_construct();
    }
    protected function _toHtml()
    {
        $this->setText('');
        foreach ($this->getSortedChildren() as $name) {
            $block = $this->getLayout()->getBlock($name);
            if (!$block) {
                Mage::throwException(Mage::helper('core')->__('Invalid block: %s', $name));
            }
            $this->addText($block->toHtml());
        }
        if(Mage::getSingleton('cms/page')->getIdentifier()=="home"){
	  return '<div id="slider-home">'. parent::_toHtml() .'</div>';
        }else{
	  return '';
        }
        //return parent::_toHtml();
    }
}
