<?php
/**
* Chweb Multi Layered Navigation 
* 
* @category     Chweb
* @package      Chweb_Mullayernav 
* @copyright    Copyright (c) 2014-2015 chweb (http://www.chaudharyweb.com/)
* @author       Chweb (Rajesh chaudhary)  
* @version      Release: 1.0.0
* @Class        Chweb_Mullayernav_Block_List   
*/
class Chweb_Mullayernav_Block_List extends Mage_Core_Block_Template {

    protected $_productCollection;
    protected $_module = 'catalog';

    /**
     * @return Mage_Catalog_Block_Product_List
     */
    public function getListBlock() {

        return $this->getChild('product_list');
    }

    public function setListOrders() {
        if ('catalogsearch' != $this->_module)
            return $this;

        $category = Mage::getSingleton('catalog/layer')
                ->getCurrentCategory();
        /* @var $category Mage_Catalog_Model_Category */
        $availableOrders = $category->getAvailableSortByOptions();
        unset($availableOrders['position']);
        $availableOrders = array_merge(array(
            'relevance' => $this->__('Relevance')
                ), $availableOrders);

        $this->getListBlock()
                ->setAvailableOrders($availableOrders)
                ->setDefaultDirection('desc')
                ->setSortBy('relevance');

        return $this;
    }

    /**
     * Set available view mode
     *
     * @return AdjustWare_Nav_Block_List
     
    public function setListModes() {

        $this->getListBlock()
                ->setModes(array(
                    'grid' => $this->__('Grid'),
                    'list' => $this->__('List'))
        );
        return $this;
    }
*/
    public function setIsSearchMode() {
        $this->_module = 'catalogsearch';
        return $this;
    }

    /**
     * Set All products collection
     *
     * @return AdjustWare_Nav_Block_List
     */
    public function setListCollection() {
        $this->getListBlock()
                ->setCollection($this->_getProductCollection());
        return $this;
    }

    protected function _toHtml() {
        $this->setListOrders();
        $this->setListModes();
        $this->setListCollection();

        $html = $this->getChildHtml('product_list');
        $html = Mage::helper('mullayernav')->wrapProducts($html);

        return $html;
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_CatalogSearch_Model_Mysql4_Fulltext_Collection
     * HFV // Se modifica este metodo para que el filtro de precio no muestro productos que no le aplican al cliente logeado
     */
    protected function _getProductCollection() {
        if (is_null($this->_productCollection)) {

            $this->_productCollection = Mage::getSingleton($this->_module . '/layer')
                    ->getProductCollection();
            

            //HFV SI ESTA LOGGEADO VERIFICAMOS CATALOGOS Y UNIDAD DE NEGOCIO
                    
             if ($this->helper('customer')->isLoggedIn() && Mage::helper('mullayernav')->getParam('price')){
                $allowCatalogs = array();
                $session = Mage::getSingleton('customer/session', array('name'=>'frontend'));
                $customer = $session->getCustomer();
                $catalogosCustomer = $customer->getCatalogNbr();
                $catalogosCustomer = trim($catalogosCustomer);
                if($catalogosCustomer && $catalogosCustomer!=""){
                    $catalogosCustomer = explode ( ',' , $catalogosCustomer );
                    foreach ($catalogosCustomer as $catalogoCustomer) {
                        Mage::log(__METHOD__."catalogoCustomer:".$catalogoCustomer);
                        $allowCatalogs[] = array('like' => "%$catalogoCustomer%");
                    }
                    $this->_productCollection->addFieldToFilter('catalog_nbr', $allowCatalogs);
                }               
                

                $customerZonas = $customer->getSupportTeamCd();
                $customerZonas = trim($customerZonas);

                $allowZonas = array();
                if($customerZonas && $customerZonas!=""){
                    $customerZonas = explode(',',$customerZonas);

                    $customerBUs=array();
                    foreach($customerZonas as $SUPPOR_TEAM_CD){
                        $zona = Mage::getModel('corbeta_erpdata/erpzona')->load($SUPPOR_TEAM_CD,'suppor_team_cd');
                        $allowZonas[] = array('like' => "%".$zona->getBusinessUnit()."%");
                    }

                    $this->_productCollection->addFieldToFilter('business_unit', $allowZonas);
                } 

             }
             
        }
     
        return $this->_productCollection;
    }

}
