<?php

class Corbeta_ErpData_Block_Admin_Export extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('corbeta/erpdata/export.phtml');
        }
        return $this;
    }

    /**
     * Unset some non-related element parameters
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
	
	

    /**
     * Get the button and scripts contents
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $originalData = $element->getOriginalData();
        $this->addData(array(
            'button_label' => Mage::helper('customer')->__($originalData['button_label']),
            'html_id' => $element->getHtmlId(),
            'ajax_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/convertclientes/convert'),
            'clientes_export_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/exporterpdata/clientes'),
            'productos_export_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/exporterpdata/productos'),
            'zonas_export_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/exporterpdata/zonas'),
            'direcciones_export_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/exporterpdata/direcciones')
        ));

        return $this->_toHtml();
    }
}
