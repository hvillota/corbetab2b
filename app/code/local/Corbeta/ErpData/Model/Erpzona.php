<?php

class Corbeta_ErpData_Model_Erpzona extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('corbeta_erpdata/erpzona');
    }
	public function validate(){
		return true;
	}
	public function exportZonasCatalogos(){
		
		$sql = "SELECT z.*, c.catalog_nbr FROM erp_zonas z 
LEFT JOIN erp_catalog_x_zonas x ON x.zona_id = z.zona_id
LEFT JOIN erp_catalogs c ON c.catalog_id = x.catalog_id;";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}

}