<?php

class Corbeta_ErpData_Model_Erpproduct extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('corbeta_erpdata/erpproduct');
    }
	public function validate(){
		return true;
	}
	public function getCatalogos(){
		
		$sql = "SELECT DISTINCT c.catalog_nbr FROM erp_product_x_catalog a
				LEFT JOIN erp_catalogs c ON c.catalog_id = a.catalog_id
				WHERE a.product_id = ".$this->getId().";";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}
	public function getCatalogosStr(){
		$sql_result = $this->getCatalogos();
		$catalogos_str = "";
		foreach ($sql_result as $arr_row) {
			$catalogos_str.=",".$arr_row['catalog_nbr'];
		}
		if($catalogos_str==""){
			return false;
		}else{
			return substr($catalogos_str,1);
		}
	}
	public function exportProductosCatalogos(){
		
		$sql = "SELECT p.*,c.catalog_nbr FROM erp_products p
LEFT JOIN erp_product_x_catalog x ON x.product_id = p.erp_product_id
LEFT JOIN erp_catalogs c ON c.catalog_id = x.catalog_id;";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}
	

}