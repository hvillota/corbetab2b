<?php

class Corbeta_ErpData_Model_Resource_Erpcuxzo_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define collection model
     */
    protected function _construct()
    {
		Mage::log(__METHOD__);
        $this->_init('corbeta_erpdata/erpcuxzo');
    }

   
}