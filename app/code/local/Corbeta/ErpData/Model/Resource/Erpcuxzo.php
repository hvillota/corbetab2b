<?php

class Corbeta_ErpData_Model_Resource_Erpcuxzo extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize connection and define main table and primary key
     */
    protected function _construct()
    {
        $this->_init('corbeta_erpdata/erpcuxzo', 'cxz_id');
    }
}