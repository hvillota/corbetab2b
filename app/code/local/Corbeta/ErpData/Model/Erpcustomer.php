<?php

class Corbeta_ErpData_Model_Erpcustomer extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('corbeta_erpdata/erpcustomer');
    }
	public function validate(){
		return true;
	}
	public function toString($format = ''){
		$cadena = $this->getSETID.",".$this->getBUSINESS_UNIT.",".$this->getNAME.",".$this->getTIPO_IDENTIFICAC.",".$this->getCUSTOMER_TYPE.",".$this->getSUPPORT_TEAM_CD.",".
			$this->getCUSTOMER_GROUP.",".$this->getCUS_CR_AVAILABLE.",".$this->getCUPO_DIS.",".$this->getHOLD_CD.",".$this->getIDENTIFICA.",".$this->getEMAIL_ADDR;
		return $cadena;
	}
	public function getZonas(){
		
		$sql = "SELECT z.suppor_team_cd FROM erp_customer_x_zonas x
					LEFT JOIN erp_zonas z ON z.zona_id = x.zona_id
					WHERE x.customer_id = ".$this->getId().";";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}
	public function getZonaDefault(){
		
		$sql = "SELECT z.suppor_team_cd FROM erp_customer_x_zonas x
					LEFT JOIN erp_zonas z ON z.zona_id = x.zona_id
					WHERE x.customer_id = ".$this->getId()." AND x.default = 'Y' LIMIT 1;";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		
		//return $sql_result->getFirstItem();
		if(is_array($sql_result) && count($sql_result)==1){
			return $sql_result[0]['suppor_team_cd'];
		}else{
			return null;
		}
		
	}
	public function getZonasStr(){
		$sql_result = $this->getZonas();
		$zonas_str = "";
		foreach ($sql_result as $arr_row) {
			$zonas_str.=",".$arr_row['suppor_team_cd'];
		}
		if($zonas_str==""){
			return false;
		}else{
			return substr($zonas_str,1);
		}
		
	}
	public function getBusinessUnits(){
		$sql_result = $this->getZonas();
		$bu_str = "";
		foreach ($sql_result as $arr_row) {
			$bu_str.=",".$arr_row['business_unit'];
		}
		if($bu_str==""){
			return false;
		}else{
			return substr($bu_str,1);
		}

	}
	public function getCatalogos(){
		
		$sql = "SELECT DISTINCT c.catalog_nbr FROM erp_customer_x_zonas a
				LEFT JOIN erp_catalog_x_zonas b ON b.zona_id = a.zona_id
				LEFT JOIN erp_catalogs c ON c.catalog_id = b.catalog_id
				WHERE a.customer_id = ".$this->getId().";";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}
	public function getCatalogosStr(){
		$sql_result = $this->getCatalogos();
		$catalogos_str = "";
		foreach ($sql_result as $arr_row) {
			$catalogos_str.=",".$arr_row['catalog_nbr'];
		}
		if($catalogos_str==""){
			return false;
		}else{
			return substr($catalogos_str,1);
		}
	}
	public function exportCustomerAddress(){
		
		$sql = "SELECT c.*, a.* FROM erp_customers c
LEFT JOIN erp_address a ON a.erp_customer_id = c.customer_id;";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}
	public function exportCustomerZonas(){
		
		$sql = "SELECT c.*, z.suppor_team_cd, x.default FROM erp_customers c
LEFT JOIN erp_customer_x_zonas x ON x.customer_id = c.customer_id
LEFT JOIN erp_zonas z ON z.zona_id = x.zona_id;";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql_result = $connection->fetchAll($sql);
		return $sql_result;
	}

}