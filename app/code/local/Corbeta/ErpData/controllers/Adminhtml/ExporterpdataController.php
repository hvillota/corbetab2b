<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * VAT validation controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Corbeta_ErpData_Adminhtml_ExporterpdataController extends Mage_Adminhtml_Controller_Action
{
    
   
	
	public function clientesAction()
    { 	
		
		$fileName   = 'erp_customers.csv';
        
		$model = Mage::getModel('corbeta_erpdata/erpcustomer');
		$data = $model->exportCustomerZonas();
		$content = '"customer_id","setid","business_unit","name","tipo_identificac","cus_id","customer_type","customer_group","cus_cr_available","cupo_dis","hold_cd","email_addr","is_new","is_updated","created_at","updated_at","suppor_team_cd","default"'."\n";

		foreach($data as $line){
		  $content .= implode(",", $line)."\n";
		}
        $this->_prepareDownloadResponse($fileName, $content);
	}
	public function direccionesAction()
    { 	
		
		
		$fileName   = 'erp_direcciones.csv';
        
		$model = Mage::getModel('corbeta_erpdata/erpcustomer');
		$data = $model->exportCustomerAddress();
		$content = '"customer_id","setid","business_unit","name","tipo_identificac","cus_id","customer_type","customer_group","cus_cr_available","cupo_dis","hold_cd","email_addr","is_new","is_updated","created_at","updated_at","address_id","address_seq_num","city","address1","phone","fax","phone1","ship_to_addr","erp_customer_id"'."\n";

		foreach($data as $line){
			$str = implode(",", $line);
			$content .= htmlspecialchars($str)."\n";
		}
        $this->_prepareDownloadResponse($fileName, $content);
	}
	public function zonasAction()
    { 	
		
		
		$fileName   = 'erp_zonas.csv';
        
		$model = Mage::getModel('corbeta_erpdata/erpzona');
		$data = $model->exportZonasCatalogos();
		$content = '"zona_id","suppor_team_cd","default_flag","business_unit","support_team_mbr","phone_type","phone","support_team_mbr1","country_code","is_new","is_updated","created_at","updated_at","catalog_nbr"'."\n";

		foreach($data as $line){
			$str = implode(",", $line);
			$content .= htmlspecialchars($str)."\n";

		}
        $this->_prepareDownloadResponse($fileName, $content);
	}
	public function productosAction()
    { 	
		
		
		$fileName   = 'erp_productos.csv';
        
		$model = Mage::getModel('corbeta_erpdata/erpproduct');
		$data = $model->exportProductosCatalogos();
		$content = '"erp_product_id","setid","business_unit","product_id","descr","unit_of_measure","convertion_rate","unit_of_measure_to","product_group1","tax_pct","list_price","vlr_impu_consumo_terr","attribute_set_id","is_new","is_updated","created_at","updated_at","catalog_nbr"'."\n";

		foreach($data as $line){

		  $str = implode(",", $line);
		  $content .= htmlspecialchars($str)."\n";
		}
        $this->_prepareDownloadResponse($fileName, $content);
	}

}
