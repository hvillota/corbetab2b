 <?php
 /**
 *
 * @author Harold Villota
 */
class Corbeta_Pqr_IndexController extends Mage_Core_Controller_Front_Action
{
    
    public function preDispatch()
    {
        parent::preDispatch();
        /*
        Mage::log(__METHOD__);
        if (!Mage::getStoreConfig('corbeta_pqr/pqr/enabled')) {
        	Mage::log(__METHOD__);
            $this->setFlag('', 'no-dispatch', true);
            $this->_redirect('noRoute');
        } 
        Mage::log(__METHOD__."18");
			*/
    }
    
    /**
     * Index action
     */
    public function indexAction()
    {
		Mage::log(__METHOD__);
    }
	
	public function sendformAction()
    {
		
		$helper = Mage::helper('corbeta_pqr');
		$helper->debugLog(__METHOD__);
		
		$user = Mage::getStoreConfig('corbeta_pqr/pqr/user');
		$password = Mage::getStoreConfig('corbeta_pqr/pqr/password');
		$url = Mage::getStoreConfig('corbeta_pqr/pqr/url');
		
		//PARAMETROS
		$identification  = trim($this->getRequest()->getParam('identification'));
		$name  = $this->getRequest()->getParam('name');
		$lastname  = $this->getRequest()->getParam('lastname');
		$email  = $this->getRequest()->getParam('email');
		$telephone  = $this->getRequest()->getParam('telephone');
		$cel  = $this->getRequest()->getParam('cel');
		$city  = $this->getRequest()->getParam('city');
		$theme  = (int)$this->getRequest()->getParam('theme');
		$type_bussine  = (int)$this->getRequest()->getParam('type_bussine');
		$comment  = $this->getRequest()->getParam('comment');
		$autorizo = "SI";
		$unity  = $this->getRequest()->getParam('unity');
		
		if (!($name && $cel && $email && $city && $theme && $comment)) {
			Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Por favor diligencie todos los campos obligatorios.'));
			$error = true;
		}
		if (!Zend_Validate::is(trim($email) , 'EmailAddress')) {
			Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('El formato del correo electrónico no es valido.'));
			$error = true;
		}
				
		$data = array(
		'usuario'=>$user,//string
		'password'=>$password,//string
		'documento_cliente'=>$identification,//string
		'nombre_cliente'=>$name,//string
		'apellido_cliente'=>$lastname,//string
		'email'=>$email,//string
		'telefono'=>$telephone,//string
		'celular'=>$cel,//string
		'ciudad'=>$city,//string
		'tema'=>$theme,//int
		'tipo_negocio'=>$type_bussine,//int
		'comentario'=>$comment,//string
		'autorizo'=>$autorizo,//string
		'unidad'=>$unity,//string
		);
		$helper->debugLog(__METHOD__.":mensaje enviado:".var_export($data,true));
		
		if (isset($error)) {
			//throw new Exception('se produjo un error');
			$this->_redirect('contacts');
		}else{
			try{
				$response = self::send($url,$data);	
			} catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('En el momento no podemos enviar la solicitud. Por favor int&eacute;ntelo mas tarde.'));
				$error = true;
				$helper->errorLog(__METHOD__.":".$e->getMessage());
				$this->_redirect('contacts');
            }
            if($response){
				$helper->debugLog(__METHOD__.":respuesta:".$response);
            }

		}
		//LAYOUT
		$this->loadLayout();
		$block = $this->getLayout()->createBlock(
		'Mage_Core_Block_Template',
		'prq_thankyou',
		array('template' => 'corbeta/register-thanks.phtml')
		);
 
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
		
    }
	protected function send($url,$data){
	
		$helper = Mage::helper('corbeta_pqr');
		$helper->debugLog(__METHOD__." url:".$url);
		if(Mage::getStoreConfig('corbeta_pqr/pqr/debug')){
			$cli = new SoapClient($url,array('trace' => 1));
		}else{
			$cli = new SoapClient($url);
		}
		
		$response = $cli->__call('crea_caso', $data);
		
		if(Mage::getStoreConfig('corbeta_pqr/pqr/debug')){
			$functions = $cli->__getFunctions();
			$lastresponse = $cli->__getLastResponse();
			$lastrequest = $cli->__getLastRequest();
			$helper->consumoLog(__METHOD__.":request:".$lastrequest);
			$helper->consumoLog(__METHOD__.":response:".$lastresponse);
		}
		
		return $response;
	}
	public function testAction()
    {
		$helper = Mage::helper('corbeta_pqr');
		$helper->debugLog(__METHOD__);
		$corbeta_url = 'http://192.200.0.153/distribuciones/WebServices/service_contactenos.php?wsdl';		
		$user = 'WEBSERVICE';
		$password = 'UA2r3yMDswAaubNB';
		
		
		$data = array(
		'usuario'=>$user,//string
		'password'=>$password,//string
		'documento_cliente'=>'80038369',//string
		'nombre_cliente'=>'harold',//string
		'apellido_cliente'=>'villota',//string
		'email'=>'harold.villota@imaginamos.com',//string
		'telefono'=>'3144016129',//string
		'celular'=>'3144016129',//string
		'ciudad'=>'bogota',//string
		'tema'=>'1',//int
		'tipo_negocio'=>'1',//int
		'comentario'=>'comentario de prueba',//string
		'autorizo'=>'si',//string
		'unidad'=>'distribuciones',//string
		);
		$respuesta = self::send($corbeta_url,$data);
		
		//HTML
        $html =  "<h1>Gracias por su solicitud</h1>";
        $html .=  "<h2>Respuesta: $respuesta</h2>";
		$this->getResponse()->setBody($html);
		
	}
   
}