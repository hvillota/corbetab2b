<?php

class Corbeta_Destacados_Block_Home extends Mage_Catalog_Block_Product_List

{
    
    protected function _construct()
    {
		//Mage::log(__METHOD__);
        parent::_construct();
    }
	protected function _getProductCollection()
    {
        
            $layer = $this->getLayer();

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        

        return $this->_productCollection;
    }
}
