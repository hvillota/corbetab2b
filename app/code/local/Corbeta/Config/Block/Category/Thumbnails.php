<?php

class Corbeta_Config_Block_Category_Thumbnails extends Mage_Core_Block_Template

{
    
    protected function _construct()
    {
        parent::_construct();
		
    }
	 protected function _prepareLayout()
    {
		parent::_prepareLayout();
		 $this->getLayout()->createBlock('catalog/breadcrumbs');
	}
	public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $this->setData('current_category', Mage::registry('current_category'));
        }
        return $this->getData('current_category');
    }

}
