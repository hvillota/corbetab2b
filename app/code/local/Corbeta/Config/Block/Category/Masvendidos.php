<?php

class Corbeta_Config_Block_Category_Masvendidos extends Mage_Catalog_Block_Product_List

{
    
    protected function _construct()
    {
        parent::_construct();
		$storeId = Mage::app()->getStore()->getId();
		 $products = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image'))
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder('ordered_qty', 'desc'); // most best sellers on top
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
 
        $products->setPageSize(100)->setCurPage(1);
        
        $this->setProductCollection($products);
    }
	 protected function _prepareLayout()
    {
		parent::_prepareLayout();
	}
	public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $this->setData('current_category', Mage::registry('current_category'));
        }
        return $this->getData('current_category');
    }
}
