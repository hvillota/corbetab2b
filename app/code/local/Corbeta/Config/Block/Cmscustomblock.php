<?php

class Corbeta_Config_Block_Cmscustomblock extends Mage_Core_Block_Template
{
    
	public function getHtml()
    {
		$blockId = $this->getBlockId();
        $html = '';
        if ($blockId) {
            $block = Mage::getModel('cms/block')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($blockId);
            if ($block->getIsActive()) {
                $html = $block->getContent();
            }
        }
        return $html;
	}
}
