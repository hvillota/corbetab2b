<?php

class Corbeta_Config_Model_Observer extends Mage_Core_Model_Abstract
{
	public function logCompiledLayout($o)
	{
		$req  = Mage::app()->getRequest();
		$info = sprintf(
			"\nRequest: %s\nFull Action Name: %s_%s_%s\nHandles:\n\t%s\nUpdate XML:\n%s",
			$req->getRouteName(),
			$req->getRequestedRouteName(),      //full action name 1/3
			$req->getRequestedControllerName(), //full action name 2/3
			$req->getRequestedActionName(),     //full action name 3/3
			implode("\n\t",$o->getLayout()->getUpdate()->getHandles()),
			$o->getLayout()->getUpdate()->asString()
		);

		// Force logging to var/log/layout.log
		//Mage::log($info, Zend_Log::INFO, 'layout.log', true);
	}
	public function addCategoryLayerLayoutHandle(Varien_Event_Observer $observer) {
        $category = Mage::registry('current_category');
        $product = Mage::registry('current_product');
        if ($category ) {
        	//Mage::log(__METHOD__.":category:".$category->getId());
            if($category->getLevel() == 2) {
                /* @var $update Mage_Core_Model_Layout_Update */
                $update = $observer->getEvent()->getLayout()->getUpdate();
                $update->addHandle('main_category');
            }
        }
    }
	public function verificarMaximoContado(Varien_Event_Observer $observer){
		
		$cart = $observer->getCart();
		$quote = $cart->getQuote();
		$session = $cart->getCheckoutSession();
		if (!$quote->validateMaximoContado()) {
	        $maximoContado = '$2.000.000';
	        $msg = 'Su orden excede el valor m&#225;ximo de compra: '.$maximoContado;
	        $msg = Mage::getSingleton('core/message')->notice($msg);
			$session->addUniqueMessages($msg);
	    }
	}
	public function verificarCupoDisponible(Varien_Event_Observer $observer){
		$cart = $observer->getCart();
		$quote = $cart->getQuote();
		$session = $cart->getCheckoutSession();
	    if (!$quote->validateCupoDisponible()) {
	        $msg = 'Estimado cliente el valor de tu pedido excede el valor de tu cupo disponible. Este pedido queda sujeto a validación de cartera';
	        $msg = Mage::getSingleton('core/message')->notice($msg);
			$session->addUniqueMessages($msg);
	    }
	}
	public function verificarCatalogo(Varien_Event_Observer $observer){
		//Mage::log(__METHOD__);
		
		$product = $observer->getProduct();
		$salable = $observer->getSalable();
		
		$session = Mage::getSingleton('customer/session', array('name'=>'frontend'));
		$salable->setIsSalable(false);
		if($session->isLoggedIn()){
					
			$customer = $session->getCustomer();
			$catalogosCustomer = $customer->getCatalogNbr();
			$catalogosCustomer = trim($catalogosCustomer);
			$fullProduct = Mage::getModel('catalog/product')->load($product->getId());
			$catalogosProducto = $fullProduct->getCatalogNbr();
			$catalogosProducto = trim($catalogosProducto);
			
			
			if($catalogosCustomer && $catalogosCustomer!="" && $catalogosProducto && $catalogosProducto!=""){				
				$catalogosCustomer = explode ( ',' , $catalogosCustomer );
				$catalogosProducto = explode ( ',' , $catalogosProducto );
				
				$customerZonas = $customer->getSupportTeamCd();
				$customerZonas = trim($customerZonas);
				$customerZonas = explode(',',$customerZonas);
				
				$customerBUs=array();
				foreach($customerZonas as $SUPPOR_TEAM_CD){
					$zona = Mage::getModel('corbeta_erpdata/erpzona')->load($SUPPOR_TEAM_CD,'suppor_team_cd');
					$customerBUs[] = $zona->getBusinessUnit();
				}

				foreach($catalogosProducto as $catalogo){
					if($catalogosProducto && in_array($catalogo,$catalogosCustomer)){
						//COINCIDE CATALOGO

						$productBU = $fullProduct->getBusinessUnit();
						$productBUs = explode(',',$productBU);
						foreach ($productBUs as $pbu) {
							if($pbu && in_array($pbu,$customerBUs)){
								$salable->setIsSalable(true);
								break 2;
							}
						}
					}
				}
			}
			
		}
		
	}
	public function verificarNombreRolUnico(Varien_Event_Observer $observer) {
        
		//@$role is Mage_Admin_Model_Roles
		$role = $observer->getObject();
		if($role){
			if($role->getRoleType()=='G'){
				
				$roles = Mage::getModel('admin/role')->getCollection()
				->addFieldToFilter('role_type',$role->getRoleType())
				->addFieldToFilter('role_name',$role->getName());
				
				if(count($roles)>1){
					throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Ya existe un rol con el el nombre '.$role->getName()),2);
				}if(count($roles)==1){
					if($role->getId()){
						$_role = $roles->getFirstItem();
						if($role->getId()!=$_role->getId()){
							throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Ya existe un rol con el el nombre '.$role->getName()),2);
						}
					}else{
						throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Nuevo Ya existe un rol con el el nombre '.$role->getName()),2);
					}
				}	
			}
		}
    }
}