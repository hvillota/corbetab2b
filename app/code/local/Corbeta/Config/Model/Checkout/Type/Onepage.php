<?php

class Corbeta_Config_Model_Checkout_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{


 /**
     * Create order based on checkout type. Create customer if necessary.
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function saveOrder()
    {
        $this->validate();Mage::log(__METHOD__.':769:');
        $isNewCustomer = false;
        switch ($this->getCheckoutMethod()) {
            case self::METHOD_GUEST:
                $this->_prepareGuestQuote();
                break;
            case self::METHOD_REGISTER:
                $this->_prepareNewCustomerQuote();
                $isNewCustomer = true;
                break;
            default:
                $this->_prepareCustomerQuote();
                break;
        }

///codigo que crea las ordenes por catalogo

        $_quote = $this->getQuote();
        $_customer = $_quote->getCustomer();
        $_cliente_catalogos = $_customer->getCatalogNbr();

        $_cliente_catalogos = explode(',',$_cliente_catalogos);
        $_catalogos_usados = array();

foreach ($_quote->getAllItems() as $item) {
     Mage::log(__METHOD__.':item:'.get_class($item));
    $_product = Mage::getModel('catalog/product')->load($item->getProductId());
    $_product_catalogos = $_product->getCatalogNbr();
    Mage::log(__METHOD__.':_product_catalogos:'.$_product_catalogos);
    $_product_catalogos = explode(',',$_product_catalogos);

    foreach ($_product_catalogos as $_product_catalogo) {
        Mage::log(__METHOD__.':_product_catalogo:'.$_product_catalogo);
        if(in_array($_product_catalogo,$_cliente_catalogos)){
            $_catalogo = $_product_catalogo;
            Mage::log(__METHOD__.':_product_catalogo encontrado:'.$_product_catalogo);
            break;
        }
    }
    if(!array_key_exists($_catalogo,$_catalogos_usados)){
         $_catalogos_usados[$_catalogo]= array();
    }
    $_catalogos_usados[$_catalogo][] = $item;
   
}

$websiteId = Mage::app()->getWebsite()->getId();
$store = Mage::app()->getStore();

$order_ids = array();
foreach ($_catalogos_usados as $catalogo =>$_catalogo_items) {
    Mage::log(__METHOD__.':procesando catalogo:'.$catalogo);
    $newquote = Mage::getModel('sales/quote')->setStoreId($store->getId());
    $newquote->setCurrency($_quote->getCurrency());
    $newquote->assignCustomer($_customer);
    $newquote->setSendCconfirmation(0);
    foreach ($_catalogo_items as $_catalogo_item) {
        $_product = Mage::getModel('catalog/product')->load($_catalogo_item->getProductId());
        $newquote->addProduct($_product,new Varien_Object(array('qty'   => $_catalogo_item->getQty())));
    }
$newquote->setIsMultiShipping(false);
    $newquote->getShippingAddress()->collectShippingRates(true)
                 ->setShippingMethod('freeshipping_freeshipping')
                 ->setPaymentMethod('cashondelivery');

    $newquote->collectTotals();

    
    $newquote->save();
    $_billingAddress = $_quote->getBillingAddress();
    $_billingAddress = Mage::getModel('customer/address')->load($_billingAddress->getCustomerAddressId());
    $newquote->getBillingAddress()->importCustomerAddress($_billingAddress)->setSaveInAddressBook(0);
    $_shippingAddress = $_quote->getShippingAddress();
    $_shippingAddress = Mage::getModel('customer/address')->load($_shippingAddress->getCustomerAddressId());
    $newquote->getShippingAddress()->importCustomerAddress($_shippingAddress)->setSaveInAddressBook(0);
 
	$newquote->getShippingAddress()->setCollectShippingRates(true)
                 ->collectShippingRates()
                 ->setShippingMethod('freeshipping_freeshipping')
                 ->setPaymentMethod('cashondelivery');
    $newquote->getPayment()->importData(array('method' => 'cashondelivery'));
    $newquote->collectTotals();

    
    if (!$newquote->validateMinimumAmount()) {
        $minimumAmount = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())
                    ->toCurrency(Mage::getStoreConfig('sales/minimum_order/amount'));
        $newquote->setIsActive(0);
        $newquote->save(); 
        Mage::throwException('El valor mínimo de compra por catálogo es de: '.$minimumAmount);
    }

    if (!$newquote->validateMaximoContado()) {
        $newquote->setIsActive(0);
        $newquote->save(); 
        $maximoContado = '$2.000.000';
        Mage::throwException('Su orden excede el valor máximo de compra: '.$maximoContado);
    }
    


    $newquote->collectTotals()->save();
    $service = Mage::getModel('sales/service_quote', $newquote);
    
    try {
          $service->submitAll();
          $increment_id = $service->getOrder()->getRealOrderId();
          $order_ids[]=$increment_id;
          $newquote->setIsActive(0);
          $newquote->save();
        Mage::log(__METHOD__.':increment_id:'.$increment_id);
    } catch (Exception $e) {
        Mage::log(__METHOD__.': 851 e:'.$e->getMessage());
    }
    
    

}

 Mage::log(__METHOD__.':869:');
$this->getQuote()->setIsActive(0);
$this->getQuote()->save();


$this->_checkoutSession->setLastOrderIds(implode(',',$order_ids));
 
 
        
        if ($isNewCustomer) {
            try {
                $this->_involveNewCustomer();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
Mage::log(__METHOD__.':794:');
        $this->_checkoutSession->setLastQuoteId($this->getQuote()->getId())
            ->setLastSuccessQuoteId($this->getQuote()->getId())
            ->clearHelperData();

        $order = $service->getOrder();
        if ($order) {
            Mage::log(__METHOD__.':800:');
            Mage::dispatchEvent('checkout_type_onepage_save_order_after',
                array('order'=>$order, 'quote'=>$this->getQuote()));

            /**
             * a flag to set that there will be redirect to third party after confirmation
             * eg: paypal standard ipn
             */
            $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();
            /**
             * we only want to send to customer about new order when there is no redirect to third party
             */
            if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
                try {
                    $order->queueNewOrderEmail();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            // add order information to the session
            $this->_checkoutSession->setLastOrderId($order->getId())
                ->setRedirectUrl($redirectUrl)
                ->setLastRealOrderId($order->getIncrementId());

            // as well a billing agreement can be created
            $agreement = $order->getPayment()->getBillingAgreement();
            if ($agreement) {
                $this->_checkoutSession->setLastBillingAgreementId($agreement->getId());
            }
        }


        // add recurring profiles information to the session
        $profiles = $service->getRecurringPaymentProfiles();
        if ($profiles) {
            $ids = array();
            foreach ($profiles as $profile) {
                $ids[] = $profile->getId();
            }
            $this->_checkoutSession->setLastRecurringProfileIds($ids);
            // TODO: send recurring profile emails
        }

        Mage::dispatchEvent(
            'checkout_submit_all_after',
            array('order' => $order, 'quote' => $this->getQuote(), 'recurring_profiles' => $profiles)
        );

        return $this;
    }


}