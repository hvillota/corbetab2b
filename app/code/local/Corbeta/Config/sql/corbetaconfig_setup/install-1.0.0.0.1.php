
<?php
$installer = $this;
$installer->startSetup();
$attribute  = array(
    'type' => 'text',
    'label'=> 'Color',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information",
	'sort_order' => 5
);
$installer->addAttribute('catalog_category', 'category_color', $attribute);
$attribute  = array(
    'type' => 'varchar',
    'label'=> 'Icono',
    'input' => 'image',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information",
	'sort_order' => 5,
	'backend'=>'catalog/category_attribute_backend_image'
);
$installer->addAttribute('catalog_category', 'category_icon', $attribute);
$installer->endSetup();
?>