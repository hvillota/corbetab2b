<?php

class Corbeta_Pedidos_Model_Pedido extends Mage_Core_Model_Abstract{
	public function crearPedido($data) {
		Mage::log(__METHOD__);
		if(!Mage::getStoreConfig('corbeta_pedidos/create/enabled')){
			return;
		}
		//$corbeta_url = 'http://192.200.0.88:8095/PSIGW/PeopleSoftServiceListeningConnector/CK_MICROS_CREACION_PEDIDOS.1.wsdl';		
		$corbeta_url = Mage::getStoreConfig('corbeta_pedidos/create/url');
		if(!$corbeta_url){
			Mage::log(__METHOD__.':12 no hay url:');
			return;
		}
		Mage::log(__METHOD__.':9:'.$corbeta_url);
		$respuesta = self::send($corbeta_url,array($data));
		return $respuesta;
		
	}
	protected function send($url,$data){
		Mage::log(__METHOD__.':20:');
		$helper = Mage::helper('corbeta_pedidos');
		Mage::log(__METHOD__.':21:');
		$helper->debugLog(__METHOD__);
		Mage::log(__METHOD__.':22:');
		if(Mage::getStoreConfig('corbeta_pedidos/create/debug')){
			$cli = new SoapClient($url,array('trace' => 1));
		}else{
			$cli = new SoapClient($url);
		}
		
		try{
			$helper->debugLog(__METHOD__.":23");
			//Esto es necesario debido a que la ip de comunicacion se usa enmascarada y el contrato devuelve la ip por defecto
			$oldLocation = $cli->__setLocation($url);
			$response = $cli->__call('CK_MICROS_CREACION_PEDIDOS', $data);
			
			if(is_soap_fault($response)){
				$helper->errorLog(__METHOD__.":soap faultcode:".$response->faultcode);
				$helper->errorLog(__METHOD__.":soap faultstring:".$response->faultstring);
			}
			
		}catch(Exception $e){
			$response = $e->getMessage();
			$helper->errorLog(__METHOD__.":error:".$e->getMessage());
			
		}
		
		if(Mage::getStoreConfig('corbeta_pedidos/create/debug')){
			$helper->debugLog(__METHOD__.":3");
			$functions = $cli->__getFunctions();
			$lastresponse = $cli->__getLastResponse();
			$lastrequest = $cli->__getLastRequest();
			$helper->debugLog(__METHOD__.":functions:".var_export($functions,true));
			$helper->consumoLog(__METHOD__.":request:".$lastrequest);
			$helper->consumoLog(__METHOD__.":response:".$lastresponse);
		}
		
		return $response;
	}
}