<?php

class Corbeta_Pedidos_Model_Observer{

	public function enviarPedido($observer) {
		
		if(!Mage::getStoreConfig('corbeta_pedidos/create/enabled')){
			return;
		}
		
		$_order = $observer->getOrder();
		
		$_customer = $_order->getCustomer();
		$_order_billing = $_order->getBillingAddress();Mage::log(__METHOD__.':16:');
		$_order_shipping = $_order->getShippingAddress();
		
		$zonas_cliente = $_customer->getSupportTeamCd();
		Mage::log(__METHOD__.': zonas cliente:'.$zonas_cliente);
		$zona_cliente_defecto = $_customer->getZonaDefault();
		Mage::log(__METHOD__.': zonas defecto:'.$zona_cliente_defecto);
		$zona_erp = Mage::getModel('corbeta_erpdata/erpzona')->load($zona_cliente_defecto,'suppor_team_cd');
		Mage::log(__METHOD__.': zonas class:'.get_class($zona_erp));
		Mage::log(__METHOD__.': zonas UNIDAD:'.$zona_erp->getBusinessUnit());
		
		$_cliente_catalogos = $_customer->getCatalogNbr();
		Mage::log(__METHOD__.':_cliente_catalogos:'.$_cliente_catalogos);
		$_cliente_catalogos = explode(',',$_cliente_catalogos);
		
		$_address = Mage::getModel('customer/address')->load($_order_shipping->getCustomerAddressId());
		
		$date = Mage::getModel('core/date')->date('Y-m-d');
		Mage::log(__METHOD__.':24:'.$date);
		
		$errors = array();
		$lines = array();
		
		$orderErpInfo = array(
				'BUSINESS_UNIT' => $zona_erp->getBusinessUnit(),
				'ORDER_DATE' =>  $date,
				'SUBCUST_QUAL1' => $zona_erp->getSupporTeamCd(),
				'CK_CONSEC_MARS_ZON' => '200',
				'CK_CONSEC_MARS' => '10000',
				'ORDER_NO' => $_order->getIncrementId(),
				'CUST_ID' => $_customer->getCusId(),
				'ADDRESS_SEQ_NUM' => $_address->getAddressSeqNum(),
				'CUSTOMER_PO' => '0',
			);
		$_order->setErpInfo(json_encode($orderErpInfo));
			
		foreach ($_order->getAllItems() as $item) {
			
			$_product = Mage::getModel('catalog/product')->load($item->getProductId());
			
			$_product_catalogos = $_product->getCatalogNbr();
			Mage::log(__METHOD__.':_product_catalogos:'.$_product_catalogos);
			$_product_catalogos = explode(',',$_product_catalogos);
			
			foreach ($_product_catalogos as $_product_catalogo) {
				Mage::log(__METHOD__.':_product_catalogo:'.$_product_catalogo);
				if(in_array($_product_catalogo,$_cliente_catalogos)){
					$_catalogo = $_product_catalogo;
					Mage::log(__METHOD__.':_product_catalogo encontrado:'.$_product_catalogo);
					break;
				}
			}
			if(!isset($_catalogo)){
				$errors[]="No se encuentra el catalogo";
				continue;
			}
			
			$CK_ROOT_WS = array();
			$CK_MICR_PED_REQ = array();
			$PSCAMA = array();
			
			$FieldTypes = array(
				'CK_ROOT_WS' => $CK_ROOT_WS,
				'CK_MICR_PED_REQ' => $CK_MICR_PED_REQ,
				'PSCAMA' => $PSCAMA,
			);
			$MsgData = array();
			
			$_M_CK_MICR_PED_REQ = array(
				'class'=>'R',
				'BUSINESS_UNIT' => array( '_' => $zona_erp->getBusinessUnit(), 'IsChanged'=>'Y'),//char OK
				'ORDER_DATE' => array( '_' => $date, 'IsChanged'=>'Y'),//date OK
				'SUBCUST_QUAL1' => array( '_' => $zona_erp->getSupporTeamCd(), 'IsChanged'=>'Y'),//char ZONA OK
				'CK_CONSEC_MARS_ZON' => array( '_' => '200', 'IsChanged'=>'Y'),//number OK
				'CK_CONSEC_MARS' => array( '_' => '10000', 'IsChanged'=>'Y'),//number OK
				'ORDER_NO' => array( '_' => $_order->getIncrementId(), 'IsChanged'=>'Y'),//char OK
				'PRODUCT_ID' => array( '_' => $_product->getSku(), 'IsChanged'=>'Y'),//char OK
				'QTY' => array( '_' => $item->getQtyOrdered(), 'IsChanged'=>'Y'),//number OK
				'CUST_ID' => array( '_' => $_customer->getCusId(), 'IsChanged'=>'Y'),//char OK
				'CATALOG_NBR' => array( '_' => $_catalogo, 'IsChanged'=>'Y'),//char  OK
				'ADDRESS_SEQ_NUM' => array( '_' => $_address->getAddressSeqNum(), 'IsChanged'=>'Y'),//number OK
				'CUSTOMER_PO' => array( '_' => '0', 'IsChanged'=>'Y'),//char OK
				'LIST_PRICE' => array( '_' => $item->getPrice(), 'IsChanged'=>'Y'),//number OK
			);

			$orderItemErpInfo = array(
				'PRODUCT_ID' => $_product->getSku(),
				'QTY' => $item->getQtyOrdered(),
				'CATALOG_NBR' => $_catalogo,
				'LIST_PRICE' => $item->getPrice(),
			);

			
			
			$Transaction = array(
				'CK_ROOT_WS' => array('CK_MICR_PED_REQ' => $_M_CK_MICR_PED_REQ,"class"=>"R"),
				'PSCAMA' => $PSCAMA,
			);
			
			$MsgData[] = $Transaction;
			
			$data = array(
				'FieldTypes'=>$FieldTypes,
				'MsgData'=>$MsgData,
			);
			
			$lines[$item->getId()] = $data;

			$item->setErpInfo(json_encode($orderItemErpInfo));
		}
		
		$model = Mage::getModel("corbeta_pedidos/pedido");
		Mage::log(__METHOD__.':55:');
		try {
			foreach($lines as $id => $line){
				$respuesta = $model->crearPedido($line);
				if($respuesta){
					Mage::log(__METHOD__.':131:'.$id);
					$_MsgData = $respuesta->MsgData;
					$_Transaction = $_MsgData->Transaction;
					$_CK_ROOT_WS = $_Transaction->CK_ROOT_WS;
					$_ERROR_FLAG = $_CK_ROOT_WS->ERROR_FLAG;
					$_ERROR_MESSAGE_TXT = $_CK_ROOT_WS->ERROR_MESSAGE_TXT;
					$error_flag=$_ERROR_FLAG->_;
					$error_msg=$_ERROR_MESSAGE_TXT->_;
					$orden_item = Mage::getModel('sales/order_item')->load($id);
					$ean = $orden_item->getSku();
					$_order->addStatusHistoryComment("Producto:$ean Estado:$error_flag Mensaje:$error_msg", $_order->getStatus());
				}

				$__MsgData = $line['MsgData'];
				$__Transaction = $__MsgData[0];
				$__CK_ROOT_WS = $__Transaction['CK_ROOT_WS'];
				$__CK_MICR_PED_REQ = $__CK_ROOT_WS['CK_MICR_PED_REQ'];
			}
          
        } catch (Exception $e) {
			Mage::log(__METHOD__.': e:'.$e->getMessage());
		}
		
		Mage::log(__METHOD__.':56:');
		//exit ();

		//$helper->debugLog(__METHOD__." finalizado");
	}
}