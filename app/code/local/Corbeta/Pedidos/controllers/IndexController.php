 <?php
 /**
 *
 * @author Harold Villota
 */
class Corbeta_Pedidos_IndexController extends Mage_Core_Controller_Front_Action
{

	//funcion para hacer pruebas
	protected function send($url,$data){
	
		$helper = Mage::helper('corbeta_pedidos');
		$helper->debugLog(__METHOD__);
		if(Mage::getStoreConfig('corbeta_pedidos/create/debug')){
			$cli = new SoapClient($url,array('trace' => 1));
		}else{
			$cli = new SoapClient($url);
		}
		
		try{
			$helper->debugLog(__METHOD__.":23");
			//Esto es necesario debido a que la ip de comunicacion se usa enmascarada y el contrato devuelve la ip por defecto
			$oldLocation = $cli->__setLocation('http://192.200.0.88:8095/PSIGW/PeopleSoftServiceListeningConnector/CK_MICROS_CREACION_PEDIDOS.1.wsdl');
			
			
			$response = $cli->__call('CK_MICROS_CREACION_PEDIDOS', $data);
			
			if(is_soap_fault($response)){
				$helper->errorLog(__METHOD__.":soap faultcode:".$response->faultcode);
				$helper->errorLog(__METHOD__.":soap faultstring:".$response->faultstring);
			}
			
		}catch(Exception $e){
			$response = $e->getMessage();
			$helper->errorLog(__METHOD__.":error:".$e->getMessage());
			
		}
		
		
		if(Mage::getStoreConfig('corbeta_pedidos/create/debug')){
			$helper->debugLog(__METHOD__.":3");
			$functions = $cli->__getFunctions();
			$lastresponse = $cli->__getLastResponse();
			$lastrequest = $cli->__getLastRequest();
			$helper->debugLog(__METHOD__.":functions:".var_export($functions,true));
			$helper->consumoLog(__METHOD__.":request:".$lastrequest);
			$helper->consumoLog(__METHOD__.":response:".$lastresponse);
		}
		
		return $response;
	}
	//funcion para hacer pruebas
	public function testAction()
    {
		$helper = Mage::helper('corbeta_pedidos');
		$helper->debugLog(__METHOD__);
		$corbeta_url = 'http://192.200.0.88:8095/PSIGW/PeopleSoftServiceListeningConnector/CK_MICROS_CREACION_PEDIDOS.1.wsdl';		
		
		/*
		$data = new stdClass();
		$data->FieldTypes = new stdClass();
		$data->FieldTypes->CK_ROOT_WS = new stdClass();
		$data->FieldTypes->CK_MICR_PED_REQ = new stdClass();
		$data->FieldTypes->PSCAMA = new stdClass();
		*/
		
		
		$CK_ROOT_WS = array();
		$CK_MICR_PED_REQ = array();
		$PSCAMA = array();
		
		$FieldTypes = array(
			'CK_ROOT_WS' => $CK_ROOT_WS,
			'CK_MICR_PED_REQ' => $CK_MICR_PED_REQ,
			'PSCAMA' => $PSCAMA,
		);
		
		$_M_CK_MICR_PED_REQ = array(
			'class'=>'R',
			'BUSINESS_UNIT' => array( '_' => 'DIBOG', 'IsChanged'=>'Y'),//char
			'ORDER_DATE' => array( '_' => '2015-06-09', 'IsChanged'=>'Y'),//date
			'SUBCUST_QUAL1' => array( '_' => 'PYG502', 'IsChanged'=>'Y'),//char ZONA
			'CK_CONSEC_MARS_ZON' => array( '_' => '200', 'IsChanged'=>'Y'),//number
			'CK_CONSEC_MARS' => array( '_' => '10000', 'IsChanged'=>'Y'),//number
			'ORDER_NO' => array( '_' => '1125631', 'IsChanged'=>'Y'),//char
			'PRODUCT_ID' => array( '_' => '7702027044168', 'IsChanged'=>'Y'),//char
			'QTY' => array( '_' => '1', 'IsChanged'=>'Y'),//number
			'CUST_ID' => array( '_' => '12121216', 'IsChanged'=>'Y'),//char
			'CATALOG_NBR' => array( '_' => 'COMABG', 'IsChanged'=>'Y'),//char
			'ADDRESS_SEQ_NUM' => array( '_' => '1', 'IsChanged'=>'Y'),//number
			'CUSTOMER_PO' => array( '_' => '0', 'IsChanged'=>'Y'),//char
			'LIST_PRICE' => array( '_' => '10000', 'IsChanged'=>'Y'),//number
		);
		
		$Transaction = array(
			'CK_ROOT_WS' => array('CK_MICR_PED_REQ' => $_M_CK_MICR_PED_REQ,"class"=>"R"),
			'PSCAMA' => $PSCAMA,
		);
		
		$MsgData = array(
			$Transaction
		);
		
		$data = array(
			'FieldTypes'=>$FieldTypes,//string
			'MsgData'=>$MsgData,//string
		);
		
		
		$respuesta = self::send($corbeta_url,array($data));
		
		//HTML
        $html =  "<h1>Gracias por su solicitud</h1>";
        $html .=  "<h2>Respuesta: ".var_export($respuesta,true)."</h2>";
		$this->getResponse()->setBody($html);
		
	}
	//funcion para ingresar contenido simulado
	public function completarproductosAction(){
	
		$productos = Mage::getModel('catalog/product')->getCollection();
		$html =  "<h1>Gracias por su solicitud</h1>";
		$m = 0;
		
		$catalogos= array('COMABG','CONMED','ELECNAL','P&GBOG');
		
		foreach($productos as $magentoProduct){
			$magentoProduct->setPrice(mt_rand (10000,50000));
			
			$magentoProduct->setSetid('COLCO');
			$magentoProduct->setBusinessUnit('DIBOG');
			$magentoProduct->setUnitOfMeasure('UND');
			$magentoProduct->setConvertionRate('28.0000');
			$magentoProduct->setUnitOfMeasureTo('BOX');
			$magentoProduct->setProductGroup1('CVG');
			//$magentoProduct->setTaxPct(??);
			//$magentoProduct->setVlrImpuConsumoTerr(??);
			$nc = mt_rand (1,4);
			$cat = array();
			for ($i = 1; $i <= $nc; $i++) {
				$xc = mt_rand (0,3);
				$cat[] = $catalogos[$xc];
			}
			
			$magentoProduct->setCatalogNbr(implode(',',$cat));
			
			if($magentoProduct->getSku()!='7506195177709'){
				//$magentoProduct->save();
			}
			
			$html .=  "<h2>Respuesta: ".$magentoProduct->getSku()."</h2>";
			$m++;
			//if($m>2)break; //limita a n productos solamente
		}
		
        //$html .=  "<h2>Respuesta: ".gettype($customer->getZonaDefault())."</h2>";
        //$html .=  "<h2>Respuesta: ".var_export($customer->getZonaDefault(),true)."</h2>";
        //$html .=  "<h2>Respuesta: ".get_class($customer->getZonaDefault())."</h2>";
        
		$this->getResponse()->setBody($html);
	}
	
	//funcion para ingresar contenido simulado
	public function completarproductos2Action(){
	
		$productos = Mage::getModel('catalog/product')->getCollection();
		$html =  "<h1>Gracias por su solicitud</h1>";
		$m = 0;
		
		
		foreach($productos as $magentoProduct){
			
			
			$magentoProduct->setCatalogNbr('COMABG');
			
			if($magentoProduct->getSku()=='8806085971615'){
				//$magentoProduct->save();
			}
			
			$html .=  "<h2>Respuesta: ".$magentoProduct->getSku()."</h2>";
			$m++;
			//if($m>2)break; //limita a n productos solamente
		}
		
        //$html .=  "<h2>Respuesta: ".gettype($customer->getZonaDefault())."</h2>";
        //$html .=  "<h2>Respuesta: ".var_export($customer->getZonaDefault(),true)."</h2>";
        //$html .=  "<h2>Respuesta: ".get_class($customer->getZonaDefault())."</h2>";
        
		$this->getResponse()->setBody($html);
	}

	public function conjuntosAction(){	
		$html =  "<h2>Conjuntos de Atributos</h2>";
		$attributeSets = Mage::getModel('eav/entity_attribute_set')->getCollection();
		$html =  "<p>encontrados:".$attributeSets->getSize()."</p>";
		foreach($attributeSets as $attributeSet){
			$html .= "<p>".$attributeSet->getId().":".$attributeSet->getAttributeSetName()."</p>";
		}
		$this->getResponse()->setBody($html);
	}
	public function erpproductsAction(){	
		$html =  "<h2>Leyendo Erp Producto</h2>";
		
		$log['msj'] = array();
		$log['total'] = array();
		$log['tiene_conjunto'] = array();
		$log['nuevos'] = array();
		$log['actualizados'] = array();
		$log['no_tiene_conjunto'] = array();

		$erpProducts = Mage::getModel('corbeta_erpdata/erpproduct')->getCollection();
		foreach($erpProducts as $erpProduct){
			$log['total'] [] = $erpProduct;
			$log['msj'][] = "EAN:". $erpProduct->getProductId()." :".$erpProduct->getAttributeSetId();
			if($erpProduct->getAttributeSetId()){
				$log['tiene_conjunto'][] = $erpProduct;
			}else{
				$log['no_tiene_conjunto'][] = $erpProduct;
			}
			if($erpProduct->getIsNew()){
				$log['nuevos'] [] = $erpProduct;
			}elseif($erpProduct->getIsUpdated()){
				$log['actualizados'] [] = $erpProduct;
			}
		}
		
		foreach($log['msj'] as $line){
			$html.="<p>$line</p>";
		}
		
		$html .=  "<h2>Total:".count($log['total'])."</h2>";
		$html .=  "<h2>Tienen Conjunto:".count($log['tiene_conjunto'])."</h2>";
		$html .=  "<h2>No Tienen Conjunto:".count($log['no_tiene_conjunto'])."</h2>";
		$html .=  "<h2>Nuevos:".count($log['nuevos'])."</h2>";
		$html .=  "<h2>Actualizados:".count($log['actualizados'])."</h2>";
		$this->getResponse()->setBody($html);
	}
	public function magentoproductsAction(){	
		$html =  "<h2>Leyendo Erp Producto</h2>";
		
		$log['conjuntos'] = array();
		
		
		$magentoProducts = Mage::getModel('catalog/product')->getCollection();

		foreach($magentoProducts as $magentoProduct){
			$setid = $magentoProduct->getAttributeSetId();
			if($setid){
				$log['conjuntos'] [$setid]= $setid;	
			}
			
		}
		$filename = realpath(dirname('archivo.csv'));
		$filename .= '/listaconjuntos.csv';
		$myfile = fopen($filename, "w");

		foreach($log['conjuntos'] as $setid){
			$attributeSet = Mage::getModel('eav/entity_attribute_set')->load($setid);
			if($attributeSet->getId()){
				fwrite($myfile,$attributeSet->getId().",".$attributeSet->getAttributeSetName()."\n");				
			}
			
		}
		fclose($myfile);
		
		$html .=  "<h2>Actualizados:".count($log['conjuntos'])."</h2>";

		$this->getResponse()->setBody($html);
	}
	public function magentocategoriesAction(){	
		$html =  "<h2>Leyendo Categorias</h2>";
		
		$log['conjuntos'] = array();
		
		
		$categories = Mage::getModel('catalog/category')->getCollection();

		$filename = realpath(dirname('archivo.csv'));
		$filename .= '/listacategorias.csv';
		$myfile = fopen($filename, "w");

		foreach($categories as $category){
			$id = $category->getId();
			$log['conjuntos'] [$id]= $id;	
			$cat = Mage::getModel('catalog/category')->load($id);
			fwrite($myfile,$category->getId().",".$cat->getName()."\n");				
		}
		
		fclose($myfile);
		
		$html .=  "<h2>Actualizados:".count($log['conjuntos'])."</h2>";

		$this->getResponse()->setBody($html);
	}
	public function agregarconjuntosAction(){	
		
		$skus = array();

		$log['msj'] = array();
		$log['n_erpproducts'] = 0;
		$log['file_registros_total'] = 0;
		$log['file_registros_correctos'] = array();
		$log['file_registros_correctos_sku'] = array();
		
		$log['file_registros_incorrectos'] = array();
		
		$log['productos_encontrados'] = array();
		$log['productos_actualizados'] = array();
		$log['productos_no_encontrados'] = array();

		$log['conjuntos_encontrados'] = array();
		$log['conjuntos_no_encontrados'] = array();

		$html =  "<h2>Completando conjuntos de attributos</h2>";
		
		
		$filename = realpath(dirname('archivo.csv'));
		$filename .= '/conjuntos.csv';
		
		$html.="<h2>Buscando archivo: $filename</h2>";

		
	

		if(file_exists ($filename)){
			$file = fopen($filename, "r");	
			$html.="<h2>Leyendo archivo...</h2>";
			while(!feof($file)){
	    		$line = fgets($file);
	    		$lineData = explode(',',$line);
	    		$sku = trim($lineData[0]);
	    		$conjunto = trim($lineData[1]);
	    		//$conjunto = str_replace('"',$conjunto);

	    		if($sku && $conjunto){
	    			$log['file_registros_correctos']  [] = $line;				
	    			$log['file_registros_correctos_sku'] [$sku] = $conjunto;					
	    		}else{
	    			$log['file_registros_incorrectos']  [] = $line;
	    		}
	    		$log['file_registros_total'] ++;
			}

			fclose($file);
			$html.="<h2>Leyendo archivo... Finalizado</h2>";
			
		}
		//$n = 0;
		foreach ($log['file_registros_correctos_sku'] as $sku => $value) {
			//if($n++>10)break;
			$html.="<p>procesando sku:$sku:$value </p>";
			$erpProduct = Mage::getModel('corbeta_erpdata/erpproduct')
					->load($sku,'product_id');
			if($erpProduct->getId()){
				$log['productos_encontrados']  [] = $sku;

				$attributeSet = Mage::getModel('eav/entity_attribute_set')->load($value,'attribute_set_name');
				if($attributeSet->getId()){
					$html.="<p>asignando conjunto:".$attributeSet->getId().":".$attributeSet->getAttributeSetName()."</p>";
					$log['conjuntos_encontrados'] [$value] = $value;
					if($attributeSet->getId() != $erpProduct->getAttributeSetId()){
						$erpProduct->setAttributeSetId($attributeSet->getId());
						$erpProduct->save();
						$log['productos_actualizados'] [$sku] = $sku;
					}
					
				}else{
					$html.="<p>conjunto no encontrado:$value</p>";
					$log['conjuntos_no_encontrados'] [$value] = $value;
				}
			
			}else{
				$html.="<p>sku no encontrado:$sku</p>";
				$log['productos_no_encontrados']  [] = $sku;
			}
		}

		$html.="<h2>Proceso Finalizado...</h2>";
		$html.="<h2>Leidos ".$log['file_registros_total']." registros</h2>";
		$html.="<h2>Correctos ".count($log['file_registros_correctos'])." registros</h2>";
		$html.="<h2>Skus ".count($log['file_registros_correctos_sku'])." registros</h2>";
		$html.="<h2>Incorrectos ".count($log['file_registros_incorrectos'])." registros</h2>";
		
		$html.="<h2>Productos Encontrados ".count($log['productos_encontrados'])." registros</h2>";
		$html.="<h2>Productos No Encontrados ".count($log['productos_no_encontrados'])." registros</h2>";
		$html.="<h2>Productos Actualizados ".count($log['productos_actualizados'])." registros</h2>";
		
		$html.="<h2>Conjuntos Encontrados ".count($log['conjuntos_encontrados'])." registros</h2>";
		$html.="<h2>Conjuntos No Encontrados ".count($log['conjuntos_no_encontrados'])." registros</h2>";
		
		foreach($log['conjuntos_no_encontrados'] as $line){
			$html.="<p>$line</p>";
		}
		$this->getResponse()->setBody($html);

	}
	public function agregardescripcionAction(){	
		
		$skus = array();

		$log['msj'] = array();
		$log['n_erpproducts'] = 0;
		$log['file_registros_total'] = 0;
		$log['file_registros_correctos'] = array();
		$log['file_registros_correctos_sku'] = array();
		
		$log['file_registros_incorrectos'] = array();
		
		$log['productos_encontrados'] = array();
		$log['productos_actualizados'] = array();
		$log['productos_no_encontrados'] = array();

		
		$html =  "<h2>Completando descripciones</h2>";
		
		
		$filename = realpath(dirname('consolidado26072015-1.csv'));
		$filename .= '/consolidado26072015-1.csv';
		
		$html.="<h2>Buscando archivo: $filename</h2>";

		
	

		if(file_exists ($filename)){
			$file = fopen($filename, "r");	
			$html.="<h2>Leyendo archivo...</h2>";
			while(!feof($file)){
	    		$line = fgets($file);
	    		$lineData = explode(';',$line);
	    		if(count($lineData)>1){
	    			$sku = trim($lineData[0]);
		    		$descripcion = trim($lineData[1]);
		    		$descripcion = "<p>".$descripcion."</p>";

		    		//$descripcion = str_replace('"',$descripcion);

		    		if($sku && $descripcion){
		    			$log['file_registros_correctos']  [] = $line;				
		    			$log['file_registros_correctos_sku'] [$sku] = $descripcion;					
		    		}else{
		    			$log['file_registros_incorrectos']  [] = $line;
		    		}
	    		}else{
	    			$log['file_registros_incorrectos']  [] = $line;
	    		}
	    		
	    		$log['file_registros_total'] ++;
			}

			fclose($file);
			$html.="<h2>Leyendo archivo... Finalizado</h2>";
			
		}
		//$n = 0;
		foreach ($log['file_registros_correctos_sku'] as $sku => $value) {
			//if($n++>10)break;
			
			$html.="<p>procesando sku:$sku:$value </p>";
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
			if($product && $product->getId()){
				$log['productos_encontrados']  [] = $sku;
				$product->setDescription($value);
				$product->save();
			}else{
				$html.="<p>sku no encontrado:$sku</p>";
				$log['productos_no_encontrados']  [] = $sku;
			}
		}

		$html.="<h2>Proceso Finalizado...</h2>";
		$html.="<h2>Leidos ".$log['file_registros_total']." registros</h2>";
		$html.="<h2>Correctos ".count($log['file_registros_correctos'])." registros</h2>";
		$html.="<h2>Skus ".count($log['file_registros_correctos_sku'])." registros</h2>";
		$html.="<h2>Incorrectos ".count($log['file_registros_incorrectos'])." registros</h2>";
		
		$html.="<h2>Productos Encontrados ".count($log['productos_encontrados'])." registros</h2>";
		$html.="<h2>Productos No Encontrados ".count($log['productos_no_encontrados'])." registros</h2>";
		$html.="<h2>Productos Actualizados ".count($log['productos_actualizados'])." registros</h2>";
		/*
		foreach($log['file_registros_correctos_sku'] as $key => $value){
			$html.="<p>$key</p>";
		}*/
		
		$this->getResponse()->setBody($html);

	}
	public function agregarimagenAction(){	
		
		
		$log['msj'] = array();
		$_n_imagenes = 0;
		$html =  "<h2>Agregando Imagenes</h2>";
		
		$path = realpath(dirname('file.jpg'));
		$magentoProducts = Mage::getModel('catalog/product')->getCollection();


		foreach($magentoProducts as $product){
			$sku = $product->getSku();
			$filename = $path."/imagenes/$sku";

			if(file_exists ($filename.".jpg")){
				$filename = $filename.".jpg";
				$ext = ".jpg";
			}elseif(file_exists ($filename.".JPG")){
				$filename = $filename.".JPG";
				$ext = ".jpg";
			}elseif(file_exists ($filename.".png")){
				$filename = $filename.".png";
				$ext = ".png";
			}elseif(file_exists ($filename.".PNG")){
				$filename = $filename.".PNG";
				$ext = ".png";
			}
			if(file_exists ($filename)){
				$img = $path."/media/import/$sku".$ext;
				if(file_exists ($img)){continue;}
				file_put_contents($img, file_get_contents($filename));
				
				$_n_imagenes++;
				$product->setMediaGallery (array('images'=>array (), 'values'=>array ()));
				$product->addImageToMediaGallery ($img , array ('image','small_image','thumbnail'), false, false);
				
			
				try{
					$product->save();
					Mage::log($_n_imagenes." imagen ok:".$sku,null, 'imagenes.log');
				}
				catch (Exception $e) {
					Mage::log("error:$sku:".$e->getMessage(),null, 'imagenes.log');
				}

				
			}
			
			
		}
	

		$html.="<h2>Proceso Finalizado...</h2>";
		

		Mage::log("total imagenes".$_n_imagenes,null, 'imagenes.log');
		
		$this->getResponse()->setBody($html);

	}

	public function agregarcategoriaAction(){	
		
		$skus = array();

		$log['msj'] = array();
		$log['n_erpproducts'] = 0;
		$log['file_registros_total'] = 0;
		$log['file_registros_correctos'] = array();
		$log['file_registros_correctos_sku'] = array();
		
		$log['file_registros_incorrectos'] = array();
		
		$log['productos_encontrados'] = array();
		$log['productos_actualizados'] = array();
		$log['productos_no_encontrados'] = array();
		$log['producto_x_cat'] = array();

		
		$html =  "<h2>Completando categoria</h2>";
		
		
		$filename = realpath(dirname('listaconjuntos1.csv'));
		$filename .= '/listaconjuntos1.csv';
		
		$html.="<h2>Buscando archivo: $filename</h2>";

		
		$data = array();

		if(file_exists ($filename)){
			$file = fopen($filename, "r");	
			$html.="<h2>Leyendo archivo...</h2>";
			while(!feof($file)){
	    		$line = fgets($file);
	    		$lineData = explode(',',$line);
	    		if(count($lineData)>1){
	    			$setid = trim($lineData[0]);
		    		$categoria = trim($lineData[1]);
		    		

		    		if($setid && $categoria){
		    			$data[$setid]=	$categoria;		
		    		}else{
		    			$log['file_registros_correctos']  [] = $line;
		    		}
	    		}else{
	    			$log['file_registros_incorrectos']  [] = $line;
	    		}
	    		
	    		$log['file_registros_total'] ++;
			}

			fclose($file);
			$html.="<h2>Leyendo archivo... Finalizado</h2>";
			
		}
		
		$magentoProducts = Mage::getModel('catalog/product')->getCollection();
		$n=0;
		foreach($magentoProducts as $product){
			
				if($product->getId()>99){
					//if($n++>100)break;	
					$_setid = $product->getAttributeSetId();
					if(array_key_exists ( $_setid , $data )){
						$log['producto_x_cat'] [] = $product->getSku().":".$_setid.":".$data[$_setid];
						$categories_pd = $product->getCategoryIds();                              
	                    $product->setCategoryIds(array_merge($product->getCategoryIds(),array($data[$_setid])));
	                    $product->setStatus(1);
	                    
					}
					try{
						$product->save();
					}
					catch (Exception $e) {
						Mage::log("error:$sku:".$e->getMessage(),null, 'categorias.log');
					}	
				}
		}

		$html.="<h2>Proceso Finalizado...</h2>";
		$html.="<h2>Leidos ".$log['file_registros_total']." registros</h2>";
		$html.="<h2>Correctos ".count($log['file_registros_correctos'])." registros</h2>";
		$html.="<h2>Skus ".count($log['file_registros_correctos_sku'])." registros</h2>";
		$html.="<h2>Incorrectos ".count($log['file_registros_incorrectos'])." registros</h2>";
		
		$html.="<h2>Productos Encontrados ".count($log['productos_encontrados'])." registros</h2>";
		$html.="<h2>Productos No Encontrados ".count($log['productos_no_encontrados'])." registros</h2>";
		$html.="<h2>Productos Actualizados ".count($log['productos_actualizados'])." registros</h2>";
		
		foreach($data as $key => $value){
			$html.="<p>$key:$value</p>";
		}
		foreach($log['producto_x_cat'] as $key => $value){
			$html.="<p>$value</p>";
		}
		
		$this->getResponse()->setBody($html);

	}

}