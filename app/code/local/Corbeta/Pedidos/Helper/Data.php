<?php

class Corbeta_Pedidos_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static $consecutivo;
	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/create/debug') && Mage::getStoreConfig('corbeta_pedidos/create/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/create/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function consumoLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/create/consumption') 
			&& Mage::getStoreConfig('corbeta_pedidos/create/log_consumption')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/create/log_consumption').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/create/errors')
			&& Mage::getStoreConfig('corbeta_pedidos/create/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/create/log_error').'_'.self::$consecutivo.'.log');
		}
	}
	public static $_LABELS    = array(0=>'Nuevo',
		'BUSINESS_UNIT'=>'Unidad de Negocio',
		'ORDER_DATE'=>'Fecha de Orden',
		'SUBCUST_QUAL1'=>'Zona',
		'CK_CONSEC_MARS_ZON'=>'',
		'CK_CONSEC_MARS'=>'',
		'ORDER_NO'=>'Orden No',
		'CUST_ID'=>'Id Cliente',
		'ADDRESS_SEQ_NUM'=>'Secuencia',
		'CUSTOMER_PO'=>'PO Cliente',
		'PRODUCT_ID'=>'Id Producto',
		'QTY'=>'Cantidad Despachada',
		'CATALOG_NBR'=>'Catalogo',
		'LIST_PRICE'=>'Precio de Lista',
		'ORDER_NO_FROM'=>'Orden No',
		'ADDRESS1'=>'Direccion 1',
		'ADDRESS2'=>'Direccion 2',
		'ACCOUNTING_DT'=>'Fecha',
		'INVOICE_AMOUNT'=>'Facturado',
		'AMOUNT'=>'Ordenado',
		'QTY_ORDERED'=>'Cantidad Ordenado',
		'DESCR'=>'Descripcion',
		'AMOUNT_APPLD_ORIG'=>'Valor Ordenado',
		'VAT_AMT'=>'IVA Ordenado',
		'LC_VLR_IMP_CONSUMO'=>'Ipoconsumo Ordenado',
		'DESCR2'=>'Categoria',
		'IN_FULFILL_STATE'=>'Estado de Línea',
		'ORDER_STATUS'=>'Estado de Orden',
		'ORIGINAL_INVOICE'=>'No Factura',
		'GROSS_EXTENDED_AMT'=>'Subtotal',
		'TOT_DISCOUNT_AMT'=>'Descuento',
		'VAT_AMT_BASE'=>'Iva',
		'USER_AMT1'=>'Ipoconsumo',
		'TOTAL_INVOICED_AMT'=>'Total',
		'TAX_AMT_GROSS_BSE'=>'Vlr Unitario',
		'CK_CONSEC_MARS'=>'',
		);
	public static function getLabel($str){
		
		if(array_key_exists($str, self::$_LABELS)){
			return self::$_LABELS[$str];
		}
		return $str;
	}
	
}
