<?php

class Corbeta_Pedidos_Block_Adminhtml_Sales_Order_Erpinfo extends Mage_Adminhtml_Block_Sales_Order_Abstract
{

	public function getErpInfo(){

		$info = json_decode($this->getOrder()->getErpInfo());
		return $info;
	}
	public function getErpEstado(){
		$estado = json_decode($this->getOrder()->getErpConsulta());
		return $estado;
	}
	
}
