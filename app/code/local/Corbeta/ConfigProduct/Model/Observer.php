<?php

class Corbeta_ConfigProduct_Model_Observer extends Mage_Core_Model_Abstract{
	public function lockAttributes($observer) {
		$event = $observer->getEvent();
		$product = $event->getProduct();
		$product->lockAttribute('setid');
		$product->lockAttribute('business_unit');
		$product->lockAttribute('unit_of_measure');
		$product->lockAttribute('convertion_rate');
		$product->lockAttribute('unit_of_measure_to');
		$product->lockAttribute('product_group1');
		$product->lockAttribute('vlr_impu_consumo_terr');
		$product->lockAttribute('catalog_nbr');
	}
}