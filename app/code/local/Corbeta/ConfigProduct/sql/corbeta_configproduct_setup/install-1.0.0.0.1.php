<?php
/**
 * Id Customer installation script
 *
 * @author Harold Villota
 */

/**
 * @var $installer Mage_Eav_Model_Entity_Setup
 */
 
$installer = $this;
$installer->startSetup();
$installer->addAttribute('catalog_product','setid',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'SETID',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','business_unit',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'BUSINESS_UNIT',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','unit_of_measure',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'UNIT_OF_MEASURE',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','convertion_rate',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'CONVERTION_RATE',
	'visible'     => true,
	'type'     => 'decimal',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','unit_of_measure_to',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'UNIT_OF_MEASURE_TO',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','product_group1',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'PRODUCT_GROUP1',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','vlr_impu_consumo_terr',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'VLR_IMPU_CONSUMO_TERR',
	'visible'     => true,
	'type'     => 'decimal',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));
$installer->addAttribute('catalog_product','catalog_nbr',array (
	'attribute_set' =>  'Default',
	'group' => 'Erp',
	'label'    => 'CATALOG_NBR',
	'visible'     => true,
	'type'     => 'varchar',
	'input'    => 'text',
	'system'   => true,
	'required' => false,
	'user_defined' => false,
));

$installer->endSetup();