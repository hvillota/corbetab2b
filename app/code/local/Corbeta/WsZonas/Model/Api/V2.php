<?php

class Corbeta_WsZonas_Model_Api_V2 extends Corbeta_WsZonas_Model_Api
{
	public function update($data) {
		$helper = Mage::helper('corbeta_wszonas');
		$helper->debugLog(__METHOD__);
		$helper->consumoLog(__METHOD__.":".file_get_contents('php://input'));
		$helper->consumoLog(__METHOD__.":zonas export:".var_export($data,true));
		
		$data_zonas = $data->zona;
		//$helper->debugLog(__METHOD__.":zonas type:".gettype($data_zonas));
		
		if (is_object($data_zonas)) {
			$array_data_zonas = array($data_zonas);
		}else{
			$array_data_zonas = $data_zonas;
		}
				
		$dataParsed = array();
		
		foreach($array_data_zonas as $data_zona){
			//$helper->debugLog(__METHOD__.":data_zona type:".gettype($data_zona));
			$zonaParsed = array();
			
			if (is_object($data_zona)||is_array($data_zona)) {
				$arr_c = get_object_vars($data_zona);
				foreach ($arr_c as $key_c => $value_c) {
					if (!is_array($value_c)&&!is_object($value_c)) {
						//$helper->debugLog(__METHOD__.":k:$key_c v:$value_c");
						$zonaParsed[$key_c]= $value_c;
					}
				}
				if(array_key_exists('CATALOG_NBR',$arr_c)){
					$catalogos = $arr_c['CATALOG_NBR'];
					if (is_array($catalogos)) {
						$zonaParsed['CATALOG_NBR']= $catalogos;
					}else{
						$zonaParsed['CATALOG_NBR']= array($catalogos);
					}
				}
				if(array_key_exists('CUSTOMER_ID',$arr_c)){
					$clientes = $arr_c['CUSTOMER_ID'];
					if (is_array($clientes)) {
						$zonaParsed['CUSTOMER_ID']= $clientes;
					}else{
						$zonaParsed['CUSTOMER_ID']= array($clientes);
					}
				}
				$dataParsed[]=$zonaParsed;
			}else{
				$helper->debugLog(__METHOD__.":Zona is not object");
			}
		}
		//$helper->debugLog(__METHOD__.":DATAPARSED:".var_export($dataParsed,true));
		return parent::update($dataParsed);
	}
} 
 
