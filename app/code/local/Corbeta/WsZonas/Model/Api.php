<?php

class Corbeta_WsZonas_Model_Api extends Mage_Api_Model_Resource_Abstract
{
	
	public function update($data) {
		
		$helper = Mage::helper('corbeta_wszonas');
		$helper->debugLog(__METHOD__);
		$log_resumen = array();
		$log_resumen['msj'] = array();
		$log_resumen['total'] = 0;
		$log_resumen['news'] = 0;
		$log_resumen['updated'] = 0;
		$log_resumen['errors'] = 0;
		$log_resumen['msj'][] = "****** RESUMEN ****";

		if (!is_array($data)) {
			$error = "La informacion debe ser de tipo array";
			$helper->errorLog(__METHOD__.":".$error);
			//$this->_fault('0148', 'El campo CUST_ID es obligatorio');
			return $error;
		}
		
		foreach($data as $data_zona){
			$log_resumen['total']++;
				$SUPPOR_TEAM_CD = $data_zona['SUPPORT_TEAM_CD'];
				$CATALOG_NBR = $data_zona['CATALOG_NBR'];
				$DEFAULT_FLAG = $data_zona['DEFAULT_FLAG'];
				$BUSINESS_UNIT = $data_zona['BUSINESS_UNIT'];
				$CUSTOMER_ID = $data_zona['CUSTOMER_ID'];
				$SUPPORT_TEAM_MBR = $data_zona['SUPPORT_TEAM_MBR'];
				$PHONE_TYPE = $data_zona['PHONE_TYPE'];
				$PHONE = $data_zona['PHONE'];
				$SUPPORT_TEAM_MBR1 = $data_zona['SUPPORT_TEAM_MBR1'];
				$COUNTRY_CODE = $data_zona['COUNTRY_CODE'];

			if(!$SUPPOR_TEAM_CD){
				$_msg = "El campo SUPPOR_TEAM_CD es obligatorio";
				$helper->errorLog(__METHOD__.":$_msg");
				$log_resumen['msj'][] = "$_msg";
				$log_resumen['errors'] ++;
				continue;
			}
				
			$erpZona = Mage::getModel('corbeta_erpdata/erpzona')->load($SUPPOR_TEAM_CD,'suppor_team_cd');
			if($erpZona->getId()){
				$helper->debugLog(__METHOD__.":zona existe:".$erpZona->getId());
				$erpZona->setIsUpdated(true);
				$erpZona->setUpdatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time())));
				
				$_is_new = false;
			}else{
				$helper->debugLog(__METHOD__.":zona no existe");
				$erpZona = Mage::getModel('corbeta_erpdata/erpzona');
				$erpZona->setSupporTeamCd($SUPPOR_TEAM_CD);//varchar 6
				$erpZona->setIsNew(true);
				$erpZona->setIsUpdated(false);
				$erpZona->setCreatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time()))); 
				
				$_is_new = true;
			}
			
			$erpZona->setDefaultFlag($DEFAULT_FLAG); //varchar 1
			$erpZona->setBusinessUnit($BUSINESS_UNIT); //varchar 5
			$erpZona->setSupportTeamMbr($SUPPORT_TEAM_MBR); //varchar 8
			$erpZona->setPhoneType($PHONE_TYPE);  //varchar 4
			$erpZona->setPhone($PHONE); //number(24)
			$erpZona->setSupportTeamMbr1($SUPPORT_TEAM_MBR1); // varchar 8
			$erpZona->setCountryCode($COUNTRY_CODE); //varchar(3)
			
			
				
				try{
					$erpZona->save();
					if($_is_new){
						$log_resumen['news'] ++;
						$log_resumen['msj'][] = "Zona Nueva:".$erpZona->getSupporTeamCd();
					}else{
						$log_resumen['updated'] ++;
						$log_resumen['msj'][] = "Zona Actualizada:".$erpZona->getSupporTeamCd();
					}
				}
				catch (Exception $e) {
					$log_resumen['errors'] ++;
					$log_resumen['msj'][] = "Error Guardando:".$SUPPOR_TEAM_CD.":".$e->getMessage();
					$helper->errorLog(__METHOD__.":error guardando la zona:".$e->getMessage());		
				}
				$erpZonaId = $erpZona->getId();
				$helper->debugLog("zona created:$erpZonaId");
				
				$erpZona->setCustomerId($CUSTOMER_ID); //varchar 15
				//AQUI CODIGO PARA RELACIONAR CUSTOMERS // no hay procedimiento logico para esto
				
				foreach($CATALOG_NBR as $catalogo){
					$helper->debugLog(__METHOD__.":analizando catalogo:$catalogo");	
					$erpCatalog = Mage::getModel('corbeta_erpdata/erpcatalog')->load($catalogo,'catalog_nbr');				
					if($erpCatalog->getId()){
						$helper->debugLog(__METHOD__.":catalogo existe:".$erpCatalog->getId());
						
						$erpCatalogXZonas = Mage::getModel('corbeta_erpdata/erpcaxzo')->getCollection()
						->addFieldToFilter('catalog_id', $erpCatalog->getId())
						->addFieldToFilter('zona_id', $erpZonaId);
						
						if($erpCatalogXZonas->getSize()>0){
							$helper->debugLog(__METHOD__.":la relacion ya existe:");	
						}else{
							$helper->debugLog(__METHOD__.":creando relacion:");	
							$erpCatalogXZonas = Mage::getModel('corbeta_erpdata/erpcaxzo');
							$erpCatalogXZonas->setCatalogId($erpCatalog->getId());
							$erpCatalogXZonas->setZonaId($erpZonaId);
							$erpCatalogXZonas->save();
						}
						
					}else{
						$helper->debugLog(__METHOD__.":catalogo no existe:".$catalogo);	
					}
				}

			
			
		}

		$log_resumen['msj'][] = "Total de Registros Procesados:".$log_resumen['total'];
		$log_resumen['msj'][] = "Registros Nuevos:".$log_resumen['news'];
		$log_resumen['msj'][] = "Registros Actualizados:".$log_resumen['updated'];
		$log_resumen['msj'][] = "Total Errores:".$log_resumen['errors'];
		
		foreach($log_resumen['msj'] as $line){
			$helper->debugLog($line);
		}
		
		return "OK";
		
	}
	
} 
 
