<?php

class Corbeta_WsZonas_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static $consecutivo;
	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wszonas/wszonas/debug') && Mage::getStoreConfig('corbeta_wszonas/wszonas/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wszonas/wszonas/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function consumoLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wszonas/wszonas/consumption') 
			&& Mage::getStoreConfig('corbeta_wszonas/wszonas/log_consumption')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wszonas/wszonas/log_consumption').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wszonas/wszonas/errors')
			&& Mage::getStoreConfig('corbeta_wszonas/wszonas/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wszonas/wszonas/log_error').'_'.self::$consecutivo.'.log');
		}
	}
}
