<?php

class Corbeta_ConvertCatalogos_Adminhtml_ConvertcatalogosController extends Mage_Adminhtml_Controller_Action
{
    
    public function convertAction()
    { 	
		$helper = Mage::helper('corbeta_convertcatalogos');
		$helper->debugLog(__METHOD__);
		
		$erpProducts = Mage::getModel('corbeta_erpdata/erpproduct')->getCollection()
						->addFieldToFilter('is_new', 1);
		if($erpProducts->getSize()>0){
			$n=$erpProducts->getSize();
			$helper->debugLog(__METHOD__.":encontrados $n productos nuevos");
			$resumen_nuevos = $this->_convertirProductos($erpProducts);
		}else{
			$helper->debugLog(__METHOD__.":no hay productos nuevos para convertir");
		}
		
		$erpProducts = Mage::getModel('corbeta_erpdata/erpproduct')->getCollection()
						->addFieldToFilter('is_updated', 1);
		if($erpProducts->getSize()>0){
			$n=$erpProducts->getSize();
			$helper->debugLog(__METHOD__.":encontrados $n productos para actualizar");
			$resumen_actualizados = $this->_convertirProductos($erpProducts);
		}else{
			$helper->debugLog(__METHOD__.":no hay productos para actualizar");
		}
		
		$helper->debugLog("*****  RESUMEN  ******");
		$helper->debugLog("*** PRODUCTOS NUEVOS ***");
		if($resumen_nuevos && count($resumen_nuevos)>0){
			$helper->debugLog("SE ENCONTRARON ".count($resumen_nuevos)." PRODUCTOS NUEVOS");
			foreach($resumen_nuevos as $linea){
				$helper->debugLog($linea);
			}
		}else{
			$helper->debugLog("NO SE ENCONTRARON PRODUCTOS NUEVOS");
		}

		$helper->debugLog("*** PRODUCTOS ACTUALIZADOS ***");
		if($resumen_actualizados && count($resumen_actualizados)>0){
			$helper->debugLog("SE ENCONTRARON ".count($resumen_actualizados)." PRODUCTOS ACTUALIZADOS");
			foreach($resumen_actualizados as $linea){
				$helper->debugLog($linea);
			}
		}else{
			$helper->debugLog("NO SE ENCONTRARON PRODUCTOS ACTUALIZADOS");
		}

        $this->getResponse()->setBody(1);
	
    }
	
	protected function _convertirProductos($erpProducts){
		$helper = Mage::helper('corbeta_convertcatalogos');
		$helper->debugLog(__METHOD__);
		$error = array();
		$resumen = array();
		foreach($erpProducts as $erpProduct){

			$helper->debugLog(__METHOD__.":procesando producto:".$erpProduct->getId());
			$product_id = $erpProduct->getProductId();
			$line_debug = $product_id;
			$helper->debugLog(__METHOD__.":product_id:$product_id:");
			

			if(!$erpProduct->getAttributeSetId()){
				$error[] = "El producto ".$product_id." no tiene conjunto de atributos: ";
				$line_debug .= ", No tiene conjunto de atributos";
				$resumen[] = $line_debug;
				continue;
			}

			$magentoProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $product_id);
			
			if($magentoProduct){
				$line_debug .= ", Producto Existe";
			}else{
				$line_debug .= ", Producto No Existe";
				$magentoProduct = Mage::getModel('catalog/product');
				$magentoProduct->setWebsiteIds(array(1));
				$magentoProduct->setAttributeSetId($erpProduct->getAttributeSetId());
				$magentoProduct->setTypeId('simple');
				$magentoProduct->setCreatedAt(strtotime('now'));
				$magentoProduct->setSku($product_id);
				$magentoProduct->setWeight(0);
				$magentoProduct->setStatus(2);
				$magentoProduct->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
				$magentoProduct->setName($erpProduct->getDescr());
				
				$magentoProduct->setTaxClassId(0);
				$magentoProduct->setDescription($erpProduct->getDescr());
				$magentoProduct->setShortDescription($erpProduct->getDescr());
				$magentoProduct->setStockData(array(
				   'use_config_manage_stock' => 1, 
			   ));
			}
			$helper->debugLog(__METHOD__.":buscando iva...:".$erpProduct->getTaxPct());
			$tax = Mage::getModel('tax/class')->load('IVA_'.$erpProduct->getTaxPct(),'class_name');
			if($tax && $tax->getId()){
				$helper->debugLog(__METHOD__.":asignando impuesto:".$tax->getId().":".$tax->getClassName());
				$magentoProduct->setTaxClassId($tax->getId());
			}

			$helper->debugLog(__METHOD__.":buscando ipoconsumo...:".$erpProduct->getVlrImpuConsumoTerr());
			$tax = Mage::getModel('tax/class')->load('IPO_'.$erpProduct->getVlrImpuConsumoTerr(),'class_name');
			if($tax && $tax->getId()){
				$helper->debugLog(__METHOD__.":asignando impuesto:".$tax->getId().":".$tax->getClassName());
				$magentoProduct->setTaxClassId($tax->getId());
			}
				
			$magentoProduct->setPrice($erpProduct->getListPrice());
			
			
			$magentoProduct->setSetid($erpProduct->getSetid());
			$magentoProduct->setBusinessUnit($erpProduct->getBusinessUnit());
			$magentoProduct->setUnitOfMeasure($erpProduct->getUnitOfMeasure());
			$magentoProduct->setConvertionRate($erpProduct->getConvertionRate());
			$magentoProduct->setUnitOfMeasureTo($erpProduct->getUnitOfMeasureTo());
			$magentoProduct->setProductGroup1($erpProduct->getProductGroup1());
			$magentoProduct->setTaxPct($erpProduct->getTaxPct());
			$magentoProduct->setVlrImpuConsumoTerr($erpProduct->getVlrImpuConsumoTerr());
			$magentoProduct->setCatalogNbr($erpProduct->getCatalogosStr());
			
			try{
				$helper->debugLog(__METHOD__.":guardando producto...");
				$line_debug .= ", Guardando...";
				$magentoProduct->save();
				$erpProduct->setIsNew(false);
				$erpProduct->setIsUpdated(false);
				$erpProduct->save();
				
				$line_debug .= ", OK";
				$resumen[] = $line_debug;
			}
			catch (Exception $e) {
				Zend_Debug::dump($e->getMessage());
				$line_debug .= ", ".$e->getMessage();
				$resumen[] = $line_debug;
				$error[] = "El producto no pudo guardarse: ".$e->getMessage();
			}
			
			
		}
		if(count($error)>0){
			$helper->errorLog(__METHOD__.":".var_export($error,true));
		}
		return $resumen;
	}

}
