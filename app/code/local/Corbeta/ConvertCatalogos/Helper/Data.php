<?php

class Corbeta_ConvertCatalogos_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static $consecutivo;
	
	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_convertcatalogos/products/debug') && Mage::getStoreConfig('corbeta_convertcatalogos/products/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_convertcatalogos/products/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_convertcatalogos/products/errors')
			&& Mage::getStoreConfig('corbeta_convertcatalogos/products/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_convertcatalogos/products/log_error').'_'.self::$consecutivo.'.log');
		}
	}
}
