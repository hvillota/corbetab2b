<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct admin edit tabs
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpproduct_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('corbeta_backcatalogos')->__('Erpproduct'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_erpproduct',
            array(
                'label'   => Mage::helper('corbeta_backcatalogos')->__('Erpproduct'),
                'title'   => Mage::helper('corbeta_backcatalogos')->__('Erpproduct'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_backcatalogos/adminhtml_erpproduct_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve erpproduct entity
     *
     * @access public
     * @return Corbeta_BackCatalogos_Model_Erpproduct
     * @author Ultimate Module Creator
     */
    public function getErpproduct()
    {
        return Mage::registry('current_erpproduct');
    }
}
