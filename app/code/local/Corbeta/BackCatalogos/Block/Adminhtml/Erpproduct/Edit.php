<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct admin edit form
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'corbeta_backcatalogos';
        $this->_controller = 'adminhtml_erpproduct';
        
		$this->_removeButton('delete');
		
		$this->_removeButton('reset');
		
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_erpproduct') && Mage::registry('current_erpproduct')->getId()) {
            return Mage::helper('corbeta_backcatalogos')->__(
                "Edit Erpproduct '%s'",
                $this->escapeHtml(Mage::registry('current_erpproduct')->getDescr())
            );
        } else {
            return Mage::helper('corbeta_backcatalogos')->__('Add Erpproduct');
        }
    }
}
