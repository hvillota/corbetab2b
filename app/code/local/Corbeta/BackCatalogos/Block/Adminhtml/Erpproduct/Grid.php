<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct admin grid block
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpproductGrid');
        $this->setDefaultSort('erp_product_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('corbeta_erpdata/erpproduct')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'erp_product_id',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('Id'),
                'index'  => 'erp_product_id',
                'type'   => 'number'
            )
        );
		 $this->addColumn(
            'setid',
            array(
                'header'    => Mage::helper('corbeta_backcatalogos')->getLabel('SETID'),
                'align'     => 'left',
                'index'     => 'setid',
            )
        );
        $this->addColumn(
            'descr',
            array(
                'header'    => Mage::helper('corbeta_backcatalogos')->getLabel('DESCR'),
                'align'     => 'left',
                'index'     => 'descr',
            )
        );
        
        
        $this->addColumn(
            'business_unit',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('BUSINESS_UNIT'),
                'index'  => 'business_unit',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'product_id',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('PRODUCT_ID'),
                'index'  => 'product_id',
                'type'=> 'text',

            )
        );
		
		 $this->addColumn(
            'unit_of_measure',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('UNIT_OF_MEASURE'),
                'index'  => 'unit_of_measure',
                'type'=> 'text',

            )
        );
		
		$this->addColumn(
            'convertion_rate',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('CONVERTION_RATE'),
                'index'  => 'convertion_rate',
                'type'=> 'text',

            )
        );
		
		$this->addColumn(
            'unit_of_measure_to',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('UNIT_OF_MEASURE_TO'),
                'index'  => 'unit_of_measure_to',
                'type'=> 'text',

            )
        );
		
		$this->addColumn(
            'product_group1',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('PRODUCT_GROUP1'),
                'index'  => 'product_group1',
                'type'=> 'text',

            )
        );
		
		
		$this->addColumn(
            'tax_pct',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('TAX_PCT'),
                'index'  => 'tax_pct',
                'type'=> 'text',

            )
        );
		
        $this->addColumn(
            'list_price',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('LIST_PRICE'),
                'index'  => 'list_price',
                'type'=> 'number',

            )
        );
		
		 $this->addColumn(
            'vlr_impu_consumo_terr',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('VLR_IMPU_CONSUMO_TERR'),
                'index'  => 'vlr_impu_consumo_terr',
                'type'=> 'number',

            )
        );
		
        $this->addColumn(
            'is_new',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('IS_NEW'),
                'index'  => 'is_new',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backcatalogos')->__('Yes'),
                    '0' => Mage::helper('corbeta_backcatalogos')->__('No'),
                )

            )
        );
        $this->addColumn(
            'is_updated',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->getLabel('IS_UPDATED'),
                'index'  => 'is_updated',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backcatalogos')->__('Yes'),
                    '0' => Mage::helper('corbeta_backcatalogos')->__('No'),
                )

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('corbeta_backcatalogos')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('corbeta_backcatalogos')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('corbeta_backcatalogos')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('corbeta_backcatalogos')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('corbeta_backcatalogos')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('corbeta_backcatalogos')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('corbeta_backcatalogos')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('erpproduct');
        
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Corbeta_BackCatalogos_Model_Erpproduct
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
