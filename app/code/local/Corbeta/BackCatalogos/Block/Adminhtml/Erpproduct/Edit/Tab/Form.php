<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
		$helper = Mage::helper('corbeta_backcatalogos');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('erpproduct_');
        $form->setFieldNameSuffix('erpproduct');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'erpproduct_form',
            array('legend' => Mage::helper('corbeta_backcatalogos')->__('Erpproduct'))
        );
		
		
		
        $fieldset->addField(
            'setid',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('SETID'),
                'name'  => 'setid',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'business_unit',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('BUSINESS_UNIT'),
                'name'  => 'business_unit',

           )
        );

        $fieldset->addField(
            'product_id',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('PRODUCT_ID'),
                'name'  => 'product_id',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'descr',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('DESCR'),
                'name'  => 'descr',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'unit_of_measure',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('UNIT_OF_MEASURE'),
                'name'  => 'unit_of_measure',

           )
        );

        $fieldset->addField(
            'convertion_rate',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('CONVERTION_RATE'),
                'name'  => 'convertion_rate',

           )
        );

        $fieldset->addField(
            'unit_of_measure_to',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('UNIT_OF_MEASURE_TO'),
                'name'  => 'unit_of_measure_to',

           )
        );

        $fieldset->addField(
            'product_group1',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('PRODUCT_GROUP1'),
                'name'  => 'product_group1',

           )
        );

        $fieldset->addField(
            'tax_pct',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('TAX_PCT'),
                'name'  => 'tax_pct',

           )
        );

        $fieldset->addField(
            'list_price',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('LIST_PRICE'),
                'name'  => 'list_price',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'vlr_impu_consumo_terr',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('VLR_IMPU_CONSUMO_TERR'),
                'name'  => 'vlr_impu_consumo_terr',

           )
        );
		
		$fieldset->addField(
            'catalogos_str',
            'label',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('CATALOG_NBR'),
                'name'  => 'catalogos_str',

           )
        );
		
		$attribute_sets = $helper->getAttributeSets();
		
		$fieldset->addField(
            'attribute_set_id',
            'select',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('Attribute Set'),
                'name'  => 'attribute_set_id',
            'required'  => true,
            'class' => 'required-entry',

            'values'=> $attribute_sets,
           )
        );
		
		
        $fieldset->addField(
            'is_new',
            'select',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('IS_NEW'),
                'name'  => 'is_new',
            'required'  => true,
			'disabled'  => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backcatalogos')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backcatalogos')->__('No'),
                ),
            ),
           )
        );

        $fieldset->addField(
            'is_updated',
            'select',
            array(
                'label' => Mage::helper('corbeta_backcatalogos')->getLabel('IS_UPDATED'),
                'name'  => 'is_updated',
            'required'  => true,
			'disabled'  => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backcatalogos')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backcatalogos')->__('No'),
                ),
            ),
           )
        );
        
        $formValues = Mage::registry('current_erpproduct')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getErpproductData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getErpproductData());
            Mage::getSingleton('adminhtml/session')->setErpproductData(null);
        } elseif (Mage::registry('current_erpproduct')) {
            $formValues = array_merge($formValues, Mage::registry('current_erpproduct')->getData());
        }
		$erpProduct = Mage::getModel('corbeta_erpdata/erpproduct')->load($formValues['erp_product_id']);
		$formValues['catalogos_str']=$erpProduct->getCatalogosStr();
		
		
		
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
