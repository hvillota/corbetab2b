<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct admin block
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Block_Adminhtml_Erpproduct extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_erpproduct';
        $this->_blockGroup         = 'corbeta_backcatalogos';
        parent::__construct();
        $this->_headerText         = Mage::helper('corbeta_backcatalogos')->__('Erpproduct');
       $this->_removeButton('add');

    }
}
