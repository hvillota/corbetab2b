<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpproduct admin controller
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Adminhtml_Backcatalogos_ErpproductController extends Corbeta_BackCatalogos_Controller_Adminhtml_BackCatalogos
{
    /**
     * init the erpproduct
     *
     * @access protected
     * @return Corbeta_BackCatalogos_Model_Erpproduct
     */
    protected function _initErpproduct()
    {
        $erpproductId  = (int) $this->getRequest()->getParam('id');
        $erpproduct    = Mage::getModel('corbeta_erpdata/erpproduct');
        if ($erpproductId) {
            $erpproduct->load($erpproductId);
        }
        Mage::register('current_erpproduct', $erpproduct);
        return $erpproduct;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backcatalogos')->__('Erp Catalogos'))
             ->_title(Mage::helper('corbeta_backcatalogos')->__('Erpproducts'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit erpproduct - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $erpproductId    = $this->getRequest()->getParam('id');
        $erpproduct      = $this->_initErpproduct();
        if ($erpproductId && !$erpproduct->getId()) {
            $this->_getSession()->addError(
                Mage::helper('corbeta_backcatalogos')->__('This erpproduct no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getErpproductData(true);
        if (!empty($data)) {
            $erpproduct->setData($data);
        }
        Mage::register('erpproduct_data', $erpproduct);
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backcatalogos')->__('Erp Catalogos'))
             ->_title(Mage::helper('corbeta_backcatalogos')->__('Erpproducts'));
        if ($erpproduct->getId()) {
            $this->_title($erpproduct->getDescr());
        } else {
            $this->_title(Mage::helper('corbeta_backcatalogos')->__('Add erpproduct'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new erpproduct action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save erpproduct - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('erpproduct')) {
            try {
                $erpproduct = $this->_initErpproduct();
                $erpproduct->addData($data);
                $erpproduct->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backcatalogos')->__('Erpproduct was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $erpproduct->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setErpproductData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was a problem saving the erpproduct.')
                );
                Mage::getSingleton('adminhtml/session')->setErpproductData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backcatalogos')->__('Unable to find erpproduct to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete erpproduct - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $erpproduct = Mage::getModel('corbeta_erpdata/erpproduct');
                $erpproduct->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backcatalogos')->__('Erpproduct was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was an error deleting erpproduct.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backcatalogos')->__('Could not find erpproduct to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete erpproduct - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $erpproductIds = $this->getRequest()->getParam('erpproduct');
        if (!is_array($erpproductIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backcatalogos')->__('Please select erpproducts to delete.')
            );
        } else {
            try {
                foreach ($erpproductIds as $erpproductId) {
                    $erpproduct = Mage::getModel('corbeta_erpdata/erpproduct');
                    $erpproduct->setId($erpproductId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backcatalogos')->__('Total of %d erpproducts were successfully deleted.', count($erpproductIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was an error deleting erpproducts.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $erpproductIds = $this->getRequest()->getParam('erpproduct');
        if (!is_array($erpproductIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backcatalogos')->__('Please select erpproducts.')
            );
        } else {
            try {
                foreach ($erpproductIds as $erpproductId) {
                $erpproduct = Mage::getSingleton('corbeta_erpdata/erpproduct')->load($erpproductId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpproducts were successfully updated.', count($erpproductIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was an error updating erpproducts.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_NEW change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsNewAction()
    {
        $erpproductIds = $this->getRequest()->getParam('erpproduct');
        if (!is_array($erpproductIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backcatalogos')->__('Please select erpproducts.')
            );
        } else {
            try {
                foreach ($erpproductIds as $erpproductId) {
                $erpproduct = Mage::getSingleton('corbeta_erpdata/erpproduct')->load($erpproductId)
                    ->setIsNew($this->getRequest()->getParam('flag_is_new'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpproducts were successfully updated.', count($erpproductIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was an error updating erpproducts.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_UPDATED change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsUpdatedAction()
    {
        $erpproductIds = $this->getRequest()->getParam('erpproduct');
        if (!is_array($erpproductIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backcatalogos')->__('Please select erpproducts.')
            );
        } else {
            try {
                foreach ($erpproductIds as $erpproductId) {
                $erpproduct = Mage::getSingleton('corbeta_erpdata/erpproduct')->load($erpproductId)
                    ->setIsUpdated($this->getRequest()->getParam('flag_is_updated'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpproducts were successfully updated.', count($erpproductIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backcatalogos')->__('There was an error updating erpproducts.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'erpproduct.csv';
        $content    = $this->getLayout()->createBlock('corbeta_backcatalogos/adminhtml_erpproduct_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'erpproduct.xls';
        $content    = $this->getLayout()->createBlock('corbeta_backcatalogos/adminhtml_erpproduct_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'erpproduct.xml';
        $content    = $this->getLayout()->createBlock('corbeta_backcatalogos/adminhtml_erpproduct_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/corbeta/erpproduct');
    }
}
