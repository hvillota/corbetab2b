<?php
/**
 * Corbeta_BackCatalogos extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackCatalogos
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * BackCatalogos default helper
 *
 * @category    Corbeta
 * @package     Corbeta_BackCatalogos
 * @author      Ultimate Module Creator
 */
class Corbeta_BackCatalogos_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $attribute_sets;
	protected $_LABELS =	array(
					'SETID'=>'Setid del Cliente',
					'BUSINESS_UNIT'=>'Unidad de Negocio',
					'CATALOG_NBR'=>'Catalogos',
					'PRODUCT_ID'=>'EAN',
					'DESCR'=>'Nombre Producto',
					'UNIT_OF_MEASURE'=>'Unidad de Medida',
					'CONVERSION_RATE'=>'Unidad de conversión',
					'CONVERTION_RATE'=>'Unidad de conversión',
					'UNIT_OF_MEASURE_TO'=>'Unidad Medida destino',
					'PRODUCT_GROUP1'=>'Grupo de Producto',
					'TAX_PCT'=>'IVA',
					'LIST_PRICE'=>'Precio de Lista',
					'LC_VLR_IMP_CONSUMO'=>'Impuesto al Consumo',
					'VLR_IMPU_CONSUMO_TERR'=>'Impuesto al Consumo',
					'IS_NEW'=>'Nuevo',
					'IS_UPDATED'=>'Actualizado',
					'CREATED AT'=>'Fecha Creacion',
					'UPDATED AT'=>'Fecha Actualizacion',
		);

	public function getLabel($str){
		if(array_key_exists($str,$this->_LABELS)){
			return $this->_LABELS[$str];
		}else{
			return $str;
		}
		
	}
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
	public function getAttributeSets(){
		if(!isset($this->attribute_sets)){
			$attribute_sets = Mage::getModel('eav/entity_attribute_set')->getCollection()
			->addFieldToFilter('entity_type_id', 4)
			->toArray();
			$result = array();
			$result[] = array('value'=>'','label'=>'Seleccione...');
			foreach($attribute_sets['items'] as $attribute_set){
				$result[] = array('value'=>$attribute_set['attribute_set_id'],
					'label'=>$attribute_set['attribute_set_name']);
			}
			$this->attribute_sets = $result;
		}
		return $this->attribute_sets;
		
	}
}
