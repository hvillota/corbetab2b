<?php

class Corbeta_ConvertClientes_Adminhtml_ConvertclientesController extends Mage_Adminhtml_Controller_Action
{
    
    public function convertAction()
    { 	
		$helper = Mage::helper('corbeta_convertclientes');
		$helper->debugLog(__METHOD__);
		
		
		$erpCustomers = Mage::getModel('corbeta_erpdata/erpcustomer')->getCollection()
						->addFieldToFilter('is_new', 1);
		
		if($erpCustomers->getSize()>0){
			$n=$erpCustomers->getSize();
			$helper->debugLog(__METHOD__.":encontrados $n clientes nuevos");
			$resumen_nuevos = $this->_convertirClientes($erpCustomers);
		}else{
			$helper->debugLog(__METHOD__.":no hay clientes nuevos para convertir");
		}
		
		$erpCustomers = Mage::getModel('corbeta_erpdata/erpcustomer')->getCollection()
						->addFieldToFilter('is_updated', 1);
		if($erpCustomers->getSize()>0){
			$n=$erpCustomers->getSize();
			$helper->debugLog(__METHOD__.":encontrados $n clientes para actualizar");
			$resumen_actualizados = $this->_convertirClientes($erpCustomers);
		}else{
			$helper->debugLog(__METHOD__.":no hay clientes para actualizar");
		}
		
		$helper->debugLog("*****  RESUMEN  ******");
		$helper->debugLog("*** CLIENTES NUEVOS ***");
		if($resumen_nuevos && count($resumen_nuevos)>0){
			$helper->debugLog("SE ENCONTRARON ".count($resumen_nuevos)." CLIENTES NUEVOS");
			foreach($resumen_nuevos as $linea){
				$helper->debugLog($linea);
			}
		}else{
			$helper->debugLog("NO SE ENCONTRARON CLIENTES NUEVOS");
		}

		$helper->debugLog("*** CLIENTES ACTUALIZADOS ***");
		if($resumen_actualizados && count($resumen_actualizados)>0){
			$helper->debugLog("SE ENCONTRARON ".count($resumen_actualizados)." CLIENTES ACTUALIZADOS");
			foreach($resumen_actualizados as $linea){
				$helper->debugLog($linea);
			}
		}else{
			$helper->debugLog("NO SE ENCONTRARON CLIENTES ACTUALIZADOS");
		}
        $this->getResponse()->setBody(1);
	
    }
	
	protected function _convertirClientes($erpCustomers){
		$helper = Mage::helper('corbeta_convertclientes');
		$helper->debugLog(__METHOD__);
		$error = array();
		$resumen = array();
		foreach($erpCustomers as $erpCustomer){
			
			$helper->debugLog(__METHOD__.":procesando cliente:".$erpCustomer->getId());
			$cus_id = $erpCustomer->getCusId();
			$helper->debugLog(__METHOD__.":cus_id:$cus_id:");
			$line_debug = $cus_id;

			$magentoCustomer = Mage::getModel('customer/customer')->getCollection()
			->addAttributeToFilter('cus_id',$cus_id);
			
			$helper->debugLog(__METHOD__.":magento customer size:".$magentoCustomer->getSize());
			
			$newMail = true;
			if($magentoCustomer->getSize()>0){
				$magentoCustomer = $magentoCustomer->getFirstItem();
				$line_debug .= ', Existe';
				$helper->debugLog(__METHOD__.":cliente existe en magento id:".$magentoCustomer->getId());
				$isNew = false;	
				if($erpCustomer->getEmailAddr() == $magentoCustomer->getEmail()){
					$newMail = false;
				}	
			}else{
				$line_debug .= ', No Existe';
				$helper->debugLog(__METHOD__.":cliente no existe en magento:");
				$magentoCustomer = Mage::getModel('customer/customer');
				$store = Mage::app()->getStore(1);
				$magentoCustomer->setWebsiteId(1);
				$magentoCustomer->setStore($store);
				$magentoCustomer->setPassword($erpCustomer->getCusId());
				$magentoCustomer->setCusId($erpCustomer->getCusId());
				$magentoCustomer->setEstado(1);
				$isNew = true;

			}
			
			$magentoCustomer->setEmail($erpCustomer->getEmailAddr());
			$magentoCustomer->setFirstname($erpCustomer->getName());
			$magentoCustomer->setLastname($erpCustomer->getName());
			$magentoCustomer->setSetid($erpCustomer->getSetid());
			$magentoCustomer->setBusinessUnit($erpCustomer->getBusinessUnit());
			$magentoCustomer->setTipoIdentificac($erpCustomer->getTipoIdentificac());
			$magentoCustomer->setCustomerType($erpCustomer->getCustomerType());
			$magentoCustomer->setCustomerGroup($erpCustomer->getCustomerGroup());
			$magentoCustomer->setCusCrAvailable($erpCustomer->getCusCrAvailable());
			$magentoCustomer->setCupoDis($erpCustomer->getCupoDis());
			$magentoCustomer->setHoldCd($erpCustomer->getHoldCd());
			$magentoCustomer->setSupportTeamCd($erpCustomer->getZonasStr());
			$magentoCustomer->setZonaDefault($erpCustomer->getZonaDefault());
			$magentoCustomer->setCatalogNbr($erpCustomer->getCatalogosStr());
			
			if(!$erpCustomer->getZonasStr()){
				$error[]="El ciente ".$erpCustomer->getCusId()." no tiene zonas.:".$erpCustomer->getZonasStr().":";
				$line_debug .= ", No tiene zonas";
			}

			$erpAddresses = Mage::getModel('corbeta_erpdata/erpaddress')->getCollection()
					->addFieldToFilter('erp_customer_id', $erpCustomer->getId());

			$helper->debugLog(__METHOD__.":el erp tiene :".$erpAddresses->getSize().": direcciones");

			if($erpAddresses && $erpAddresses->getSize()>0 && (count($error)==0)){

				try{
					$line_debug .= ', Guardando...';
					$magentoCustomer->save();
					if($isNew || $newMail){
						$newPassword = $magentoCustomer->generatePassword();
						$magentoCustomer->changePassword($newPassword);
						$magentoCustomer->setChangePasswordRequired(true);
						$magentoCustomer->save();	
					}
					
					
					$erpCustomer->setIsNew(false);
					$erpCustomer->setIsUpdated(false);
					$erpCustomer->save();
					$line_debug .= ', OK';
					$customerSaved = true;
				}
				catch (Exception $e) {
					Zend_Debug::dump($e->getMessage());
					$line_debug .= ', '.$e->getMessage();
					$error[] = "El cliente ".$erpCustomer->getCusId()." no pudo guardarse: ".$e->getMessage();
					$customerSaved = false;
				}
			}else{
				$line_debug .= ', no tiene direcciones';
				$error[] = "El cliente ".$erpCustomer->getCusId()." no tiene direcciones: ";				
				$customerSaved = false;
			}

			if($customerSaved){
				try{
					$magentoCustomer->sendPasswordReminderEmail();
				}catch (Exception $e) {
					Zend_Debug::dump($e->getMessage());
					$line_debug .= ', '.$e->getMessage();
					$error[] = "El cliente ".$erpCustomer->getCusId()." no se pudo enviar correo: ".$e->getMessage();
				}
			}
			
			
			
			//DIRECCIONES
			if($customerSaved){
				

				foreach($erpAddresses as $erpAddress){
					
					$helper->debugLog(__METHOD__.":procesando direccion ");
					$magentoCustomerAddress = Mage::getModel('customer/address')->getCollection()
						->addAttributeToFilter('parent_id',$magentoCustomer->getId())
						->addAttributeToFilter('address_seq_num',$erpAddress->getAddressSeqNum());	
						
					if($magentoCustomerAddress->getSize()>0){
						$helper->debugLog(__METHOD__.":magento tiene :".$magentoCustomerAddress->getSize().": direcciones");
						$magentoAddress = $magentoCustomerAddress->getFirstItem();
					}else{
						$helper->debugLog(__METHOD__.":magento no tiene direcciones");
						$magentoAddress = Mage::getModel("customer/address");
						$magentoAddress->setCustomerId($magentoCustomer->getId());
						$magentoAddress->setAddressSeqNum($erpAddress->getAddressSeqNum());
					}
					
					$magentoAddress->setFirstname($magentoCustomer->getFirstname());
					$magentoAddress->setLastname($magentoCustomer->getLastname());
					$magentoAddress->setStreet($erpAddress->getAddress1());
					$magentoAddress->setCity($erpAddress->getDescr());
					$magentoAddress->setErpcity($erpAddress->getCity());
					$magentoAddress->setCountryId('CO');
					$magentoAddress->setTelephone($erpAddress->getPhone());
					$magentoAddress->setFax($erpAddress->getFax());	
					$magentoAddress->setPhone1($erpAddress->getPhone1());
					$magentoAddress->setPostcode('11001');
					
					if($erpAddress->getShipToAddr()=='Y'){
						$magentoAddress->setIsDefaultBilling(1);
						$magentoAddress->setIsDefaultShipping(1);
					}else{
						$magentoAddress->setIsDefaultBilling(0);
						$magentoAddress->setIsDefaultShipping(0);
					}
					$magentoAddress->setSaveInAddressBook('1');
							
					try{
						$line_debug .= ', Guardando Direccion...';
						$magentoAddress->save();
						$line_debug .= ', OK';
					}
					catch (Exception $e) {
						Zend_Debug::dump($e->getMessage());
						$line_debug .= ', '.$e->getMessage();
						$error[]="La direccion no pudo guardarse".$e->getMessage();
					}
				}
				
			}
			$resumen[] = $line_debug;
			
			if(count($error)>0){
				$helper->errorLog(__METHOD__.":".var_export($error,true));
			}
			
		}
		return $resumen;
	}
	

}
