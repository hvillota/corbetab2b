<?php

class Corbeta_WsCatalogo_Model_Api_V2 extends Corbeta_WsCatalogo_Model_Api
{
	public function update($data) {
		
		$helper = Mage::helper('corbeta_wscatalogo');
		$helper->debugLog(__METHOD__);
		$helper->consumoLog(__METHOD__.":".file_get_contents('php://input'));
		$helper->consumoLog(__METHOD__.":wscatalogo export:".var_export($data,true));
		
		$data_productos = $data->producto;
		$helper->debugLog(__METHOD__.":productos export:".var_export($data_productos,true));
		
		if (is_object($data_productos)) {
			$array_data_productos = array($data_productos);
		}else{
			$array_data_productos = $data_productos;
		}
				
		$dataParsed = array();
		
		foreach($array_data_productos as $data_producto){
			$helper->debugLog(__METHOD__.":data_producto type:".gettype($data_producto));
			$productoParsed = array();
			
			if (is_object($data_producto)||is_array($data_producto)) {
				$arr_c = get_object_vars($data_producto);
				foreach ($arr_c as $key_c => $value_c) {
					if (!is_array($value_c)&&!is_object($value_c)) {
						//$helper->debugLog(__METHOD__.":k:$key_c v:$value_c");
						$productoParsed[$key_c]= $value_c;
					}
				}
				if(array_key_exists('CATALOG_NBR',$arr_c)){
					$catalogo = $arr_c['CATALOG_NBR'];
					if (is_array($catalogo)) {
						$productoParsed['CATALOG_NBR']= $catalogo;
					}else{
						$productoParsed['CATALOG_NBR']= array($catalogo);
					}
				}
				$dataParsed[]=$productoParsed;
			}else{
				$helper->debugLog(__METHOD__.":Product is not object");
			}
		}
		//$helper->debugLog(__METHOD__.":DATAPARSED:".var_export($dataParsed,true));
		return parent::update($dataParsed);
	}
} 
 
