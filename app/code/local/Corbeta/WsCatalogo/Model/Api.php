<?php

class Corbeta_WsCatalogo_Model_Api extends Mage_Api_Model_Resource_Abstract
{
	
	public function update($data) {
		
		$helper = Mage::helper('corbeta_wscatalogo');
		$helper->debugLog(__METHOD__);
		
		$log_resumen = array();
		$log_resumen['msj'] = array();
		$log_resumen['total'] = 0;
		$log_resumen['products'] = array();
		$log_resumen['news'] =  array();
		$log_resumen['updated'] =  array();
		$log_resumen['errors'] = 0;
		$log_resumen['msj'][] = "****** RESUMEN ****";
		
		if (!is_array($data)) {
			$error = "La informacion debe ser de tipo array";
			$helper->errorLog($error);
			return $error;
		}
		
		foreach($data as $product){
			$log_resumen['total']++;
				$SETID = $product['SETID'];
				$BUSINESS_UNIT = $product['BUSINESS_UNIT'];
				$PRODUCT_ID = $product['PRODUCT_ID'];
				$DESCR = $product['DESCR'];
				$UNIT_OF_MEASURE = $product['UNIT_OF_MEASURE'];
				$CONVERTION_RATE = $product['CONVERTION_RATE'];
				$UNIT_OF_MEASURE_TO = $product['UNIT_OF_MEASURE_TO'];
				$PRODUCT_GROUP1 = $product['PRODUCT_GROUP_VAT'];
				$TAX_PCT = $product['TAX_PCT'];
				$LIST_PRICE = $product['LIST_PRICE'];
				$VLR_IMPU_CONSUMO_TERR = $product['VLR_IMPU_CONSUMO_TERR'];
				
				$CATALOG_NBR = $product['CATALOG_NBR'];
			
			if(!$PRODUCT_ID){
				$_msg = "El campo PRODUCT_ID es obligatorio";
				$helper->errorLog(__METHOD__.":$_msg");
				$log_resumen['msj'][] = "$_msg";
				$log_resumen['errors'] ++;
				continue;
			}
				$log_resumen['products'][$PRODUCT_ID] = $PRODUCT_ID;
				$erpProduct = Mage::getModel('corbeta_erpdata/erpproduct')
					->load($PRODUCT_ID,'product_id');
					
			if($erpProduct->getId()){
				$helper->debugLog(__METHOD__.":producto existe:".$erpProduct->getProductId());
				$erpProduct->setIsUpdated(true);
				$erpProduct->setUpdatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time())));
				
				//ACTUALIZAR UNIDADES DE NEGOCIO
				$currentBusinnesUnit = $erpProduct->getBusinessUnit();
				if($currentBusinnesUnit){
					$currentBusinnesUnits = explode(',',$currentBusinnesUnit);
					if(!in_array(trim($BUSINESS_UNIT),$currentBusinnesUnits)){
						$currentBusinnesUnits[] = trim($BUSINESS_UNIT);
						$newBusinessUnit = implode(',',$currentBusinnesUnits);
						$erpProduct->setBusinessUnit($newBusinessUnit);	
						$helper->debugLog(__METHOD__.":actualizado unidad de negocio $newBusinessUnit".$erpProduct->getProductId());
					}else{
						$helper->debugLog(__METHOD__.":la unidad de negocio $BUSINESS_UNIT ya existe:".$erpProduct->getProductId());
					}
				}else{
					$erpProduct->setBusinessUnit(trim($BUSINESS_UNIT)); //varchar 40
				}



				$_is_new = false;
			}else{
				$helper->debugLog(__METHOD__.":producto no existe");
				$erpProduct = Mage::getModel('corbeta_erpdata/erpproduct');
				$erpProduct->setProductId($PRODUCT_ID);//varchar 18
				$erpProduct->setIsNew(true);
				$erpProduct->setIsUpdated(false);
				$erpProduct->setBusinessUnit($BUSINESS_UNIT); //varchar 40
				$erpProduct->setCreatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time()))); 
				$_is_new = true;
			}
			
			
			$erpProduct->setSetid($SETID); //varchar 5

			$erpProduct->setDescr($DESCR); //varchar 1
			$erpProduct->setUnitOfMeasure($UNIT_OF_MEASURE); //varchar 1
			$erpProduct->setConvertionRate($CONVERTION_RATE); //varchar 6
			$erpProduct->setUnitOfMeasureTo($UNIT_OF_MEASURE_TO); //varchar 6
			$erpProduct->setProductGroup1($PRODUCT_GROUP1); //varchar 6
			$erpProduct->setTaxPct($TAX_PCT); //varchar 6
			$erpProduct->setListPrice($LIST_PRICE); //varchar 6
			$erpProduct->setVlrImpuConsumoTerr($VLR_IMPU_CONSUMO_TERR); //varchar 6
			
			
				
		try{
			$erpProduct->save();
			if($_is_new){
				$log_resumen['news'] [$PRODUCT_ID] = $PRODUCT_ID;
				$log_resumen['msj'][] = "Producto Nuevo:".$erpProduct->getProductId();
			}else{
				$log_resumen['updated'] [$PRODUCT_ID] = $PRODUCT_ID;
				$log_resumen['msj'][] = "Producto Actualizado:".$erpProduct->getProductId();
			}
		}
		catch (Exception $e) {
			$log_resumen['errors'] ++;
			$log_resumen['msj'][] = "Error Guardando:".$PRODUCT_ID.":".$e->getMessage();
			$helper->errorLog(__METHOD__.":error guardando el producto:".$e->getMessage());		
		}

		$erpProductId = $erpProduct->getId();
		$helper->debugLog("product created:$erpProductId");
	
		$helper->debugLog("CATALOG_NBR type:".gettype($CATALOG_NBR));
		foreach($CATALOG_NBR as $catalogo){
			$helper->debugLog(__METHOD__.":analizando catalogo:$catalogo");	
			$erpCatalog = Mage::getModel('corbeta_erpdata/erpcatalog')->load($catalogo,'catalog_nbr');				
			if($erpCatalog->getId()){
				$helper->debugLog(__METHOD__.":catalogo existe:".$erpCatalog->getId());
			}else{
				$helper->debugLog(__METHOD__.":catalogo no existe:".$catalogo);
				$erpCatalog = Mage::getModel('corbeta_erpdata/erpcatalog');	
				$erpCatalog->setCatalogNbr($catalogo);
				if($erpCatalog->validate()){
					$erpCatalog->save();
					$erpCatalogId = $erpCatalog->getId();
					$helper->debugLog("catalog created:$erpCatalogId");
				}
			}
			
			$erpProductXCatalog = Mage::getModel('corbeta_erpdata/erpprxca')->getCollection()
				->addFieldToFilter('catalog_id', $erpCatalog->getId())
				->addFieldToFilter('product_id', $erpProductId);
				
				if($erpProductXCatalog->getSize()>0){
					$helper->debugLog(__METHOD__.":la relacion ya existe:");	
				}else{
					$helper->debugLog(__METHOD__.":creando relacion:");	
					$erpProductXCatalog = Mage::getModel('corbeta_erpdata/erpprxca');
					$erpProductXCatalog->setCatalogId($erpCatalog->getId());
					$erpProductXCatalog->setProductId($erpProductId);
					$erpProductXCatalog->save();
				}
				
			}
					
		}
		
		$log_resumen['msj'][] = "Total de Registros Recibidos:".$log_resumen['total'];
		$log_resumen['msj'][] = "Productos Procesados:".count($log_resumen['products']);
		$log_resumen['msj'][] = "Productos Nuevos:".count($log_resumen['news']);
		$log_resumen['msj'][] = "Productos Actualizados:".count($log_resumen['updated']);
		$log_resumen['msj'][] = "Total Errores:".$log_resumen['errors'];
		
		foreach($log_resumen['msj'] as $line){
			$helper->debugLog($line);
		}

		return "OK";
		
	}
	
} 
 
