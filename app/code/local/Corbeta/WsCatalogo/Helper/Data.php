<?php

class Corbeta_WsCatalogo_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static $consecutivo;
	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd");
		if(Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/debug') && Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function consumoLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd");
		if(Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/consumption') 
			&& Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_consumption')){
			$consecutivo = date("Ymd_His");
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_consumption').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd");
		if(Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/errors')
			&& Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wscatalogo/wscatalogo/log_error').'_'.self::$consecutivo.'.log');
		}
	}
}
