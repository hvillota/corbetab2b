<?php

class Corbeta_WsClientes_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $errors = array();
	public static $consecutivo;

	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wsclientes/wsclientes/debug') && Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function consumoLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wsclientes/wsclientes/consumption') 
			&& Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_consumption')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_consumption').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_wsclientes/wsclientes/errors')
			&& Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_wsclientes/wsclientes/log_error').'_'.self::$consecutivo.'.log');
		}
		$this->errors[] = $str;
	}
	public function hasErrors(){
		if(count($this->errors)>0){
			return true;
		}else{
			return false;
		}
	}
	public function getErrors(){
		return $this->errors; 
	}
}
