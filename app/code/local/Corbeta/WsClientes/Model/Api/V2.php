<?php

class Corbeta_WsClientes_Model_Api_V2 extends Corbeta_WsClientes_Model_Api
{
	public function update($data) {
		$helper = Mage::helper('corbeta_wsclientes');
		$helper->debugLog(__METHOD__);
		$helper->consumoLog(__METHOD__.":".file_get_contents('php://input'));
		$helper->consumoLog(__METHOD__.":clientes export:".var_export($data,true));
		
		$data_clientes = $data->cliente;
		$helper->debugLog(__METHOD__.":clientes type:".gettype($data_clientes));
		
		if (is_object($data_clientes)) {
			$array_data_clientes = array($data_clientes);
		}else{
			$array_data_clientes = $data_clientes;
		}
				
		$dataParsed = array();
		
		foreach($array_data_clientes as $data_customer){
			$helper->debugLog(__METHOD__.":data_customer type:".gettype($data_customer));
			$customerParsed = array();
			$customerParsed['DIRECCIONES'] = array();
			if (is_object($data_customer)||is_array($data_customer)) {
				$arr_c = get_object_vars($data_customer);
				foreach ($arr_c as $key_c => $value_c) {
					if (!is_array($value_c)&&!is_object($value_c)) {
						//$helper->debugLog(__METHOD__.":k:$key_c v:$value_c");
						$customerParsed[$key_c]= $value_c;
					}
				}
				if(array_key_exists('direccion',$arr_c)){
					$data_address = $arr_c['direccion'];
					//$helper->debugLog(__METHOD__.":data_address type:".gettype($data_address));
					$address = array();
					if (is_object($data_address)) {
						$array_data_address = array($data_address);
					}else{
						$array_data_address = $data_address;
					}
					foreach($array_data_address as $address_o){
						//$helper->debugLog(__METHOD__.":address_o type:".gettype($address_o));
						if (is_object($address_o)||is_array($address_o)) {
							$arr_d = get_object_vars($address_o);
							foreach ($arr_d as $key_d => $value_d) {
								if (!is_object($value_d)) {
									//$helper->debugLog(__METHOD__.":  k:$key_d v:$value_d");
									$address[$key_d]= $value_d;
								}
							}
							$customerParsed['DIRECCIONES'][]=$address;
						}
					}
				}
				if(array_key_exists('SUPPORT_TEAM_CD',$arr_c)){
					$zonas = $arr_c['SUPPORT_TEAM_CD'];
					if (is_array($zonas)) {
						$customerParsed['SUPPORT_TEAM_CD']= $zonas;
					}else{
						$customerParsed['SUPPORT_TEAM_CD']= array($zonas);
					}
				}
				$dataParsed[]=$customerParsed;
			}else{
				$helper->debugLog(__METHOD__.":Customer is not object");
			}
		}
		//$helper->debugLog(__METHOD__.":DATAPARSED:".var_export($dataParsed,true));
		return parent::update($dataParsed);
		
	}
} 
 
