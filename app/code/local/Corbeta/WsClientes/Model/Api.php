<?php

class Corbeta_WsClientes_Model_Api extends Mage_Api_Model_Resource_Abstract
{
	
	public function update($data) {
		
		$helper = Mage::helper('corbeta_wsclientes');
		$helper->debugLog(__METHOD__);
		
		$log_resumen = array();
		$log_resumen['msj'] = array();
		$log_resumen['total'] = 0;
		$log_resumen['news'] = 0;
		$log_resumen['updated'] = 0;
		$log_resumen['errors'] = 0;
		$log_resumen['msj'][] = "****** RESUMEN ****";

		if (!is_array($data)) {
			$error = "La informacion debe ser de tipo array";
			$helper->errorLog(__METHOD__.":".$error);
			return $error;
		}
		
		foreach($data as $customer){
			$log_resumen['total']++;
				$_SETID = $customer['SETID'];
				$_BUSINESS_UNIT = $customer['BUSINESS_UNIT'];
				$_NAME = $customer['NAME'];
				$_TIPO_IDENTIFICAC = $customer['TIPO_IDENTIFICAC'];
				$_CUSTOMER_TYPE = $customer['CUSTOMER_TYPE'];
				$_SUPPORT_TEAM_CD = $customer['SUPPORT_TEAM_CD'];
				$_CUSTOMER_GROUP = $customer['CUSTOMER_GROUP'];
				$_CUS_CR_AVAILABLE = $customer['CUST_CR_AVAILABLE'];
				$_CUPO_DIS = $customer['CUPO_DIS'];
				$_HOLD_CD = $customer['HOLD_CD'];
				$_CUS_ID = $customer['CUST_ID'];
				$_EMAIL_ADDR = $customer['EMAIL_ADDR'];
				$_DIRECCIONES = $customer['DIRECCIONES'];
				$_DEFAULT_FLAG = $customer['DEFAULT_FLAG'];
				
				$_CK_FLAGMVTA = $customer['CK_FLAGMVTA'];
			
			/** valores flag 	
				0= Cliente Nuevo
				1= Modifica Cabecera
				2= Nueva Zona
				3= Inactiva Zona
				4= Nueva direccion
				5= Modifica Direccion
			*/
			
			if(!$_CUS_ID){
				$_msg = "El campo CUST_ID es obligatorio";
				$helper->errorLog(__METHOD__.":$_msg");
				$log_resumen['msj'][] = "$_msg";
				$log_resumen['errors'] ++;
				continue;
			}
			if(!$_EMAIL_ADDR){
				$_msg = ":El campo _EMAIL_ADDR es obligatorio CUST_ID:".$_CUS_ID;
				$helper->errorLog(__METHOD__.":$_msg");
				$log_resumen['msj'][] = "$_msg";
				$log_resumen['errors'] ++;
				continue;
			}
			$erpCustomer = Mage::getModel('corbeta_erpdata/erpcustomer')->load($_CUS_ID,'cus_id');
			if($erpCustomer->getId()){
				$helper->debugLog(__METHOD__.":cliente existe:".$erpCustomer->getId());
				$erpCustomer->setIsUpdated(true);
				$erpCustomer->setUpdatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time())));
				$_is_new = false;		
			}else{
				$helper->debugLog(__METHOD__.":$_CUS_ID:cliente no existe:");				
				//HFV Sequita esta validacion para que cree el cliente en todo caso flag
				/*
				if($_CK_FLAGMVTA!='0'){
					$_msg = ":Se solicita procesar un cliente que no existe CUST_ID:".$_CUS_ID;
					$helper->errorLog(__METHOD__.":$_msg");
					$log_resumen['msj'][] = "$_msg";
					$log_resumen['errors'] ++;
					continue;
				}
				*/
				$erpCustomer = Mage::getModel('corbeta_erpdata/erpcustomer');
				$erpCustomer->setCusId($_CUS_ID); //varchar(15)
				$erpCustomer->setIsNew(true);
				$erpCustomer->setIsUpdated(false);
				$erpCustomer->setCreatedAt(date("Y-m-d H:i:s",Mage::getModel('core/date')->timestamp(time()))); 
				$_is_new = true;
			}
				
			$erpCustomer->setSetid($_SETID);//varchar 5
			$erpCustomer->setBusinessUnit($_BUSINESS_UNIT); //varchar 5
			$erpCustomer->setName($_NAME); //varchar 40
			$erpCustomer->setTipoIdentificac($_TIPO_IDENTIFICAC); //varchar 1
			$erpCustomer->setCustomerType($_CUSTOMER_TYPE); //varchar 1
			$erpCustomer->setCustomerGroup($_CUSTOMER_GROUP);  //varchar 10
			$erpCustomer->setCusCrAvailable($_CUS_CR_AVAILABLE); //decimal(13,2)
			$erpCustomer->setCupoDis($_CUPO_DIS); //decimal(28,3)
			$erpCustomer->setHoldCd($_HOLD_CD); //varchar(6)
			$erpCustomer->setEmailAddr($_EMAIL_ADDR); //varchar(55)
			
			
				//HFV Sequita esta validacion para que guarde el cliente en todo caso flag
				
				//if($_CK_FLAGMVTA=='0' || $_CK_FLAGMVTA=='1'){

					try{
						$erpCustomer->save();
						if($_is_new){
							$log_resumen['news'] ++;
							$log_resumen['msj'][] = ":$_CUS_ID:Cliente Nuevo:".$erpCustomer->getCusId();
						}else{
							$log_resumen['updated'] ++;
							$log_resumen['msj'][] = ":$_CUS_ID:Cliente Actualizado:".$erpCustomer->getCusId();
						}
					}
					catch (Exception $e) {
						$log_resumen['errors'] ++;
						$log_resumen['msj'][] = "Error Guardando:".$_CUS_ID.":".$e->getMessage();
						$helper->errorLog(__METHOD__.":$_CUS_ID:error guardando el cliente:".$e->getMessage());		
					}

				//}

				$erpCustomerId = $erpCustomer->getId();
				$helper->debugLog(__METHOD__.":$_CUS_ID:customer saved:$erpCustomerId");
				
				if($_CK_FLAGMVTA=='0' || $_CK_FLAGMVTA=='4' || $_CK_FLAGMVTA=='5'){
				
					foreach($_DIRECCIONES as $address){
					
						$ADDRESS_SEQ_NUM = $address['ADDRESS_SEQ_NUM'];
						$CITY = $address['CITY'];
						$DESCR = $address['DESCR'];
						$ADDRESS1 = $address['ADDRESS1'];
						$PHONE = $address['PHONE'];
						$FAX = $address['FAX'];
						$PHONE1 = $address['PHONE1'];
						$SHIP_TO_ADDR = $address['SHIP_TO_ADDR'];
						
						if(!$ADDRESS_SEQ_NUM){
							$helper->errorLog(__METHOD__.":$_CUS_ID: no se procesa la secuencia, el dato ADDRESS_SEQ_NUM es obligatorio");	
							continue;
						}
						if(!$PHONE){
							$helper->errorLog(__METHOD__.":$_CUS_ID: no se procesa la secuencia $ADDRESS_SEQ_NUM, el dato PHONE es obligatorio");	
							continue;
						}
						if(!$CITY){
							$helper->errorLog(__METHOD__.":$_CUS_ID: no se procesa la secuencia $ADDRESS_SEQ_NUM, el dato CITY es obligatorio");	
							continue;
						}
						if(!$DESCR){
							$helper->errorLog(__METHOD__.":$_CUS_ID: no se procesa la secuencia $ADDRESS_SEQ_NUM, el dato DESCR es obligatorio");	
							continue;
						}
						if(!$ADDRESS1){
							$helper->errorLog(__METHOD__.":$_CUS_ID: no se procesa la secuencia $ADDRESS_SEQ_NUM, el dato ADDRESS1 es obligatorio");	
							continue;
						}

						$erpAddress = Mage::getModel('corbeta_erpdata/erpaddress')->getCollection()
							->addFieldToFilter('erp_customer_id', $erpCustomerId)
							->addFieldToFilter('address_seq_num', $ADDRESS_SEQ_NUM);
							
						if($erpAddress->getSize()>0){
							$erpAddress=$erpAddress->getFirstItem();
							if($_CK_FLAGMVTA=='4'){
								$helper->errorLog(__METHOD__.":$_CUS_ID:se solicita agregar una direccion que ya existe:$ADDRESS_SEQ_NUM");	
								continue;
							}
						}else{
							if($_CK_FLAGMVTA=='5'){
								$helper->errorLog(__METHOD__.":$_CUS_ID:no se puede modificar una direccion que no existe:$ADDRESS_SEQ_NUM");	
								continue;
							}
							$helper->debugLog(__METHOD__.":direccion nueva:");	
							$erpAddress = Mage::getModel('corbeta_erpdata/erpaddress');
							$erpAddress->setErpCustomerId($erpCustomerId);
							$erpAddress->setAddressSeqNum($ADDRESS_SEQ_NUM);
						}
						
						$erpAddress->setCity($CITY);
						$erpAddress->setDescr($DESCR);
						$erpAddress->setAddress1($ADDRESS1);
						$erpAddress->setPhone($PHONE);
						$erpAddress->setFax($FAX);
						$erpAddress->setPhone1($PHONE1);
						$erpAddress->setShipToAddr($SHIP_TO_ADDR);
					
						
						try{
							$erpAddress->save();
							$erpAddressId = $erpAddress->getId();
							$helper->debugLog(__METHOD__.":$_CUS_ID:address created:$erpAddressId for customer:$erpCustomerId");
								
						}
						catch (Exception $e) {
							$log_resumen['errors'] ++;
							$log_resumen['msj'][] = "Error Guardando Direccion:$_CUS_ID:sec:$ADDRESS_SEQ_NUM:".$e->getMessage();
							$helper->errorLog(__METHOD__."Error Guardando Direccion:$_CUS_ID:sec:$ADDRESS_SEQ_NUM:".$e->getMessage());		
						}

						
					}
				}
				
				if($_CK_FLAGMVTA=='0' || $_CK_FLAGMVTA=='2' || $_CK_FLAGMVTA=='3'){
					foreach($_SUPPORT_TEAM_CD as $zona){
						$helper->debugLog(__METHOD__.":$_CUS_ID:analizando zona:$zona");	
						$erpZona = Mage::getModel('corbeta_erpdata/erpzona')->load($zona,'suppor_team_cd');				
						if($erpZona->getId()){
							$helper->debugLog(__METHOD__.":$_CUS_ID:zona existe:".$erpZona->getId());
							
							$erpCustomerXZona = Mage::getModel('corbeta_erpdata/erpcuxzo')->getCollection()
							->addFieldToFilter('customer_id', $erpCustomerId)
							->addFieldToFilter('zona_id', $erpZona->getId());
							
							if($erpCustomerXZona->getSize()>0){
								if($_CK_FLAGMVTA=='2'){
									$helper->errorLog(__METHOD__.":$_CUS_ID:se solicita asignar una zona que ya existe:");
									continue;
								}
								if($_CK_FLAGMVTA=='3'){
									$helper->debugLog(__METHOD__.":la relacion ya existe. procediendo a eliminarla:");	
									try{
										$_model = $erpCustomerXZona->getFirstItem();
										$_model->delete();
									}
									catch (Exception $e) {
										Zend_Debug::dump($e->getMessage());
										$helper->errorLog(__METHOD__.":$_CUS_ID:error eliminando la zona:".$e->getMessage());		
									}
									
									$helper->debugLog(__METHOD__.":$_CUS_ID:eliminado ok:");	
								}								
							}else{
								if($_CK_FLAGMVTA=='0' || $_CK_FLAGMVTA=='2'){
									$helper->debugLog(__METHOD__.":creando relacion:");	
									$erpCustomerXZona = Mage::getModel('corbeta_erpdata/erpcuxzo');
									$erpCustomerXZona->setCustomerId($erpCustomerId);
									$erpCustomerXZona->setZonaId($erpZona->getId());
									$erpCustomerXZona->setDefault($_DEFAULT_FLAG);
									$erpCustomerXZona->save();
								}
								if($_CK_FLAGMVTA=='3'){
									$helper->errorLog(__METHOD__.":$_CUS_ID:se solicta desasignar una zona que aun no ha sido asignada:");	
								}
							}
							
						}else{
							$helper->errorLog(__METHOD__.":$_CUS_ID:la zona no existe:".$zona);
						}
					}
				}
				
			
		}

		if($helper->hasErrors()){
			$helper->debugLog("errores:".var_export($helper->getErrors(),true));
		}

		$log_resumen['msj'][] = "Total de Registros Procesados:".$log_resumen['total'];
		$log_resumen['msj'][] = "Registros Nuevos:".$log_resumen['news'];
		$log_resumen['msj'][] = "Registros Actualizados:".$log_resumen['updated'];
		$log_resumen['msj'][] = "Total Errores:".$log_resumen['errors'];
		
		foreach($log_resumen['msj'] as $line){
			$helper->debugLog($line);
		}
		if($helper->hasErrors()){
			return implode(';',$helper->getErrors());
		}else{
			return "OK";	
		}
		
	}
	
} 
 
