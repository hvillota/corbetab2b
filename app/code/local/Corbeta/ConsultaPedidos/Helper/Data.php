<?php

class Corbeta_ConsultaPedidos_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static $consecutivo;
	public static $_ESTADO_IN_FULFILL_STATE    = array(0=>'Nuevo',
		10=>'Validacion Cartera',
		20=>'Validacion Inventario',
		30=>'Validacion Inventario',
		40=>'Validacion Inventario',
		50=>'Validacion Inventario',
		60=>'Validacion Inventario',
		70=>'Facturado',
		90=>'Cancelado');

	public static  $_ESTADO_ORDER_STATUS    = array('O'=>'Abierto',
		'C'=>'Cerrado',
		'P'=>'Pendiente',
		'X'=>'Cancelado');

	public function debugLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/update/debug') && Mage::getStoreConfig('corbeta_pedidos/update/log_debug')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/update/log_debug').'_'.self::$consecutivo.'.log');
		}
	}
	public function consumoLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/update/consumption') 
			&& Mage::getStoreConfig('corbeta_pedidos/update/log_consumption')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/update/log_consumption').'_'.self::$consecutivo.'.log');
		}
	}
	public function errorLog($str){
		if(!self::$consecutivo)self::$consecutivo = date("Ymd_His");
		if(Mage::getStoreConfig('corbeta_pedidos/update/errors')
			&& Mage::getStoreConfig('corbeta_pedidos/update/log_error')){
			Mage::log($str,null,Mage::getStoreConfig('corbeta_pedidos/update/log_error').'_'.self::$consecutivo.'.log');
		}

	}
	public static function getEstadoLinea($estado){
		
		if(array_key_exists($estado, self::$_ESTADO_IN_FULFILL_STATE)){
			return self::$_ESTADO_IN_FULFILL_STATE[$estado];
		}
		return "Sin estado";
	}
}
