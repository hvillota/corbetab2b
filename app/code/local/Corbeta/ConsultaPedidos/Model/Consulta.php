<?php

class Corbeta_ConsultaPedidos_Model_Consulta extends Mage_Core_Model_Abstract{

	 

	public function consultaPedido($data,$customer=null) {
		
		$helper = Mage::helper('corbeta_consultapedidos');
		$helper->debugLog(__METHOD__);
		$log_resumen = array();
		$log_resumen[] = "****** RESUMEN ****";
		if(!Mage::getStoreConfig('corbeta_pedidos/update/enabled')){
			return;
		}
        
		$corbeta_url = Mage::getStoreConfig('corbeta_pedidos/update/url');		
		if(!$corbeta_url){
			$helper->debugLog(__METHOD__.':17 no hay url:');
			return;
		}

		$log_resumen[] = "Consultando pedidos para el cliente $customer";
		

		$respuesta = self::send($corbeta_url,array($data));
		if(!$respuesta)return;
		$log_resumen[] = "Respuesta:";
		$_MsgData = $respuesta->MsgData;
		$_Transaction = $_MsgData->Transaction;
		$_CK_ROOT_WS = $_Transaction->CK_ROOT_WS;
		$_CK_RESPEDIDOSMG = $_CK_ROOT_WS->CK_RESPEDIDOSMG;
		
		$helper->debugLog(__METHOD__.':25:');
		$arrayHistory= array();
		$arrayEstado= array();

		$facturas = array();
		$arrayImpuestos = array();
		$arrayDescuentos = array();

		foreach($_CK_RESPEDIDOSMG as $item){
			try{
				$helper->debugLog(__METHOD__.':28:'.var_export($item,true));
				
				if($item->CK_CONSEC_MARS->_ != '200')continue;
				
				

				$info = array();
				$infoItem = array();
				$log_resumen[] = "Recibida Orden:".$item->ORDER_NO->_." Producto".$item->PRODUCT_ID->_. " procesando...";
				$infoItem['ORDER_NO'] = $item->ORDER_NO->_;
				$info['ORDER_NO_FROM'] = $item->ORDER_NO_FROM->_;
				$infoItem['ORIGINAL_INVOICE'] = $item->ORIGINAL_INVOICE->_;
				$info['ORDER_DATE'] = $item->ORDER_DATE->_;
				$info['ADDRESS1'] = $item->ADDRESS1->_;
				$info['ADDRESS2'] = $item->ADDRESS2->_;
				$infoItem['ACCOUNTING_DT'] = $item->ACCOUNTING_DT->_;
				$info['INVOICE_AMOUNT'] = $item->INVOICE_AMOUNT->_;
				$info['AMOUNT'] = $item->AMOUNT->_;
				$infoItem['QTY_ORDERED'] = $item->QTY_ORDERED->_;
				$infoItem['PRODUCT_ID'] = $item->PRODUCT_ID->_;
				$infoItem['DESCR'] = $item->DESCR->_;
				$infoItem['AMOUNT_APPLD_ORIG'] = $item->AMOUNT_APPLD_ORIG->_;
				$infoItem['VAT_AMT'] = $item->VAT_AMT->_;
				$infoItem['LC_VLR_IMP_CONSUMO'] = $item->LC_VLR_IMP_CONSUMO->_;
				$infoItem['DESCR2'] = $item->DESCR2->_;
				$infoItem['IN_FULFILL_STATE'] = $item->IN_FULFILL_STATE->_;
				$infoItem['QTY'] = $item->QTY->_;
				$info['ORDER_STATUS'] = $item->ORDER_STATUS->_;
				$infoItem['GROSS_EXTENDED_AMT'] = $item->GROSS_EXTENDED_AMT->_;//valor sin impuestos factura linea
				$infoItem['TOT_DISCOUNT_AMT'] = $item->TOT_DISCOUNT_AMT->_;//valor descuentos factura linea
				$infoItem['VAT_AMT_BASE'] = $item->VAT_AMT_BASE->_;//iva factura linea
				$infoItem['USER_AMT1'] = $item->USER_AMT1->_;//ipoconsumo factura linea
				$infoItem['TOTAL_INVOICED_AMT'] = $item->TOTAL_INVOICED_AMT->_;//total linea
				$infoItem['TAX_AMT_GROSS_BSE'] = $item->TAX_AMT_GROSS_BSE->_;//valor unitario
				$infoItem['CK_CONSEC_MARS'] = $item->CK_CONSEC_MARS->_;
				
				Mage::log(__METHOD__.':47 info:'.var_export($info,true));

				$helper->debugLog(__METHOD__.':buscando la orden:'.$item->ORDER_NO_FROM->_);

				$orden = Mage::getModel('sales/order')->loadByIncrementId($item->ORDER_NO_FROM->_);
			
				
				if(!$orden->getId()){
					$log_resumen[] = "No existe registro para la orden :".$item->ORDER_NO_FROM->_." Producto:".$item->PRODUCT_ID->_;
					$helper->debugLog(__METHOD__.':no se encontro la orden:'.$item->ORDER_NO_FROM->_);
					continue;
				}
				//concatenando facturas
				if (!array_key_exists($orden->getId(),$facturas)){
					$facturas[$orden->getId()] = array();
				}

				$facturas[$orden->getId()][$item->ORIGINAL_INVOICE->_] = $item->ORIGINAL_INVOICE->_;


				$helper->debugLog(__METHOD__.':buscando el producto:'.$infoItem['PRODUCT_ID']);
				$orden_item = Mage::getModel('sales/order_item')->getCollection()
								->addFieldToFilter('order_id', $orden->getId())
								->addFieldToFilter('sku', $infoItem['PRODUCT_ID']);
				
				if(!array_key_exists($orden->getId(), $arrayHistory)){
					$arrayHistory[$orden->getId()] = array();	
				}
				

				if($orden_item->getSize()>1){
					$helper->debugLog(__METHOD__.":49 error: la consulta arroja mas de un order item");
					$arrayHistory[$orden->getId()][] = "error: la consulta arroja mas de un order item producto:".$infoItem['PRODUCT_ID'];

				}else{
					$orden_item = $orden_item->getFirstItem();
					if(!$orden_item->getId()){
						$helper->debugLog(__METHOD__.':no se encontro la linea orden para producto:'.$infoItem['PRODUCT_ID']);
						continue;
					}
					$arrayHistory[$orden->getId()][] = $infoItem['PRODUCT_ID']." Estado :".$infoItem['IN_FULFILL_STATE'];
					$arrayEstado[$orden->getId()] = $info;
					
					
					$orden_item->setErpConsulta(json_encode($infoItem));
					
					//qty facturado
					if ($infoItem['QTY']):
						$orden_item->setQtyInvoiced($infoItem['QTY']);
					endif;

					//iva
					$impuestos = 0;
					if ($infoItem['VAT_AMT_BASE']):
						$impuestos += $infoItem['VAT_AMT_BASE'];	
					endif;
					//ipconsumo
					if ($infoItem['USER_AMT1']):
						$impuestos += $infoItem['USER_AMT1'];
					endif;
					//impuestos
					if ($impuestos>0):
						$orden_item->setTaxAmount($impuestos);
						
					endif;

					//totalizando impuestos

					if (!array_key_exists($orden->getId(),$arrayImpuestos)){
						$arrayImpuestos[$orden->getId()] = 0;
					}
					$arrayImpuestos[$orden->getId()] += $impuestos;//totalizando impuestos
					
					//totalizando descuentos
					if (!array_key_exists($orden->getId(),$arrayDescuentos)){
						$arrayDescuentos[$orden->getId()] = 0;
					}
					$arrayDescuentos[$orden->getId()] += (-1*$infoItem['TOT_DISCOUNT_AMT']);


					//total facturado sin impuestos por linea
					if ($infoItem['GROSS_EXTENDED_AMT']):
						$orden_item->setRowTotal($infoItem['GROSS_EXTENDED_AMT']);
					endif;

					//descuentos
					if ($infoItem['TOT_DISCOUNT_AMT']):
						$orden_item->setDiscountAmount(-1*$infoItem['TOT_DISCOUNT_AMT']);
					endif;

					$orden_item->save();
					$log_resumen[] = "Se actualizo la orden:".$item->ORDER_NO_FROM->_." Producto:".$item->PRODUCT_ID->_;
				}
							

			}catch(Exception $e){
				Mage::log(__METHOD__.":52:".$e->getMessage());
			}

		}
		

		foreach($arrayHistory as $id => $comments){
			$orden = Mage::getModel('sales/order')->load($id);
			$str = "";
			foreach($comments as $comment){
				$str.= $comment." - ";
			}
			if($str!=""){
				$orden->addStatusHistoryComment($str);
				$orden->save();	
			}
			
		}
		foreach($arrayEstado as $id => $info){
			$orden = Mage::getModel('sales/order')->load($id);

		$facturasStr="";	
		foreach($facturas[$id] as $key => $value){
			$facturasStr .= $key.",";
		}
		$facturasStr = substr($facturasStr, 0,strlen($facturasStr)-1);

		$info['ORIGINAL_INVOICE'] = $facturasStr;
		$info['TOTAL_IMPUESTOS'] = $arrayImpuestos[$id];
		$info['TOTAL_DESCUENTOS'] = $arrayDescuentos[$id];

			$orden->setErpConsulta(json_encode($info));
			$estado_orden = $info['ORDER_STATUS'];
			Mage::log(__METHOD__.":131:".$estado_orden);
			switch ($estado_orden) {
				case 'O':
					Mage::log(__METHOD__.":STATE_NEW:");
					if($orden->getState()!=Mage_Sales_Model_Order::STATE_NEW)
					$orden->setState(Mage_Sales_Model_Order::STATE_NEW,'pending',"NUEVO ESTADO ERP");
					break;
				case 'C':
					Mage::log(__METHOD__.":STATE_CLOSED:");
					if($orden->getState()!=Mage_Sales_Model_Order::STATE_CLOSED)
					$orden->setState(Mage_Sales_Model_Order::STATE_CLOSED,'closed',"NUEVO ESTADO ERP");
					break;
				case 'X':Mage::log(__METHOD__.":STATE_CANCELED:");
					if($orden->getState()!=Mage_Sales_Model_Order::STATE_CANCELED)
					$orden->setState(Mage_Sales_Model_Order::STATE_CANCELED,'canceled',"NUEVO ESTADO ERP");
					break;
				case 'P':Mage::log(__METHOD__.":STATE_PROCESSING:");
					if($orden->getState()!=Mage_Sales_Model_Order::STATE_PROCESSING)
					$orden->setState(Mage_Sales_Model_Order::STATE_PROCESSING,'processing',"NUEVO ESTADO ERP");
					break;
				
			}

			$subtotal = 0;
			foreach ($orden->getAllItems() as $item) {
				$subtotal +=  $item->getRowTotal();
			}
			if($info['INVOICE_AMOUNT']){
				$orden->setGrandTotal($info['INVOICE_AMOUNT']);	
				$orden->setSubtotal($subtotal);	
			}else{
				$orden->setGrandTotal($info['AMOUNT']);	
				$orden->setSubtotal($subtotal);	
			}
			if($info['TOTAL_IMPUESTOS']){
				$orden->setTaxInvoiced($info['TOTAL_IMPUESTOS']);	
				$orden->setTaxAmount($info['TOTAL_IMPUESTOS']);	
				$orden->setBaseTaxAmount($info['TOTAL_IMPUESTOS']);	
				$orden->setBaseTaxInvoiced($info['TOTAL_IMPUESTOS']);	
			}
			if($info['TOTAL_DESCUENTOS']){
				$orden->setBaseDiscountAmount($info['TOTAL_DESCUENTOS']);	
				$orden->setBaseDiscountInvoiced($info['TOTAL_DESCUENTOS']);	
				$orden->setDiscountAmount($info['TOTAL_DESCUENTOS']);	
				$orden->setDiscountInvoiced($info['TOTAL_DESCUENTOS']);	
			}

			$orden->save();	
		}
		foreach($log_resumen as $line){
			$helper->debugLog($line);
		}
		return true;
		
	}
	
	protected function send($url,$data){
	
		$helper = Mage::helper('corbeta_consultapedidos');
		$helper->debugLog(__METHOD__);
		if(Mage::getStoreConfig('corbeta_pedidos/update/debug')){
			$helper->debugLog(__METHOD__.":1");
			$cli = new SoapClient($url,array('trace' => 1));
		}else{
			$helper->debugLog(__METHOD__.":2");
			$cli = new SoapClient($url);
		}
		
		
		
		try{
			$helper->debugLog(__METHOD__.":23");
			//Esto es necesario debido a que la ip de comunicacion se usa enmascarada y el contrato devuelve la ip por defecto
			$oldLocation = $cli->__setLocation($url);
			
			$response = $cli->__call('WS_MAGENTO_CONS_PEDIDO', $data);
			
			if(is_soap_fault($response)){
				$helper->errorLog(__METHOD__.":soap faultcode:".$response->faultcode);
				$helper->errorLog(__METHOD__.":soap faultstring:".$response->faultstring);
			}
			
		}catch(Exception $e){
			$response = $e->getMessage();
			$helper->errorLog(__METHOD__.":error:".$e->getMessage());
			
		}
		
		if(Mage::getStoreConfig('corbeta_pedidos/update/debug')){
		
			$functions = $cli->__getFunctions();
			$lastresponse = $cli->__getLastResponse();
			$lastrequest = $cli->__getLastRequest();
			$helper->consumoLog(__METHOD__.":request:".$lastrequest);
			$helper->consumoLog(__METHOD__.":response:".$lastresponse);
		}
		
		return $response;
	}
}