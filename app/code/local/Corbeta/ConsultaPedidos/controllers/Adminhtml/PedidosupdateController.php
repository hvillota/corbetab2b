<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * VAT validation controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Corbeta_ConsultaPedidos_Adminhtml_PedidosupdateController extends Mage_Adminhtml_Controller_Action
{
    
   
	public function erpupdateAction()
    { 	
		
		$customerId = (int) $this->getRequest()->getParam('customer_id');
        $customer = Mage::getModel('customer/customer');

        if ($customerId) {
            $customer->load($customerId);
            $id=$customer->getCusId();
            //alista la informacion para ejecutar la consulta
            $FieldTypes = array(
				'CK_ROOT_WS' => array(),
				'CK_REQPEDIDOSMG' => array(),
				'PSCAMA' => array(),
			);
			
			$CK_REQPEDIDOSMG = array(
				'class'=>'R',
				'SETID' => 'DISTR',
				'CUST_ID' => $id,
			);
			
			$Transaction = array(
				'CK_ROOT_WS' => array('CK_REQPEDIDOSMG' => $CK_REQPEDIDOSMG,"class"=>"R"),
				'PSCAMA' => array(),
			);
			
			$MsgData = array(
				$Transaction
			);
			
			$data = array(
				'FieldTypes'=>$FieldTypes,//string
				'MsgData'=>$MsgData,//string
			);
			
			$model = Mage::getModel('corbeta_consultapedidos/consulta');
			if($model->consultaPedido($data,$id)){
				Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Se ejecuto la consulta de pedidos al ERP para '.$customer->getFirstname())
                );	
			}else{
				Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Ocurrio un error ejecutando la consulta.')
                );
			}

            
        }else{
        	Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Cliente no valido.')
                );
        }

       
        $this->_redirect('*/customer/edit', array(
                        'id' => $customerId,
                        '_current' => true
                    ));
	}
	

}
