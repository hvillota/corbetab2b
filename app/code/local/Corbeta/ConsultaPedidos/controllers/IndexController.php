 <?php
 /**
 *
 * @author Harold Villota
 */
class Corbeta_ConsultaPedidos_IndexController extends Mage_Core_Controller_Front_Action
{

	protected function send($url,$data){
	
		$helper = Mage::helper('corbeta_consultapedidos');
		$helper->debugLog(__METHOD__);
		if(Mage::getStoreConfig('corbeta_pedidos/update/debug')){
			$helper->debugLog(__METHOD__.":1");
			$cli = new SoapClient($url,array('trace' => 1));
		}else{
			$helper->debugLog(__METHOD__.":2");
			$cli = new SoapClient($url);
		}
		
		try{
			$helper->debugLog(__METHOD__.":23");
			//Esto es necesario debido a que la ip de comunicacion se usa enmascarada y el contrato devuelve la ip por defecto
			$oldLocation = $cli->__setLocation('http://192.200.0.88:8095/PSIGW/PeopleSoftServiceListeningConnector/CK_MAGENTO_CONS_PEDIDO.1.wsdl');
			
			$response = $cli->__call('WS_MAGENTO_CONS_PEDIDO', $data);
			
			if(is_soap_fault($response)){
				$helper->errorLog(__METHOD__.":soap faultcode:".$response->faultcode);
				$helper->errorLog(__METHOD__.":soap faultstring:".$response->faultstring);
			}
			
		}catch(Exception $e){
			$response = $e->getMessage();
			$helper->errorLog(__METHOD__.":error:".$e->getMessage());
			
		}
		
		if(Mage::getStoreConfig('corbeta_pedidos/update/debug')){
		$helper->debugLog(__METHOD__.":3");
			$functions = $cli->__getFunctions();
			$lastresponse = $cli->__getLastResponse();
			$lastrequest = $cli->__getLastRequest();
			$helper->debugLog(__METHOD__.":functions:".var_export($functions,true));
			$helper->consumoLog(__METHOD__.":request:".$lastrequest);
			$helper->consumoLog(__METHOD__.":response:".$lastresponse);
		}
		
		return $response;
	}
	public function testAction()
    {
		$helper = Mage::helper('corbeta_consultapedidos');
		$helper->debugLog(__METHOD__);
		
		$corbeta_url = 'http://192.200.0.88:8095/PSIGW/PeopleSoftServiceListeningConnector/CK_MAGENTO_CONS_PEDIDO.1.wsdl';		
		
		/*
		$data = new stdClass();
		$data->FieldTypes = new stdClass();
		$data->FieldTypes->CK_ROOT_WS = new stdClass();
		$data->FieldTypes->CK_MICR_PED_REQ = new stdClass();
		$data->FieldTypes->PSCAMA = new stdClass();
		*/
		
		
		$FieldTypes = array(
			'CK_ROOT_WS' => array(),
			'CK_REQPEDIDOSMG' => array(),
			'PSCAMA' => array(),
		);
		
		$CK_REQPEDIDOSMG = array(
			'class'=>'R',
			'SETID' => 'DISTR',
			//'SETID' => 'COLCO',
			'CUST_ID' => '8190056296',
			//'CUST_ID' => '12121216',
		);
		
		$Transaction = array(
			'CK_ROOT_WS' => array('CK_REQPEDIDOSMG' => $CK_REQPEDIDOSMG,"class"=>"R"),
			'PSCAMA' => array(),
		);
		
		$MsgData = array(
			$Transaction
		);
		
		$data = array(
			'FieldTypes'=>$FieldTypes,//string
			'MsgData'=>$MsgData,//string
		);
		
		$respuesta = self::send($corbeta_url,array($data));
		
		$_MsgData = $respuesta->MsgData;
		$_Transaction = $_MsgData->Transaction;
		$_CK_ROOT_WS = $_Transaction->CK_ROOT_WS;
		$_CK_RESPEDIDOSMG = $_CK_ROOT_WS->CK_RESPEDIDOSMG;
		//HTML
        $html =  "<h1>Gracias por su solicitud</h1>";
        
		foreach($_CK_RESPEDIDOSMG as $item){
			$info = array();
			$info['ORDER_NO'] = $item->ORDER_NO->_;
			$info['ORDER_NO_FROM'] = $item->ORDER_NO_FROM->_;
			$info['ORDER_DATE'] = $item->ORDER_DATE->_;
			$info['ADDRESS1'] = $item->ADDRESS1->_;
			$info['ADDRESS2'] = $item->ADDRESS2->_;
			$info['ACCOUNTING_DT'] = $item->ACCOUNTING_DT->_;
			$info['INVOICE_AMOUNT'] = $item->INVOICE_AMOUNT->_;
			$info['AMOUNT'] = $item->AMOUNT->_;
			$info['QTY_ORDERED'] = $item->QTY_ORDERED->_;
			$info['PRODUCT_ID'] = $item->PRODUCT_ID->_;
			$info['DESCR'] = $item->DESCR->_;
			$info['AMOUNT_APPLD_ORIG'] = $item->AMOUNT_APPLD_ORIG->_;
			$info['VAT_AMT'] = $item->VAT_AMT->_;
			$info['LC_VLR_IMP_CONSUMO'] = $item->LC_VLR_IMP_CONSUMO->_;
			$info['DESCR2'] = $item->DESCR2->_;
			$info['IN_FULFILL_STATE'] = $item->IN_FULFILL_STATE->_;
			$info['QTY'] = $item->QTY->_;
			$info['ORDER_STATUS'] = $item->ORDER_STATUS->_;
			
			$html .=  "<h2>Respuesta: ".$_ORDER_NO."</h2>";
			$html .=  "<h2>Respuesta: ".var_export($item,true)."</h2>";
			$html .=  "<h2>Respuesta: ".var_export($info,true)."</h2>";
			
			$orden = Mage::getModel('sales/order')->loadByIncrementId($item->ORDER_NO->_);
			
			$orden_item = Mage::getModel('sales/order_item')->getCollection()
							->addFieldToFilter('order_id', $orden->getId())
							->addFieldToFilter('product_id', $info['PRODUCT_ID']);
							
			if($orden_item->getSize()>0){
				//errr
			}else{
				$orden_item->setAdditionalData(json_encode($info));
				$orden_item->save();
			}
				
		}
        
		$this->getResponse()->setBody($html);
		
	}
	
}