<?php

class Corbeta_ConsultaPedidos_Block_Trigger extends Mage_Core_Block_Abstract
{     
	
	protected function _toHtml()
    {		
		
		$session = Mage::getSingleton('customer/session');	
		
		if(!$session->getFlagUpdateErp()){
			$customer =  $session->getCustomer();
			$id=$customer->getCusId();
			if(!$id)return '';
			Mage::log(__METHOD__." customer id:".$id);
			$FieldTypes = array(
				'CK_ROOT_WS' => array(),
				'CK_REQPEDIDOSMG' => array(),
				'PSCAMA' => array(),
			);
			
			$CK_REQPEDIDOSMG = array(
				'class'=>'R',
				'SETID' => 'DISTR',
				'CUST_ID' => $id,
			);
			
			$Transaction = array(
				'CK_ROOT_WS' => array('CK_REQPEDIDOSMG' => $CK_REQPEDIDOSMG,"class"=>"R"),
				'PSCAMA' => array(),
			);
			
			$MsgData = array(
				$Transaction
			);
			
			$data = array(
				'FieldTypes'=>$FieldTypes,//string
				'MsgData'=>$MsgData,//string
			);
			
			$model = Mage::getModel('corbeta_consultapedidos/consulta');
			$model->consultaPedido($data,$id);
		}
		$session->setFlagUpdateErp('updated');
		return '';
    }
}
