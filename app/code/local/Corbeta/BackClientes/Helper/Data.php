<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * BackClientes default helper
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_LABELS =	array(
					'SETID'=>'Setid del Cliente',
					'BUSINESS_UNIT'=>'Unidad de Negocio',
					'NAME'=>'Nombre',
					'TIPO_IDENTIFICAC'=>'Tipo Id',
					'CUST_ID'=>'Id Cliente',
					'CUS_ID'=>'Id Cliente',
					'SUPPORT_TEAM_CD'=>'Id Zona',
					'SUPPOR_TEAM_CD'=>'Id Zona',
					'CUSTOMER_TYPE'=>'Tipo cliente',
					'CUSTOMER_GROUP'=>'Grupo cliente',
					'CUST_CR_AVAILABLE'=>'Credito disponible',
					'CUS_CR_AVAILABLE'=>'Credito disponible',
					'CATALOG_NBR'=>'Catalogos',
					'CUPO_DIS'=>'Cupo disponible',
					'HOLD_CD'=>'HOLD_CD',
					'CK_FLAGMVTA'=>'CK_FLAGMVTA',
					'EMAIL_ADDR'=>'Email',
					'ADDRESS_SEQ_NUM'=>'Secuencia Direccion',
					'CITY'=>'Codigo Ciudad',
					'DESCR'=>'Nombre Ciudad',
					'ADDRESS1'=>'Direccion',
					'PHONE'=>'Telefono',
					'FAX'=>'Fax',
					'PHONE1'=>'Telefono Aux',
					'SHIP_TO_ADDR'=>'Direccion Principal',
					'IS_NEW'=>'Nuevo',
					'IS_UPDATED'=>'Actualizado',
					'CREATED AT'=>'Fecha Creacion',
					'UPDATED AT'=>'Fecha Actualizacion',
		);

	public function getLabel($str){
		if(array_key_exists($str,$this->_LABELS)){
			return $this->_LABELS[$str];
		}else{
			return $str;
		}
		
	}
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
}
