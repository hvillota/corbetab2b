<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer admin controller
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator - Imaginamos
 */
class Corbeta_BackClientes_Adminhtml_Backclientes_ErpcustomerController extends Corbeta_BackClientes_Controller_Adminhtml_BackClientes
{
    /**
     * init the erpcustomer
     *
     * @access protected
     * @return Corbeta_BackClientes_Model_Erpcustomer
     */
    protected function _initErpcustomer()
    {
        $erpcustomerId  = (int) $this->getRequest()->getParam('id');
        $erpcustomer    = Mage::getModel('corbeta_erpdata/erpcustomer');
        if ($erpcustomerId) {
            $erpcustomer->load($erpcustomerId);
        }
        Mage::register('current_erpcustomer', $erpcustomer);
        return $erpcustomer;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backclientes')->__('Erp Clientes'))
             ->_title(Mage::helper('corbeta_backclientes')->__('Erpcustomers'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit erpcustomer - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $erpcustomerId    = $this->getRequest()->getParam('id');
        $erpcustomer      = $this->_initErpcustomer();
        if ($erpcustomerId && !$erpcustomer->getId()) {
            $this->_getSession()->addError(
                Mage::helper('corbeta_backclientes')->__('This erpcustomer no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getErpcustomerData(true);
        if (!empty($data)) {
            $erpcustomer->setData($data);
        }
        Mage::register('erpcustomer_data', $erpcustomer);
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backclientes')->__('Erp Clientes'))
             ->_title(Mage::helper('corbeta_backclientes')->__('Erpcustomers'));
        if ($erpcustomer->getId()) {
            $this->_title($erpcustomer->getName());
        } else {
            $this->_title(Mage::helper('corbeta_backclientes')->__('Add erpcustomer'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new erpcustomer action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save erpcustomer - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('erpcustomer')) {
            try {
                $erpcustomer = $this->_initErpcustomer();
                $erpcustomer->addData($data);
                $erpcustomer->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backclientes')->__('Erpcustomer was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $erpcustomer->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setErpcustomerData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was a problem saving the erpcustomer.')
                );
                Mage::getSingleton('adminhtml/session')->setErpcustomerData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backclientes')->__('Unable to find erpcustomer to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete erpcustomer - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $erpcustomer = Mage::getModel('corbeta_erpdata/erpcustomer');
                $erpcustomer->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backclientes')->__('Erpcustomer was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was an error deleting erpcustomer.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backclientes')->__('Could not find erpcustomer to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete erpcustomer - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $erpcustomerIds = $this->getRequest()->getParam('erpcustomer');
        if (!is_array($erpcustomerIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backclientes')->__('Please select erpcustomers to delete.')
            );
        } else {
            try {
                foreach ($erpcustomerIds as $erpcustomerId) {
                    $erpcustomer = Mage::getModel('corbeta_erpdata/erpcustomer');
                    $erpcustomer->setId($erpcustomerId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backclientes')->__('Total of %d erpcustomers were successfully deleted.', count($erpcustomerIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was an error deleting erpcustomers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $erpcustomerIds = $this->getRequest()->getParam('erpcustomer');
        if (!is_array($erpcustomerIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backclientes')->__('Please select erpcustomers.')
            );
        } else {
            try {
                foreach ($erpcustomerIds as $erpcustomerId) {
                $erpcustomer = Mage::getSingleton('corbeta_erpdata/erpcustomer')->load($erpcustomerId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpcustomers were successfully updated.', count($erpcustomerIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was an error updating erpcustomers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_NEW change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsNewAction()
    {
        $erpcustomerIds = $this->getRequest()->getParam('erpcustomer');
        if (!is_array($erpcustomerIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backclientes')->__('Please select erpcustomers.')
            );
        } else {
            try {
                foreach ($erpcustomerIds as $erpcustomerId) {
                $erpcustomer = Mage::getSingleton('corbeta_erpdata/erpcustomer')->load($erpcustomerId)
                    ->setIsNew($this->getRequest()->getParam('flag_is_new'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpcustomers were successfully updated.', count($erpcustomerIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was an error updating erpcustomers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_UPDATED change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsUpdatedAction()
    {
        $erpcustomerIds = $this->getRequest()->getParam('erpcustomer');
        if (!is_array($erpcustomerIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backclientes')->__('Please select erpcustomers.')
            );
        } else {
            try {
                foreach ($erpcustomerIds as $erpcustomerId) {
                $erpcustomer = Mage::getSingleton('corbeta_erpdata/erpcustomer')->load($erpcustomerId)
                    ->setIsUpdated($this->getRequest()->getParam('flag_is_updated'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpcustomers were successfully updated.', count($erpcustomerIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backclientes')->__('There was an error updating erpcustomers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'erpcustomer.csv';
        $content    = $this->getLayout()->createBlock('corbeta_backclientes/adminhtml_erpcustomer_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'erpcustomer.xls';
        $content    = $this->getLayout()->createBlock('corbeta_backclientes/adminhtml_erpcustomer_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'erpcustomer.xml';
        $content    = $this->getLayout()->createBlock('corbeta_backclientes/adminhtml_erpcustomer_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/corbeta/erpcustomer');
    }
}
