<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('erpcustomer_');
        $form->setFieldNameSuffix('erpcustomer');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'erpcustomer_form',
            array('legend' => Mage::helper('corbeta_backclientes')->__('Erpcustomer Info'))
        );
		
        $fieldset->addField(
            'setid',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('SETID'),
                'name'  => 'setid',

           )
        );

        $fieldset->addField(
            'business_unit',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('BUSINESS_UNIT'),
                'name'  => 'business_unit',

           )
        );

        $fieldset->addField(
            'name',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('NAME'),
                'name'  => 'name',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'tipo_identificac',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('TIPO_IDENTIFICAC'),
                'name'  => 'tipo_identificac',

           )
        );

        $fieldset->addField(
            'cus_id',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CUS_ID'),
                'name'  => 'cus_id',

           )
        );

        $fieldset->addField(
            'customer_type',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CUSTOMER_TYPE'),
                'name'  => 'customer_type',

           )
        );

        $fieldset->addField(
            'customer_group',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CUSTOMER_GROUP'),
                'name'  => 'customer_group',

           )
        );

        $fieldset->addField(
            'cus_cr_available',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CUS_CR_AVAILABLE'),
                'name'  => 'cus_cr_available',

           )
        );

        $fieldset->addField(
            'cupo_dis',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CUPO_DIS'),
                'name'  => 'cupo_dis',

           )
        );

        $fieldset->addField(
            'hold_cd',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('HOLD_CD'),
                'name'  => 'hold_cd',

           )
        );

        $fieldset->addField(
            'email_addr',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('EMAIL_ADDR'),
                'name'  => 'email_addr',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		
		$fieldset->addField(
            'support_team_cd',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('SUPPORT_TEAM_CD'),
                'name'  => 'support_team_cd',
           )
        );
		
		$fieldset->addField(
            'catalog_nbr',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CATALOG_NBR'),
                'name'  => 'catalog_nbr',
           )
        );
		
		$fieldset->addField(
            'created_at',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('CREATED AT'),
                'name'  => 'created_at',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		$fieldset->addField(
            'updated_at',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('UPDATED AT'),
                'name'  => 'updated_at',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		

        $fieldset->addField(
            'is_new',
            'select',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('IS_NEW'),
                'name'  => 'is_new',
            'required'  => true,
			'disabled' => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backclientes')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backclientes')->__('No'),
                ),
            ),
           )
        );

        $fieldset->addField(
            'is_updated',
            'select',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('IS_UPDATED'),
                'name'  => 'is_updated',
            'required'  => true,
			 'disabled' => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backclientes')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backclientes')->__('No'),
                ),
            ),
           )
        );
        
        $formValues = Mage::registry('current_erpcustomer')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getErpcustomerData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getErpcustomerData());
            Mage::getSingleton('adminhtml/session')->setErpcustomerData(null);
        } elseif (Mage::registry('current_erpcustomer')) {
            $formValues = array_merge($formValues, Mage::registry('current_erpcustomer')->getData());
        }
		
		$erpCustomer = Mage::getModel('corbeta_erpdata/erpcustomer')->load($formValues['customer_id']);
		
		$formValues['support_team_cd']=$erpCustomer->getZonasStr();
		$formValues['catalog_nbr']=$erpCustomer->getCatalogosStr();
		
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
