<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer admin edit tabs
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpcustomer_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('corbeta_backclientes')->__('Erpcustomer'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_erpcustomer',
            array(
                'label'   => Mage::helper('corbeta_backclientes')->__('Erpcustomer'),
                'title'   => Mage::helper('corbeta_backclientes')->__('Erpcustomer'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_backclientes/adminhtml_erpcustomer_edit_tab_form'
                )
                ->toHtml(),
            )
        );
		$this->addTab(
            'form_erpcustomer_address',
            array(
                'label'   => Mage::helper('corbeta_backclientes')->__('Direcciones'),
                'title'   => Mage::helper('corbeta_backclientes')->__('Direcciones'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_backclientes/adminhtml_erpcustomer_edit_tab_address'
                )
                ->toHtml(),
            )
        );
		/*
		$this->addTab(
            'form_erpcustomer_zonas',
            array(
                'label'   => Mage::helper('corbeta_backclientes')->__('Zonas'),
                'title'   => Mage::helper('corbeta_backclientes')->__('Zonas'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_backclientes/adminhtml_erpcustomer_edit_tab_zonas'
                )
                ->toHtml(),
            )
        );
		*/
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve erpcustomer entity
     *
     * @access public
     * @return Corbeta_BackClientes_Model_Erpcustomer
     * @author Ultimate Module Creator
     */
    public function getErpcustomer()
    {
        return Mage::registry('current_erpcustomer');
    }
}
