<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer admin grid block
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpcustomerGrid');
        $this->setDefaultSort('customer_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('corbeta_erpdata/erpcustomer')
            ->getCollection();
        $subquery1 = new Zend_Db_Expr("(SELECT * FROM erp_address WHERE ship_to_addr = 'Y')");
        $subquery2 = new Zend_Db_Expr("(SELECT * FROM erp_customer_x_zonas WHERE erp_customer_x_zonas.default = 'Y')");
        
        $collection->getSelect()->joinLeft( array("erp_address"=> $subquery1), 'erp_address.erp_customer_id = main_table.customer_id', array('erp_address.*'));
        $collection->getSelect()->joinleft( array('erp_customer_x_zonas'=> $subquery2), 'erp_customer_x_zonas.customer_id = main_table.customer_id', array('erp_customer_x_zonas.zona_id'));
        //Mage::log(__METHOD__.":56:".$collection->getSelectSql());
        
        $collection->getSelect()->joinleft( array('erp_zonas'=> erp_zonas), 'erp_zonas.zona_id = erp_customer_x_zonas.zona_id', array('erp_zonas.suppor_team_cd'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'setid',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('SETID'),
                'index'  => 'setid',
                'type'   => 'text'
            )
        );
		
		 $this->addColumn(
            'customer_id',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('Id'),
                'index'  => 'customer_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('corbeta_backclientes')->getLabel('NAME'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        
        
        $this->addColumn(
            'business_unit',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('BUSINESS_UNIT'),
                'index'  => 'business_unit',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'tipo_identificac',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('TIPO_IDENTIFICAC'),
                'index'  => 'tipo_identificac',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'cus_id',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CUS_ID'),
                'index'  => 'cus_id',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'customer_type',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CUSTOMER_TYPE'),
                'index'  => 'customer_type',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'customer_group',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CUSTOMER_GROUP'),
                'index'  => 'customer_group',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'cus_cr_available',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CUS_CR_AVAILABLE'),
                'index'  => 'cus_cr_available',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'cupo_dis',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CUPO_DIS'),
                'index'  => 'cupo_dis',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'email_addr',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('EMAIL_ADDR'),
                'index'  => 'email_addr',
                'type'=> 'text',

            )
        );
		
		$this->addColumn(
            'suppor_team_cd',
            array(
                'header'    => Mage::helper('corbeta_backclientes')->getLabel('SUPPORT_TEAM_CD'),
                'align'     => 'left',
                'index'     => 'suppor_team_cd',
            )
        );
		
		$this->addColumn(
            'address_seq_num',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('ADDRESS_SEQ_NUM'),
                'index'  => 'address_seq_num',
                'type'   => 'text'
            )
        );
		
		$this->addColumn(
            'city',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CITY'),
                'index'  => 'city',
                'type'   => 'text'
            )
        );
		
		$this->addColumn(
            'address1',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('ADDRESS1'),
                'index'  => 'address1',
                'type'   => 'text'
            )
        );
		
		
		$this->addColumn(
            'phone',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('PHONE'),
                'index'  => 'phone',
                'type'   => 'text'
            )
        );
		
		$this->addColumn(
            'fax',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('FAX'),
                'index'  => 'fax',
                'type'   => 'text'
            )
        );
		
		$this->addColumn(
            'phone1',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('PHONE1'),
                'index'  => 'phone1',
                'type'   => 'text'
            )
        );
		
		$this->addColumn(
            'ship_to_addr',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('SHIP_TO_ADDR'),
                'index'  => 'ship_to_addr',
                'type'   => 'text'
            )
        );
		
		/*
		$this->addColumn(
            'support_team_cd',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('SUPPORT_TEAM_CD'),
                'index'  => 'support_team_cd',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'catalog_nbr',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('CATALOG_NBR'),
                'index'  => 'catalog_nbr',
                'type'=> 'text',

            )
        );
		*/
        $this->addColumn(
            'is_new',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('IS_NEW'),
                'index'  => 'is_new',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backclientes')->__('Yes'),
                    '0' => Mage::helper('corbeta_backclientes')->__('No'),
                )

            )
        );
        $this->addColumn(
            'is_updated',
            array(
                'header' => Mage::helper('corbeta_backclientes')->getLabel('IS_UPDATED'),
                'index'  => 'is_updated',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backclientes')->__('Yes'),
                    '0' => Mage::helper('corbeta_backclientes')->__('No'),
                )

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('corbeta_backclientes')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('corbeta_backclientes')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('corbeta_backclientes')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('corbeta_backclientes')->__('Detalle'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('corbeta_backclientes')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('corbeta_backclientes')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('corbeta_backclientes')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('customer_id');
        $this->getMassactionBlock()->setFormFieldName('erpcustomer');
        
        
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Corbeta_BackClientes_Model_Erpcustomer
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
