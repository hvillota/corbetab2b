<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer admin edit form
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'corbeta_backclientes';
        $this->_controller = 'adminhtml_erpcustomer';
       
		$this->_removeButton('delete');
		$this->_removeButton('save');
		$this->_removeButton('reset');
        
        
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_erpcustomer') && Mage::registry('current_erpcustomer')->getId()) {
            return Mage::helper('corbeta_backclientes')->__(
                "Edit Erpcustomer '%s'",
                $this->escapeHtml(Mage::registry('current_erpcustomer')->getName())
            );
        } else {
            return Mage::helper('corbeta_backclientes')->__('Add Erpcustomer');
        }
    }
}
