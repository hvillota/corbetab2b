<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Address extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('erpcustomer_');
        $form->setFieldNameSuffix('erpcustomer');
        $this->setForm($form);
		
		$customer_data = Mage::registry('current_erpcustomer')->getData();
		$customer_id = $customer_data['customer_id'];
		
		$direccionesCollection = Mage::getModel('corbeta_erpdata/erpaddress')
            ->getCollection();
		$direccionesCollection->addFieldToFilter('erp_customer_id', $customer_id);
		
		foreach($direccionesCollection as $direccionObj){
			$id = $direccionObj->getAddressId();
			$fieldset = $form->addFieldset(
				'erpcustomer_form_address'.$id,
				array('legend' => 'Direccion '.$id)
			);
			
			$fieldset->addField(
				'address_seq_num'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('ADDRESS_SEQ_NUM'),
					'name'  => 'address_seq_num'.$id,

			   )
			);
			$fieldset->addField(
				'city'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('CITY'),
					'name'  => 'city'.$id,

			   )
			);
			$fieldset->addField(
				'address1'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('ADDRESS1'),
					'name'  => 'address1'.$id,

			   )
			);
			$fieldset->addField(
				'phone'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('PHONE'),
					'name'  => 'phone'.$id,

			   )
			);
			$fieldset->addField(
				'fax'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('FAX'),
					'name'  => 'fax'.$id,

			   )
			);
			$fieldset->addField(
				'phone1'.$id,
				'label',
				array(
					'label' => Mage::helper('corbeta_backclientes')->getLabel('PHONE1'),
					'name'  => 'phone1'.$id,

			   )
			);
			
		}
		
		$formValues = array();
		foreach($direccionesCollection as $direccion){
			$id = $direccion->getAddressId();
			$formValues['address_seq_num'.$id] = $direccion->getAddressSeqNum();
			$formValues['city'.$id] = $direccion->getCity();
			$formValues['address1'.$id] = $direccion->getAddress1();
			$formValues['phone'.$id] = $direccion->getPhone();
			$formValues['fax'.$id] = $direccion->getFax();
			$formValues['phone1'.$id] = $direccion->getPhone1();
		}
		$form->setValues($formValues);
        return parent::_prepareForm();
    }
}
