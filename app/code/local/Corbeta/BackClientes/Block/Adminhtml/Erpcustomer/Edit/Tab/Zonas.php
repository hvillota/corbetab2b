<?php
/**
 * Corbeta_BackClientes extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackClientes
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpcustomer edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_BackClientes
 * @author      Ultimate Module Creator
 */
class Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Zonas extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_BackClientes_Block_Adminhtml_Erpcustomer_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('erpcustomer_');
        $form->setFieldNameSuffix('erpcustomer');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'erpcustomer_form',
            array('legend' => Mage::helper('corbeta_backclientes')->__('Zonas'))
        );
		
        $fieldset->addField(
            'setid',
            'label',
            array(
                'label' => Mage::helper('corbeta_backclientes')->getLabel('SETID'),
                'name'  => 'setid',

           )
        );

        
        $formValues = Mage::registry('current_erpcustomer')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getErpcustomerData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getErpcustomerData());
            Mage::getSingleton('adminhtml/session')->setErpcustomerData(null);
        } elseif (Mage::registry('current_erpcustomer')) {
            $formValues = array_merge($formValues, Mage::registry('current_erpcustomer')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
