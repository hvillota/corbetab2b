<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona admin controller
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Adminhtml_Backzonas_ErpzonaController extends Corbeta_BackZonas_Controller_Adminhtml_BackZonas
{
    /**
     * init the erpzona
     *
     * @access protected
     * @return Corbeta_BackZonas_Model_Erpzona
     */
    protected function _initErpzona()
    {
        $erpzonaId  = (int) $this->getRequest()->getParam('id');
        $erpzona    = Mage::getModel('corbeta_erpdata/erpzona');
        if ($erpzonaId) {
            $erpzona->load($erpzonaId);
        }
        Mage::register('current_erpzona', $erpzona);
        return $erpzona;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backzonas')->__('Erp Zonas'))
             ->_title(Mage::helper('corbeta_backzonas')->__('Erpzonas'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit erpzona - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $erpzonaId    = $this->getRequest()->getParam('id');
        $erpzona      = $this->_initErpzona();
        if ($erpzonaId && !$erpzona->getId()) {
            $this->_getSession()->addError(
                Mage::helper('corbeta_backzonas')->__('This erpzona no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getErpzonaData(true);
        if (!empty($data)) {
            $erpzona->setData($data);
        }
        Mage::register('erpzona_data', $erpzona);
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_backzonas')->__('Erp Zonas'))
             ->_title(Mage::helper('corbeta_backzonas')->__('Erpzonas'));
        if ($erpzona->getId()) {
            $this->_title($erpzona->getSupporTeamCd());
        } else {
            $this->_title(Mage::helper('corbeta_backzonas')->__('Add erpzona'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new erpzona action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save erpzona - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('erpzona')) {
            try {
                $erpzona = $this->_initErpzona();
                $erpzona->addData($data);
                $erpzona->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backzonas')->__('Erpzona was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $erpzona->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setErpzonaData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was a problem saving the erpzona.')
                );
                Mage::getSingleton('adminhtml/session')->setErpzonaData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backzonas')->__('Unable to find erpzona to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete erpzona - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $erpzona = Mage::getModel('corbeta_erpdata/erpzona');
                $erpzona->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backzonas')->__('Erpzona was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was an error deleting erpzona.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_backzonas')->__('Could not find erpzona to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete erpzona - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $erpzonaIds = $this->getRequest()->getParam('erpzona');
        if (!is_array($erpzonaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backzonas')->__('Please select erpzonas to delete.')
            );
        } else {
            try {
                foreach ($erpzonaIds as $erpzonaId) {
                    $erpzona = Mage::getModel('corbeta_erpdata/erpzona');
                    $erpzona->setId($erpzonaId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_backzonas')->__('Total of %d erpzonas were successfully deleted.', count($erpzonaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was an error deleting erpzonas.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $erpzonaIds = $this->getRequest()->getParam('erpzona');
        if (!is_array($erpzonaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backzonas')->__('Please select erpzonas.')
            );
        } else {
            try {
                foreach ($erpzonaIds as $erpzonaId) {
                $erpzona = Mage::getSingleton('corbeta_erpdata/erpzona')->load($erpzonaId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpzonas were successfully updated.', count($erpzonaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was an error updating erpzonas.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_NEW change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsNewAction()
    {
        $erpzonaIds = $this->getRequest()->getParam('erpzona');
        if (!is_array($erpzonaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backzonas')->__('Please select erpzonas.')
            );
        } else {
            try {
                foreach ($erpzonaIds as $erpzonaId) {
                $erpzona = Mage::getSingleton('corbeta_erpdata/erpzona')->load($erpzonaId)
                    ->setIsNew($this->getRequest()->getParam('flag_is_new'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpzonas were successfully updated.', count($erpzonaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was an error updating erpzonas.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass IS_UPDATED change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsUpdatedAction()
    {
        $erpzonaIds = $this->getRequest()->getParam('erpzona');
        if (!is_array($erpzonaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_backzonas')->__('Please select erpzonas.')
            );
        } else {
            try {
                foreach ($erpzonaIds as $erpzonaId) {
                $erpzona = Mage::getSingleton('corbeta_erpdata/erpzona')->load($erpzonaId)
                    ->setIsUpdated($this->getRequest()->getParam('flag_is_updated'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d erpzonas were successfully updated.', count($erpzonaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_backzonas')->__('There was an error updating erpzonas.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'erpzona.csv';
        $content    = $this->getLayout()->createBlock('corbeta_backzonas/adminhtml_erpzona_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'erpzona.xls';
        $content    = $this->getLayout()->createBlock('corbeta_backzonas/adminhtml_erpzona_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'erpzona.xml';
        $content    = $this->getLayout()->createBlock('corbeta_backzonas/adminhtml_erpzona_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/corbeta/erpzona');
    }
}
