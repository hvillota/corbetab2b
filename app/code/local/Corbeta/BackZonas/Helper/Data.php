<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * BackZonas default helper
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Helper_Data extends Mage_Core_Helper_Abstract
{
    
	protected $_LABELS =	array(
					'SUPPORT_TEAM_CD'=>'ID de Zona',
					'SUPPOR_TEAM_CD'=>'ID de Zona',
					'CATALOG_NBR'=>'Nombre del catalogo',
					'BUSINESS_UNIT'=>'Unidad de Negocio',
					'CUSTOMER_ID'=>'Identificación cliente',
					'SUPPORT_TEAM_MBR'=>'Vendedor',
					'PHONE_TYPE'=>'Tipo de teléfono',
					'PHONE'=>'Teléfono vendedor',
					'SUPPORT_TEAM_MBR1'=>'Gerente',
					'COUNTRY_CODE'=>'Prefijo',
					'DEFAULT_FLAG'=>'Flag',
					'IS_NEW'=>'Nuevo',
					'IS_UPDATED'=>'Actualizado',
		);

	public function getLabel($str){
		if(array_key_exists($str,$this->_LABELS)){
			return $this->_LABELS[$str];
		}else{
			return $str;
		}
		
	}
	
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
	
}
