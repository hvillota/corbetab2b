<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona admin block
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Block_Adminhtml_Erpzona extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_erpzona';
        $this->_blockGroup         = 'corbeta_backzonas';
        parent::__construct();
        $this->_headerText         = Mage::helper('corbeta_backzonas')->__('Erpzona');
      $this->_removeButton('add');

    }
}
