<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Block_Adminhtml_Erpzona_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('erpzona_');
        $form->setFieldNameSuffix('erpzona');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'erpzona_form',
            array('legend' => Mage::helper('corbeta_backzonas')->__('Erpzona'))
        );

        $fieldset->addField(
            'suppor_team_cd',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('SUPPOR_TEAM_CD'),
                'name'  => 'suppor_team_cd',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'default_flag',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('DEFAULT_FLAG'),
                'name'  => 'default_flag',

           )
        );

        $fieldset->addField(
            'business_unit',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('BUSINESS_UNIT'),
                'name'  => 'business_unit',

           )
        );

        $fieldset->addField(
            'support_team_mbr',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('SUPPORT_TEAM_MBR'),
                'name'  => 'support_team_mbr',

           )
        );

        $fieldset->addField(
            'phone_type',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('PHONE_TYPE'),
                'name'  => 'phone_type',

           )
        );

        $fieldset->addField(
            'phone',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('PHONE'),
                'name'  => 'phone',

           )
        );

        $fieldset->addField(
            'support_team_mbr1',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('SUPPORT_TEAM_MBR1'),
                'name'  => 'support_team_mbr1',

           )
        );

        $fieldset->addField(
            'country_code',
            'label',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('COUNTRY_CODE'),
                'name'  => 'country_code',

           )
        );

        $fieldset->addField(
            'is_new',
            'select',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('IS_NEW'),
                'name'  => 'is_new',
            'required'  => true,
            'disabled'  => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backzonas')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backzonas')->__('No'),
                ),
            ),
           )
        );

        $fieldset->addField(
            'is_updated',
            'select',
            array(
                'label' => Mage::helper('corbeta_backzonas')->getLabel('IS_UPDATED'),
                'name'  => 'is_updated',
            'required'  => true,
			'disabled'  => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_backzonas')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_backzonas')->__('No'),
                ),
            ),
           )
        );
        
        $formValues = Mage::registry('current_erpzona')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getErpzonaData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getErpzonaData());
            Mage::getSingleton('adminhtml/session')->setErpzonaData(null);
        } elseif (Mage::registry('current_erpzona')) {
            $formValues = array_merge($formValues, Mage::registry('current_erpzona')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
