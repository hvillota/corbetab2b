<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona admin edit tabs
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Block_Adminhtml_Erpzona_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpzona_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('corbeta_backzonas')->__('Erpzona'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_erpzona',
            array(
                'label'   => Mage::helper('corbeta_backzonas')->__('Erpzona'),
                'title'   => Mage::helper('corbeta_backzonas')->__('Erpzona'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_backzonas/adminhtml_erpzona_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve erpzona entity
     *
     * @access public
     * @return Corbeta_BackZonas_Model_Erpzona
     * @author Ultimate Module Creator
     */
    public function getErpzona()
    {
        return Mage::registry('current_erpzona');
    }
}
