<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona admin edit form
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Block_Adminhtml_Erpzona_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'corbeta_backzonas';
        $this->_controller = 'adminhtml_erpzona';
        
			$this->_removeButton('delete');
		$this->_removeButton('save');
		$this->_removeButton('reset');
		
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_erpzona') && Mage::registry('current_erpzona')->getId()) {
            return Mage::helper('corbeta_backzonas')->__(
                "Edit Erpzona '%s'",
                $this->escapeHtml(Mage::registry('current_erpzona')->getSupporTeamCd())
            );
        } else {
            return Mage::helper('corbeta_backzonas')->__('Add Erpzona');
        }
    }
}
