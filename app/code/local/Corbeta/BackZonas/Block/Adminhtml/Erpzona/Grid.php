<?php
/**
 * Corbeta_BackZonas extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_BackZonas
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Erpzona admin grid block
 *
 * @category    Corbeta
 * @package     Corbeta_BackZonas
 * @author      Ultimate Module Creator
 */
class Corbeta_BackZonas_Block_Adminhtml_Erpzona_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('erpzonaGrid');
        $this->setDefaultSort('zona_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('corbeta_erpdata/erpzona')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'zona_id',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('Id'),
                'index'  => 'zona_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'suppor_team_cd',
            array(
                'header'    => Mage::helper('corbeta_backzonas')->getLabel('SUPPOR_TEAM_CD'),
                'align'     => 'left',
                'index'     => 'suppor_team_cd',
            )
        );
        
        
        $this->addColumn(
            'business_unit',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('BUSINESS_UNIT'),
                'index'  => 'business_unit',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'support_team_mbr',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('SUPPORT_TEAM_MBR'),
                'index'  => 'support_team_mbr',
                'type'=> 'number',

            )
        );
		$this->addColumn(
            'phone_type',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('PHONE_TYPE'),
                'index'  => 'phone_type',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'phone',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('PHONE'),
                'index'  => 'phone',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'support_team_mbr1',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('SUPPORT_TEAM_MBR1'),
                'index'  => 'support_team_mbr1',
                'type'=> 'text',

            )
        );
		$this->addColumn(
            'country_code',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('COUNTRY_CODE'),
                'index'  => 'country_code',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'is_new',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('IS_NEW'),
                'index'  => 'is_new',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backzonas')->getLabel('Yes'),
                    '0' => Mage::helper('corbeta_backzonas')->getLabel('No'),
                )

            )
        );
        $this->addColumn(
            'is_updated',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('IS_UPDATED'),
                'index'  => 'is_updated',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_backzonas')->getLabel('Yes'),
                    '0' => Mage::helper('corbeta_backzonas')->getLabel('No'),
                )

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('corbeta_backzonas')->getLabel('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('corbeta_backzonas')->getLabel('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('corbeta_backzonas')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('corbeta_backzonas')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('corbeta_backzonas')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('corbeta_backzonas')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('corbeta_backzonas')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('erpzona');
        
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Corbeta_BackZonas_Model_Erpzona
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Corbeta_BackZonas_Block_Adminhtml_Erpzona_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
