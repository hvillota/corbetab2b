 <?php
 /**
 *
 * @author Harold Villota
 */
class Corbeta_ProductPopup_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Pre dispatch action that allows to redirect to no route page in case of disabled extension through admin panel
     */
    public function preDispatch()
    {
        parent::preDispatch();
        /*
        if (!Mage::helper('corbeta_productpopup')->isEnabled()) {
            $this->setFlag('', 'no-dispatch', true);
            $this->_redirect('noRoute');
        } 
		*/		
    }
    
    /**
     * Index action
     */
    public function indexAction()
    {
		$productId  = (int) $this->getRequest()->getParam('id');
        $this->loadLayout();

        $popup = $this->getLayout()->getBlock('productpopup');
        $popup->setProductId($productId);
        $popup->init();
		
        $this->renderLayout();
    }

   
}