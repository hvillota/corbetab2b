<?php

class Corbeta_ProductPopup_Block_Home_Popup extends Mage_Catalog_Block_Product_Abstract

{
    
    public $product;
	
    protected function _construct()
    {
        parent::_construct();
    }
	
	public function init(){
		$_product = Mage::getModel('catalog/product')->load($this->getProductId()); 
		$this->product = $_product;
	}
	public function getProduct(){
		return 	$this->product;
	}
}
