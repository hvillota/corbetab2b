<?php

class Corbeta_ConfigCustomer_Block_Page_Html_Welcome extends Mage_Page_Block_Html_Welcome
{
	protected function _toHtml()
    {
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_data['welcome'] = $this->__('Welcome, %s!', $this->escapeHtml(Mage::getSingleton('customer/session')->getCustomer()->getFirstname()));
            } else {
                $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
            }
        }
        $returnHtml =  $this->_data['welcome'];

        if (!empty($this->_data['additional_html'])) {
            $returnHtml .= ' ' . $this->_data['additional_html'];
        }

        return $returnHtml;
    }
}