<?php
/**
 * Id Customer installation script
 *
 * @author Harold Villota
 */

/**
 * @var $installer Mage_Customer_Model_Resource_Setup
 */
 
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'setid', array(
    'label'         => 'setid',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'business_unit', array(
    'label'         => 'business_unit',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'tipo_identificac', array(
    'label'         => 'tipo_identificac',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'customer_type', array(
    'label'         => 'customer_type',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'support_team_cd', array(
    'label'         => 'support_team_cd',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));


$installer->addAttribute('customer', 'customer_group', array(
    'label'         => 'customer_group',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'cus_cr_available', array(
    'label'         => 'cus_cr_available',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'cupo_dis', array(
    'label'         => 'cupo_dis',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'hold_cd', array(
    'label'         => 'hold_cd',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$installer->addAttribute('customer', 'cus_id', array(
    'label'         => 'cus_id',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));
$installer->addAttribute('customer', 'catalog_nbr', array(
    'label'         => 'catalog_nbr',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));




$installer->addAttribute('customer_address', 'address_seq_num', array(
    'label'         => 'address_seq_num',
    'type'          => 'varchar',
    'input'         => 'hidden',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));
$attributeid = $installer->getAttributeId ('customer_address','address_seq_num');
$installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), array(
    'form_code'     => 'adminhtml_customer_address',
    'attribute_id'   => $attributeid
));	

$installer->addAttribute('customer_address', 'phone1', array(
    'label'         => 'phone1',
    'type'          => 'varchar',
    'input'         => 'hidden',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));
$attributeid = $installer->getAttributeId ('customer_address','phone1');
$installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), array(
    'form_code'     => 'adminhtml_customer_address',
    'attribute_id'   => $attributeid
));

$installer->addAttribute('customer', 'change_password_required', array(
    'label'         => 'Requiere Cambiar Password',
    'type'          => 'int',
    'input'         => 'boolean',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$attributeid = $installer->getAttributeId ('customer','change_password_required');
$installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), array(
    'form_code'     => 'adminhtml_customer',
    'attribute_id'   => $attributeid
));


$installer->addAttribute('customer', 'estado', array(
    'label'         => 'Estado',
    'type'          => 'int',
    'input'         => 'select',
    'visible'       => true,
    'source'       => true,
    'required'      => false,
    'position'      => 9999,
	'source'        => 'corbeta_configcustomer/estados',
));

$attributeid = $installer->getAttributeId ('customer','estado');
$installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), array(
    'form_code'     => 'adminhtml_customer',
    'attribute_id'   => $attributeid
));



$installer->addAttribute('customer', 'zona_default', array(
    'label'         => 'Zona Principal',
    'type'          => 'varchar',
    'input'         => 'text',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));


$installer->endSetup();