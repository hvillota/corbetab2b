<?php
/**
 * Id Customer installation script
 *
 * @author Harold Villota
 */

/**
 * @var $installer Mage_Customer_Model_Resource_Setup
 */
 
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer_address', 'erpcity', array(
    'label'         => 'erpcity',
    'type'          => 'varchar',
    'input'         => 'hidden',
    'visible'       => true,
    'required'      => false,
    'position'      => 9999,
));

$attributeid = $installer->getAttributeId ('customer_address','erpcity');
$installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), array(
    'form_code'     => 'adminhtml_customer_address',
    'attribute_id'   => $attributeid
));

$installer->endSetup();