<?php

class Corbeta_ConfigCustomer_Model_Estados extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = array(
				array(
                    'value' => '',
                    'label' => '',
                ),
                array(
                    'value' => '1',
                    'label' => 'ACTIVO',
                ),
                array(
                    'value' => '2',
                    'label' => 'BLOQUEADO',
                ),
				array(
                    'value' => '3',
                    'label' => 'INACTIVO',
                ),
            );
        }
        return $this->_options;
    }
	public function getAllOptionsForGrid(){
		$options = $this->getAllOptions();
		$result = array();
		foreach($options as $option){
			$result[$option['value']] = $option['label'];
		}
		return $result;
	}
}
