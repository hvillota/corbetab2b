<?php

class Corbeta_ConfigCustomer_Model_Observer extends Mage_Core_Model_Abstract{
	public function validarCedula($observer) {
		//Mage::log(__METHOD__);
		// $model -> Mage_Customer_Model_Customer
		$model = $observer->getModel();
		$password = $observer->getPassword();
		$cus_id = $model->getCusId();
		
		$login = Mage::app()->getRequest()->getPost('login');
		if($cus_id===$login['identification']){
			return;
		}
		
		throw Mage::exception('Mage_Core', Mage::helper('customer')->__('No es posible ingresar. Por favor verifica los datos e intenta nuevamente.'),2);
		
	}
	public function validarEstado($observer) {
		//Mage::log(__METHOD__);
		// $model -> Mage_Customer_Model_Customer
		$model = $observer->getModel();
		$password = $observer->getPassword();
		$estado = $model->getEstado();
		
		if($estado==2){
			throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Lo sentimos. Su cuenta se encuentra en estado BLOQUEADO'),2);
		}
		if($estado==3){
			throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Lo sentimos. Su cuenta se encuentra en estado INACTIVO'),2);
		}
		if($estado!=1){
			throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Lo sentimos. No es posible validar el estado de su cuenta'),2);
		}
		
	}

	
	public function validarUrlPermitidas($observer){
		//Mage::log(__METHOD__);
		
		$session = Mage::getSingleton('customer/session');
		if($session->getRequireChangePassword()){
			$controller = $observer->getControllerAction();
			if(!(strpos ($controller->getFullActionName(),'customer_account')===0)){
				Mage::log(__METHOD__.":logout:");
				$session->setRequireChangePassword(false);
				$session->logout()->renewSession();
			}
		}
	}
	public function validarCambioPasswordNecesario($observer){
		//Mage::log(__METHOD__);
		
		$session =  $observer->getCustomerSession();
		$customer =  $observer->getCustomerSession()->getCustomer();
		
		//se debe consultar si el cliente requiere cambio de contraseņa
		
		if($customer->getChangePasswordRequired()){
			Mage::log(__METHOD__.":requiere cambio ");
			$session->setRequireChangePassword(true);
		
			$msg = Mage::getSingleton('core/message')->error("Para  hacer tus compras  por primera vez, te  invitamos a cambiar tu contrase&#241;a en la parte inferior.");
			$session->addUniqueMessages($msg);
			
			if(!(
				(strpos(Mage::app()->getRequest()->getOriginalPathInfo(),'/customer/account/edit/changepass/1')===0)
				|| (strpos(Mage::app()->getRequest()->getOriginalPathInfo(),'/customer/account/editPost')===0)
				|| (strpos(Mage::app()->getRequest()->getOriginalPathInfo(),'/customer/account/edit')===0)
					)){
				
				Mage::app()->getResponse()->setRedirect(Mage::getUrl('customer/account/edit/changepass/1'));
			}
		}else{
			//Mage::log(__METHOD__.":no requiere cambio");
		}
	}
	public function verificarCambioPassword($observer){
		//Mage::log(__METHOD__);
		$customer =  $observer->getCustomer();
		
		$session = Mage::getSingleton('customer/session');
		if($session->getRequireChangePassword()){
			Mage::log(__METHOD__.":se solicito cambio password");
			if($session->getIntentoCambioPassword()){
				Mage::log(__METHOD__.":se intento cambiar password");
				$session->setRequireChangePassword(false);
				$session->setIntentoCambioPassword(false);
				Mage::log(__METHOD__.":se reinician flags de la session");
				$customer->setChangePasswordRequired(0);
				Mage::log(__METHOD__.":se reinicia el flag del cliente");
				$customer->save();
			}
		}
		
		
	}
}