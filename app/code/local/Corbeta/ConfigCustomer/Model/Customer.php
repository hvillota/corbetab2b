<?php

class Corbeta_ConfigCustomer_Model_Customer extends Mage_Customer_Model_Customer{
	
	public function authenticate($login, $password)
    {
        
		
		
         $this->loadByEmail($login);
        if ($this->getConfirmation() && $this->isConfirmationRequired()) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('This account is not confirmed.'),
                self::EXCEPTION_EMAIL_NOT_CONFIRMED
            );
        }
        if (!$this->validatePassword($password)) {
			
			//Valida max 5 intentos de autenticacion
			
			$session = Mage::getSingleton('customer/session');
			if($session->getIntentos()){
				$intentos = $session->getIntentos();
			}else{
				$intentos = 0;
			}
			$intentos++;
			$session->setIntentos($intentos);		
			if($intentos>=5){
				$session->setIntentos(0);
				$this->setEstado(2);
				$this->save();
				$msg = 'Excedió número de intentos usuario Bloqueado - Por favor solicite el desbloqueo con la línea
de atención al cliente';
				throw Mage::exception('Mage_Core', Mage::helper('customer')->__($msg),2);

			}
			
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'),
                self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }
        Mage::dispatchEvent('customer_customer_authenticated', array(
           'model'    => $this,
           'password' => $password,
        ));

        return true;
    }
}