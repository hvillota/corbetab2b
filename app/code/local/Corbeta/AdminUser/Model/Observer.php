<?php

class Corbeta_AdminUser_Model_Observer  extends Mage_Core_Model_Abstract
{
    
    //valida si el password debe cambiarse
    public function validarAdminPassword($observer){
        //Mage::log(__METHOD__);
        // @var $user Mage_Admin_Model_User 
        $user = $observer->getUser();
        
        //valida flag de usuario ingreso primera vez
        if($user->getChangePasswordRequired()){
            $adminSession = Mage::getSingleton('admin/session');
            $adminSession->unsetAll();
            $adminSession->getCookie()->delete($adminSession->getSessionName());
            Mage::throwException('Por favor realice click en Restablecer Contraseña para asignar una por primera vez.');
        }


        //valida caducidad contrasena
        //si no hay fecha de ultimo cambio  
        if(!$user->getLastPasswordChange()){
            $adminSession = Mage::getSingleton('admin/session');
            $adminSession->unsetAll();
            $adminSession->getCookie()->delete($adminSession->getSessionName());
            Mage::throwException('Su contraseña ha caducado. Debe hacer click en el link Restablecer Contraseña');
        }else{
            $currentTimestamp = Varien_Date::toTimestamp(now());
            $lastchangeTimestamp = Varien_Date::toTimestamp($user->getLastPasswordChange());
            $diferencia = $currentTimestamp - $lastchangeTimestamp;
            $max = 3628800;
            //si la ultima fecha de cambio es mayor de 42 dias
            if($diferencia>$max){
                $_user = Mage::getModel('admin/user')->load($user->getId());    
                $_user->setIsActive(0);
                $_user->save();
                $adminSession = Mage::getSingleton('admin/session');
                $adminSession->unsetAll();
                $adminSession->getCookie()->delete($adminSession->getSessionName());
                Mage::throwException('Su contraseña ha caducado. Debe hacer click en el link Restablecer Contraseña');                
            }
        }
        
        
        if($user->getLoginAttempts()>0){
            $_user = Mage::getModel('admin/user')->load($user->getId());    
            $_user->setLoginAttempts(0);
            $_user->save();
        }
        
    }

    // incrementa el contador de intentos fallidos de login
    public function addAttempt($observer){
        //Mage::log(__METHOD__);
        
        $username = $observer->getUserName();
        $user = Mage::getModel('admin/user')->load($username,'username');
        
        if($user->getId() && $user->getIsActive()){
            if($user->getLoginAttempts()>=4){
                $user->setIsActive(0);
                $user->setLoginAttempts(0);
            }else{
                $user->setLoginAttempts($user->getLoginAttempts()+1);
            }
            $user->setLastLoginAttempt(now());
            $user->save();
        }
        
    }
    public function checkAdminPasswordChange($observer)
    {
        // @var $user Mage_Admin_Model_User 
        $user = $observer->getEvent()->getObject();
        $password = ($user->getNewPassword() ? $user->getNewPassword() : $user->getPassword());
        if ($password && !$user->getForceNewPassword() && $user->getId()) {
            // check whether password was used before
            $resource     = Mage::getResourceSingleton('enterprise_pci/admin_user');
            $passwordHash = Mage::helper('core')->getHash($password, false);
            foreach ($resource->getOldPasswords($user,12) as $oldPasswordHash) {
                if ($passwordHash === $oldPasswordHash) {
                    Mage::throwException(Mage::helper('enterprise_pci')->__('Esta contraseña ha sido utilizada con anterioridad, use una nueva.'));//This password was used earlier, try another one.
                }
            }
        }
    }

    
}

