<?php

$installer = $this;
$installer ->startSetup();

		$installer ->run(
			"ALTER TABLE admin_user ADD login_attempts INT NOT NULL DEFAULT '0';
             ALTER TABLE admin_user ADD last_login_attempt TIMESTAMP NULL;
             ALTER TABLE admin_user ADD password_history TEXT;
             ALTER TABLE admin_user ADD last_password_change TIMESTAMP NULL;
             ALTER TABLE admin_user ADD change_password_required BOOLEAN NOT NULL DEFAULT '1';
		");

$installer->endSetup();

