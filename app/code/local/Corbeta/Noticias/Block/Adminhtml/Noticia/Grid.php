<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia admin grid block
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Adminhtml_Noticia_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('noticiaGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('corbeta_noticias/noticia')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'titulo',
            array(
                'header'    => Mage::helper('corbeta_noticias')->__('Titulo'),
                'align'     => 'left',
                'index'     => 'titulo',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('corbeta_noticias')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('corbeta_noticias')->__('Enabled'),
                    '0' => Mage::helper('corbeta_noticias')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'fecha',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Fecha'),
                'index'  => 'fecha',
                'type'=> 'date',

            )
        );
        $this->addColumn(
            'principal',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Principal'),
                'index'  => 'principal',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_noticias')->__('Yes'),
                    '0' => Mage::helper('corbeta_noticias')->__('No'),
                )

            )
        );
        $this->addColumn(
            'secundaria',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Secundaria'),
                'index'  => 'secundaria',
                'type'    => 'options',
                    'options'    => array(
                    '1' => Mage::helper('corbeta_noticias')->__('Yes'),
                    '0' => Mage::helper('corbeta_noticias')->__('No'),
                )

            )
        );
        $this->addColumn(
            'orden',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Orden'),
                'index'  => 'orden',
                'type'=> 'number',

            )
        );
        $this->addColumn(
            'url_key',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('URL key'),
                'index'  => 'url_key',
            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('corbeta_noticias')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('corbeta_noticias')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('corbeta_noticias')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('corbeta_noticias')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('corbeta_noticias')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('corbeta_noticias')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('corbeta_noticias')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('noticia');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('corbeta_noticias')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('corbeta_noticias')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('corbeta_noticias')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('corbeta_noticias')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('corbeta_noticias')->__('Enabled'),
                            '0' => Mage::helper('corbeta_noticias')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'principal',
            array(
                'label'      => Mage::helper('corbeta_noticias')->__('Change Principal'),
                'url'        => $this->getUrl('*/*/massPrincipal', array('_current'=>true)),
                'additional' => array(
                    'flag_principal' => array(
                        'name'   => 'flag_principal',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('corbeta_noticias')->__('Principal'),
                        'values' => array(
                                '1' => Mage::helper('corbeta_noticias')->__('Yes'),
                                '0' => Mage::helper('corbeta_noticias')->__('No'),
                            )

                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'secundaria',
            array(
                'label'      => Mage::helper('corbeta_noticias')->__('Change Secundaria'),
                'url'        => $this->getUrl('*/*/massSecundaria', array('_current'=>true)),
                'additional' => array(
                    'flag_secundaria' => array(
                        'name'   => 'flag_secundaria',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('corbeta_noticias')->__('Secundaria'),
                        'values' => array(
                                '1' => Mage::helper('corbeta_noticias')->__('Yes'),
                                '0' => Mage::helper('corbeta_noticias')->__('No'),
                            )

                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Corbeta_Noticias_Model_Noticia
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
