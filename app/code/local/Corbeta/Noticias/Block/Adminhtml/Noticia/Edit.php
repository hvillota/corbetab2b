<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia admin edit form
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Adminhtml_Noticia_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'corbeta_noticias';
        $this->_controller = 'adminhtml_noticia';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('corbeta_noticias')->__('Save Noticia')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('corbeta_noticias')->__('Delete Noticia')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('corbeta_noticias')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_noticia') && Mage::registry('current_noticia')->getId()) {
            return Mage::helper('corbeta_noticias')->__(
                "Edit Noticia '%s'",
                $this->escapeHtml(Mage::registry('current_noticia')->getTitulo())
            );
        } else {
            return Mage::helper('corbeta_noticias')->__('Add Noticia');
        }
    }
}
