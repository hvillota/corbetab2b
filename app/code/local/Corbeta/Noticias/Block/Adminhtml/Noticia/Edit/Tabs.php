<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia admin edit tabs
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Adminhtml_Noticia_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('noticia_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('corbeta_noticias')->__('Noticia'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_noticia',
            array(
                'label'   => Mage::helper('corbeta_noticias')->__('Noticia'),
                'title'   => Mage::helper('corbeta_noticias')->__('Noticia'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_noticias/adminhtml_noticia_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_noticia',
            array(
                'label'   => Mage::helper('corbeta_noticias')->__('Meta'),
                'title'   => Mage::helper('corbeta_noticias')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'corbeta_noticias/adminhtml_noticia_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve noticia entity
     *
     * @access public
     * @return Corbeta_Noticias_Model_Noticia
     * @author Ultimate Module Creator
     */
    public function getNoticia()
    {
        return Mage::registry('current_noticia');
    }
}
