<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia edit form tab
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Adminhtml_Noticia_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Adminhtml_Noticia_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('noticia_');
        $form->setFieldNameSuffix('noticia');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'noticia_form',
            array('legend' => Mage::helper('corbeta_noticias')->__('Noticia'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('corbeta_noticias/adminhtml_noticia_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $fieldset->addField(
            'titulo',
            'text',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Titulo'),
                'name'  => 'titulo',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'descripcion',
            'editor',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Descripcion'),
                'name'  => 'descripcion',
            'config' => $wysiwygConfig,
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'boton_titulo',
            'text',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Titulo Boton'),
                'name'  => 'boton_titulo',

           )
        );

        $fieldset->addField(
            'link',
            'text',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Link'),
                'name'  => 'link',
				

           )
        );

        $fieldset->addField(
            'fecha',
            'date',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Fecha'),
                'name'  => 'fecha',
				'required'  => true,

            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format'  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
           )
        );

        $fieldset->addField(
            'imagen',
            'image',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Imagen'),
                'name'  => 'imagen',

           )
        );

        $fieldset->addField(
            'principal',
            'select',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Principal'),
                'name'  => 'principal',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_noticias')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_noticias')->__('No'),
                ),
            ),
           )
        );

        $fieldset->addField(
            'secundaria',
            'select',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Secundaria'),
                'name'  => 'secundaria',

            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('corbeta_noticias')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('corbeta_noticias')->__('No'),
                ),
            ),
           )
        );

        $fieldset->addField(
            'orden',
            'text',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Orden'),
                'name'  => 'orden',

           )
        );
        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => Mage::helper('corbeta_noticias')->__('Url key'),
                'name'  => 'url_key',
                'note'  => Mage::helper('corbeta_noticias')->__('Relative to Website Base URL')
            )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('corbeta_noticias')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('corbeta_noticias')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('corbeta_noticias')->__('Disabled'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_noticia')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getNoticiaData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getNoticiaData());
            Mage::getSingleton('adminhtml/session')->setNoticiaData(null);
        } elseif (Mage::registry('current_noticia')) {
            $formValues = array_merge($formValues, Mage::registry('current_noticia')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
