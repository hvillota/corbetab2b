<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia view block
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Noticia_View extends Mage_Core_Block_Template
{
    /**
     * get the current noticia
     *
     * @access public
     * @return mixed (Corbeta_Noticias_Model_Noticia|null)
     * @author Ultimate Module Creator
     */
    public function getCurrentNoticia()
    {
        return Mage::registry('current_noticia');
    }
}
