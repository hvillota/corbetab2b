<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia list block
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author Ultimate Module Creator
 */
class Corbeta_Noticias_Block_Noticia_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $noticias = Mage::getResourceModel('corbeta_noticias/noticia_collection')
                         ->addFieldToFilter('status', 1)
                         ->addFieldToFilter('principal', 0)
                         ->addFieldToFilter('secundaria', 0);
						
						 
        $noticias->setOrder('fecha', 'desc');
        $this->setNoticias($noticias);
		
		$principal = Mage::getResourceModel('corbeta_noticias/noticia_collection')
                         ->addFieldToFilter('status', 1)
                         ->addFieldToFilter('principal', 1);
        
        $this->setPrincipal($principal);
		
		$secundaria = Mage::getResourceModel('corbeta_noticias/noticia_collection')
                         ->addFieldToFilter('status', 1)
                         ->addFieldToFilter('secundaria', 1);
        $secundaria->setOrder('orden', 'asc');
        $this->setSecundaria($secundaria);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Corbeta_Noticias_Block_Noticia_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'corbeta_noticias.noticia.html.pager'
        )
        ->setCollection($this->getNoticias());
        $this->setChild('pager', $pager);
		$pager->setLimit(6);
		$this->getNoticias()->setPageSize(6);
		if($pager->isFirstPage()){
			$this->getPrincipal()->load();
			$this->getPrincipal()->setPageSize(1);
			$this->getSecundaria()->load();
			$this->getSecundaria()->setPageSize(2);
			
		}
        $this->getNoticias()->load();
        
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
