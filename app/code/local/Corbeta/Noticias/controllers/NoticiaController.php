<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia front contrller
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_NoticiaController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('corbeta_noticias/noticia')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('corbeta_noticias')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'noticias',
                    array(
                        'label' => Mage::helper('corbeta_noticias')->__('Noticias'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('corbeta_noticias/noticia')->getNoticiasUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('corbeta_noticias/noticia/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('corbeta_noticias/noticia/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('corbeta_noticias/noticia/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Noticia
     *
     * @access protected
     * @return Corbeta_Noticias_Model_Noticia
     * @author Ultimate Module Creator
     */
    protected function _initNoticia()
    {
        $noticiaId   = $this->getRequest()->getParam('id', 0);
        $noticia     = Mage::getModel('corbeta_noticias/noticia')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($noticiaId);
        if (!$noticia->getId()) {
            return false;
        } elseif (!$noticia->getStatus()) {
            return false;
        }
        return $noticia;
    }

    /**
     * view noticia action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $noticia = $this->_initNoticia();
        if (!$noticia) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_noticia', $noticia);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('noticias-noticia noticias-noticia' . $noticia->getId());
        }
        if (Mage::helper('corbeta_noticias/noticia')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('corbeta_noticias')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'noticias',
                    array(
                        'label' => Mage::helper('corbeta_noticias')->__('Noticias'),
                        'link'  => Mage::helper('corbeta_noticias/noticia')->getNoticiasUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'noticia',
                    array(
                        'label' => $noticia->getTitulo(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $noticia->getNoticiaUrl());
        }
        if ($headBlock) {
            if ($noticia->getMetaTitle()) {
                $headBlock->setTitle($noticia->getMetaTitle());
            } else {
                $headBlock->setTitle($noticia->getTitulo());
            }
            $headBlock->setKeywords($noticia->getMetaKeywords());
            $headBlock->setDescription($noticia->getMetaDescription());
        }
        $this->renderLayout();
    }
}
