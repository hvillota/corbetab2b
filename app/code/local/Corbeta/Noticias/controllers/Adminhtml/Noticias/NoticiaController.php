<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia admin controller
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Adminhtml_Noticias_NoticiaController extends Corbeta_Noticias_Controller_Adminhtml_Noticias
{
    /**
     * init the noticia
     *
     * @access protected
     * @return Corbeta_Noticias_Model_Noticia
     */
    protected function _initNoticia()
    {
        $noticiaId  = (int) $this->getRequest()->getParam('id');
        $noticia    = Mage::getModel('corbeta_noticias/noticia');
        if ($noticiaId) {
            $noticia->load($noticiaId);
        }
        Mage::register('current_noticia', $noticia);
        return $noticia;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_noticias')->__('Noticias'))
             ->_title(Mage::helper('corbeta_noticias')->__('Noticias'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit noticia - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $noticiaId    = $this->getRequest()->getParam('id');
        $noticia      = $this->_initNoticia();
        if ($noticiaId && !$noticia->getId()) {
            $this->_getSession()->addError(
                Mage::helper('corbeta_noticias')->__('This noticia no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getNoticiaData(true);
        if (!empty($data)) {
            $noticia->setData($data);
        }
        Mage::register('noticia_data', $noticia);
        $this->loadLayout();
        $this->_title(Mage::helper('corbeta_noticias')->__('Noticias'))
             ->_title(Mage::helper('corbeta_noticias')->__('Noticias'));
        if ($noticia->getId()) {
            $this->_title($noticia->getTitulo());
        } else {
            $this->_title(Mage::helper('corbeta_noticias')->__('Add noticia'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new noticia action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save noticia - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('noticia')) {
            try {
                $data = $this->_filterDates($data, array('fecha'));
                $noticia = $this->_initNoticia();
                $noticia->addData($data);
                $imagenName = $this->_uploadAndGetName(
                    'imagen',
                    Mage::helper('corbeta_noticias/noticia_image')->getImageBaseDir(),
                    $data
                );
                $noticia->setData('imagen', $imagenName);
                $noticia->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_noticias')->__('Noticia was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $noticia->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['imagen']['value'])) {
                    $data['imagen'] = $data['imagen']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setNoticiaData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['imagen']['value'])) {
                    $data['imagen'] = $data['imagen']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was a problem saving the noticia.')
                );
                Mage::getSingleton('adminhtml/session')->setNoticiaData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_noticias')->__('Unable to find noticia to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete noticia - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $noticia = Mage::getModel('corbeta_noticias/noticia');
                $noticia->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_noticias')->__('Noticia was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was an error deleting noticia.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('corbeta_noticias')->__('Could not find noticia to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete noticia - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $noticiaIds = $this->getRequest()->getParam('noticia');
        if (!is_array($noticiaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_noticias')->__('Please select noticias to delete.')
            );
        } else {
            try {
                foreach ($noticiaIds as $noticiaId) {
                    $noticia = Mage::getModel('corbeta_noticias/noticia');
                    $noticia->setId($noticiaId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('corbeta_noticias')->__('Total of %d noticias were successfully deleted.', count($noticiaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was an error deleting noticias.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $noticiaIds = $this->getRequest()->getParam('noticia');
        if (!is_array($noticiaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_noticias')->__('Please select noticias.')
            );
        } else {
            try {
                foreach ($noticiaIds as $noticiaId) {
                $noticia = Mage::getSingleton('corbeta_noticias/noticia')->load($noticiaId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d noticias were successfully updated.', count($noticiaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was an error updating noticias.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Principal change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massPrincipalAction()
    {
        $noticiaIds = $this->getRequest()->getParam('noticia');
        if (!is_array($noticiaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_noticias')->__('Please select noticias.')
            );
        } else {
            try {
                foreach ($noticiaIds as $noticiaId) {
                $noticia = Mage::getSingleton('corbeta_noticias/noticia')->load($noticiaId)
                    ->setPrincipal($this->getRequest()->getParam('flag_principal'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d noticias were successfully updated.', count($noticiaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was an error updating noticias.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Secundaria change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massSecundariaAction()
    {
        $noticiaIds = $this->getRequest()->getParam('noticia');
        if (!is_array($noticiaIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('corbeta_noticias')->__('Please select noticias.')
            );
        } else {
            try {
                foreach ($noticiaIds as $noticiaId) {
                $noticia = Mage::getSingleton('corbeta_noticias/noticia')->load($noticiaId)
                    ->setSecundaria($this->getRequest()->getParam('flag_secundaria'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d noticias were successfully updated.', count($noticiaIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('corbeta_noticias')->__('There was an error updating noticias.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'noticia.csv';
        $content    = $this->getLayout()->createBlock('corbeta_noticias/adminhtml_noticia_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'noticia.xls';
        $content    = $this->getLayout()->createBlock('corbeta_noticias/adminhtml_noticia_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'noticia.xml';
        $content    = $this->getLayout()->createBlock('corbeta_noticias/adminhtml_noticia_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/corbeta_noticias/noticia');
    }
}
