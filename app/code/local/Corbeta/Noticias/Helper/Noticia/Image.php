<?php 
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticia image helper
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Helper_Noticia_Image extends Corbeta_Noticias_Helper_Image_Abstract
{
    /**
     * image placeholder
     * @var string
     */
    protected $_placeholder = 'images/placeholder/noticia.jpg';
    /**
     * image subdir
     * @var string
     */
    protected $_subdir      = 'noticia';
}
