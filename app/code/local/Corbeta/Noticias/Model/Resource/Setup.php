<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticias setup
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
class Corbeta_Noticias_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
}
