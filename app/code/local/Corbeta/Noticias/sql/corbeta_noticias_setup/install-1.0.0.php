<?php
/**
 * Corbeta_Noticias extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Corbeta
 * @package        Corbeta_Noticias
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Noticias module install script
 *
 * @category    Corbeta
 * @package     Corbeta_Noticias
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('corbeta_noticias/noticia'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Noticia ID'
    )
    ->addColumn(
        'titulo',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Titulo'
    )
    ->addColumn(
        'descripcion',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(
            'nullable'  => false,
        ),
        'Descripcion'
    )
    ->addColumn(
        'boton_titulo',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Titulo Boton'
    )
    ->addColumn(
        'link',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Link'
    )
    ->addColumn(
        'fecha',
        Varien_Db_Ddl_Table::TYPE_DATETIME, 255,
        array(),
        'Fecha'
    )
    ->addColumn(
        'imagen',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Imagen'
    )
    ->addColumn(
        'principal',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Principal'
    )
    ->addColumn(
        'secundaria',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Secundaria'
    )
    ->addColumn(
        'orden',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Orden'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'url_key',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'URL key'
    )
    ->addColumn(
        'meta_title',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Meta title'
    )
    ->addColumn(
        'meta_keywords',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Meta keywords'
    )
    ->addColumn(
        'meta_description',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Meta description'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Noticia Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Noticia Creation Time'
    ) 
    ->setComment('Noticia Table');
$this->getConnection()->createTable($table);
$this->endSetup();
