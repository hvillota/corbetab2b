var popupcontainer;
function getproductpopup(url){
	if($(popupcontainer) == undefined){
		popupcontainer = document.createElement("div");
		popupcontainer.id = "productpopup";
		popupcontainer.hide();
		$(document.body).appendChild(popupcontainer);
		
		popupwraper = document.createElement("div");
		popupwraper.id = "popupwraper";
		
		$(popupcontainer).appendChild(popupwraper);
		
		popupcloser = document.createElement("div");
		popupcloser.id = "productcloser";
		$(popupcloser).update("X CERRAR");
		popupcontent = document.createElement("div");
		popupcontent.id = "productcontent";
		
		$(popupwraper).appendChild(popupcontent);
		$(popupwraper).appendChild(popupcloser);
		
		Event.observe(popupcloser,'click',closeproductpopup.bind(this));
		Event.observe(document,'keyup',identifyEscKeyPressedEventProduct.bind(this));
	}
	
	//new Ajax.Updater(popupcontent, url, {onSuccess:openproductpopup(this)});
	new Ajax.Request(
		url, {
		onSuccess: function(response){
			popupcontent.update(response.responseText);
			popupcontainer.show();
			}
		} 
	);
  return false;
}
function openproductpopup(){
	popupcontainer.show();
}
function closeproductpopup(){
	popupcontainer.hide();
}
function identifyEscKeyPressedEventProduct(keyEvent)
{               
	var pressedKeyValue = keyEvent.keyCode;
	if(pressedKeyValue == 27)
	{
		closeproductpopup();
	}
}
function notallowedproductpopup(id){
	$$("."+id).each(function(element){
	  element.update('Este producto no est&aacute; disponible para tu catálogo');
	});

	
}