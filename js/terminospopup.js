var terminospopupcontainer;
function getterminospopup(){
	
	if($(terminospopupcontainer) == undefined){
		terminospopupcontainer = document.createElement("div");
		terminospopupcontainer.id = "terminospopup";
		terminospopupcontainer.addClassName('mylightbox');
		terminospopupcontainer.hide();
		$(document.body).appendChild(terminospopupcontainer);
		
		popupwraper = document.createElement("div");
		
		popupwraper.addClassName("popupwraper");
		
		$(terminospopupcontainer).appendChild(popupwraper);
		
		popupcloser = document.createElement("div");
		
		popupcloser.addClassName("popupcloser");
		$(popupcloser).update("x cerrar");
		popupcontent = document.createElement("div");
		
		popupcontent.addClassName("popupcontent");
		
		$(popupwraper).appendChild(popupcontent);
		$(popupwraper).appendChild(popupcloser);
		
		Event.observe(popupcloser,'click',closeterminospopup.bind(this));
		Event.observe(document,'keyup',identifyEscKeyPressedEvent.bind(this));
		content = $('checkout-agreements');
		
		
		
		popupcontent.update(content.down('.agreement-content').innerHTML);
	}
	terminospopupcontainer.show();
	
  return false;
}
function openterminospopup(){
	terminospopupcontainer.show();
}
function closeterminospopup(){
	terminospopupcontainer.hide();
}
function identifyEscKeyPressedEvent(keyEvent)
{               
	var pressedKeyValue = keyEvent.keyCode;
	if(pressedKeyValue == 27)
	{
		closeterminospopup();
	}
}