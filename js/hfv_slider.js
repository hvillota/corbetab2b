var _banner_timmer;
var _banner_i = 0;
var _banner_n;
var _banner;
var nav_box;
document.observe("dom:loaded", function() {
	var _banner_box = $('slider-home');
	_banner_box.addClassName("slider-home");
	nav_box = document.createElement("DIV");
	nav_box.addClassName("banner-nav-box");
	_banner_box.appendChild(nav_box);
	
	nav_left = document.createElement("DIV");
	nav_left.addClassName("banner-nav-left");
	Event.observe(nav_left,'click',navleft);
	_banner_box.appendChild(nav_left);
	nav_right = document.createElement("DIV");
	nav_right.addClassName("banner-nav-right");
	Event.observe(nav_right,'click',navright);
	_banner_box.appendChild(nav_right);
	
	_banner = $$('#slider-home .banner-slider');
	_banner_n = _banner.length;
	_banner_id = 0;
	_banner.each(function(element){
		 Element.hide(element);
		 nav = document.createElement("DIV");
		 nav.addClassName("banner-nav");
		 nav.id = "nav-"+_banner_id;
		 nav.setAttribute('name',_banner_id);
		 Event.observe(nav,'click',navclick);
		 _banner_id++;
		 nav_box.appendChild(nav);
	});
	cambiarBanner(0);
	$('nav-0').addClassName('banner-nav-off');
});
	
function navleft(event){cambiarBanner(_banner_i-1);}
function navright(event){cambiarBanner(_banner_i+1);}
function navclick(event){
  var element = event.element();
  cambiarBanner(element.getAttribute('name'));
}

function cambiarBanner(index){
	clearTimeout(_banner_timmer);
	if(index<0)index=_banner_n-1;
	if(index>=_banner_n)index=0;
	image = _banner[index];
	Effect.Appear(image);
	if(index!=_banner_i){
		$('nav-'+_banner_i).removeClassName('banner-nav-off');
		last_image = _banner[_banner_i];
		Effect.Fade(last_image);
		_banner_i = index;
		$('nav-'+_banner_i).addClassName('banner-nav-off');
	}	
	_banner_timmer = setTimeout('cambiarBanner('+(index+1)+')', 4000);
}

