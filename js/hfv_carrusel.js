 
var CarruselStaticBlock = Class.create();
CarruselStaticBlock.prototype = {
    initialize: function(id,speed,maxitems,width_item,margen,container){
        this._carrusel_id= id;
	
	this._maxitems= maxitems;
	this._margen= margen;
	this._speed = speed;
	this._width_item = width_item;
	
	if (typeof container != 'undefined') {
		this._container = $(container);
	}else{
		this._container = document.viewport;
	}
	
	this._carrusel_box = $(this._carrusel_id);
	this._carrusel_timmer;
	this._carrusel_i = 0;
	this._carrusel_n;
	this._carrusel;
	this._contenedor = document.createElement("div");
	this._contenedor.addClassName("carrusel-content");
	
	this._carrusel_box.appendChild(this._contenedor);
	this._current_position=0;
	
	//this.log = document.createElement("DIV");
	//this._carrusel_box.appendChild(this.log);
	
	this.nav_left = document.createElement("div");
	this.nav_left.addClassName("carrusel-nav-left");
	Event.observe(this.nav_left,'click',this.navleft.bind(this));
	this._carrusel_box.appendChild(this.nav_left);
	
	this.nav_right = document.createElement("div");
	this.nav_right.addClassName("carrusel-nav-right");
	Event.observe(this.nav_right,'click',this.navright.bind(this));
	this._carrusel_box.appendChild(this.nav_right);
	
	this._carrusel = this._carrusel_box.select('.marca','p>a', 'p>img','>a','>img');
	this._carrusel_n = this._carrusel.length;
	this._carrusel_id = 0;
	
	this._width_contenedor=this._width_item * this._carrusel_n;
	this._contenedor.setStyle({width:this._width_contenedor+"px"});
	this.left=0;
	this._carrusel.each(function(element){
	  var item = document.createElement("div");
	  item.setStyle({width:this._width_item+"px"});
	  item.setStyle({left:this.left+"px"});
	  item.addClassName('carruselsb-item');
	  item.appendChild(element);
	  this._contenedor.appendChild(item);
	  this.left+=this._width_item;
	}.bind(this));
	this.redraw();
	this.nav_left.hide();
	if(this._carrusel_n<=this._maxitems){
	  this.nav_right.hide();
	}else{
	  this.cambiar();  
	}
		Event.observe(window, "resize", function() {
			this.redraw();
			if(this._carrusel_n<=this._maxitems){
				this.left=0;
				this._contenedor.childElements().each(function(element){
					element.setStyle({left:this.left+"px"}); 
					this.left+=this._width_item;
				}.bind(this));
				this._current_position=0;
				this._contenedor.setStyle({left:this._current_position+"px"});
				clearTimeout(this._carrusel_timmer);
			}else{
			  this.cambiar();  
			}
		}.bind(this));
		
    },
    
    navleft: function(event){
      this._current_position+=this._width_item;
      //this.log.innerHTML=this._current_position;
      new Effect.Move(this._contenedor,{ x:this._current_position, y: 0 ,mode: 'absolute'});
      if(this._current_position==0){
		this.nav_left.hide();//ok
		this.nav_right.show();
      }
      if(this._current_position>-(this._width_contenedor-(this._width_item*this._maxitems))){
		this.nav_right.show();
      }
      
      this.cambiar();
    },
    
    navrightbak: function(event){
      
      this._current_position-=this._width_item;
      //this.log.innerHTML=this._current_position;
      if(this._current_position<-(this._width_contenedor-(this._width_item*this._maxitems))){
	this._current_position=0;
	this.nav_left.hide();
	this.nav_right.show();
      }
      new Effect.Move(this._contenedor,{ x: this._current_position, y: 0 ,mode: 'absolute'});
      
      if(this._current_position<0){
	this.nav_left.show();//ok
      }
      if(this._current_position==-(this._width_contenedor-(this._width_item*this._maxitems))){
	this.nav_right.hide();//ok
      }
      this.cambiar();
    },
    
	navright: function(event){
      
	  if(this._current_position<0){
		firstelement = this._contenedor.firstDescendant();
		this._contenedor.appendChild(firstelement);
		firstelement.setStyle({left:this.left+"px"});
		this.left+=this._width_item;
	  }
      this._current_position-=this._width_item;
      //this.log.innerHTML=this._current_position;
	  
	  this._width_contenedor+=this._width_item;
	  this._contenedor.setStyle({width:this._width_contenedor+"px"});
	  
      new Effect.Move(this._contenedor,{ x: this._current_position, y: 0 ,mode: 'absolute'});
	  
      if(this._current_position<0){
	this.nav_left.show();//ok
      }
      if(this._current_position==-(this._width_contenedor-(this._width_item*this._maxitems))){
	this.nav_right.hide();//ok
      }
      this.cambiar();
    },
    cambiar: function(){
	clearTimeout(this._carrusel_timmer);
	this._carrusel_timmer = setTimeout(function(){this.navright()}.bind(this), this._speed);
    },
	
	redraw:function(){	
		
		var _n = (this._container.getWidth()-this._margen)/this._width_item;
		var _n_var = _n+'';	
		//this.log.innerHTML=_n_var;
		if(_n_var.indexOf('.')>0){
			this._maxitems = _n_var.substring(0,_n_var.indexOf('.'));
		}
		
		if(this._maxitems>this._carrusel_n){
			this._maxitems=this._carrusel_n;
		}
		var new_width = this._width_item * this._maxitems;
		this._carrusel_box.setStyle({width:new_width+"px"});
    }
}
 
