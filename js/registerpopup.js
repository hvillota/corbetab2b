var registerpopupcontainer;
function getregisterpopup(){
	url = '/registerpopup/';
	
	if($(registerpopupcontainer) == undefined){
		
		registerpopupcontainer = document.createElement("div");
		registerpopupcontainer.id = "registerpopup";
		registerpopupcontainer.hide();
		$("logodown").appendChild(registerpopupcontainer);
		
		popupwraper = document.createElement("div");
		popupwraper.id = "popupwraper";
		
		$(registerpopupcontainer).appendChild(popupwraper);
		
		popupcloser = document.createElement("div");
		popupcloser.id = "popupcloser";
		$(popupcloser).update("X CERRAR");
		popupcontent = document.createElement("div");
		popupcontent.id = "popupcontent";
		
		$(popupwraper).appendChild(popupcontent);
		$(popupwraper).appendChild(popupcloser);
		
		Event.observe(popupcloser,'click',closeregisterpopup.bind(this));
		Event.observe(document,'keyup',identifyEscKeyPressedEvent.bind(this));
	
		//new Ajax.Updater(popupcontent, url, {onSuccess:openregisterpopup(this)});
		new Ajax.Request(
			url, {
			onSuccess: function(response){
				popupcontent.update(response.responseText);
				registerpopupcontainer.show();
				}
			} 
		);
		
	}else{
		$(registerpopupcontainer).toggle();
	}
  return false;
}
function openregisterpopup(){
	registerpopupcontainer.show();
}
function closeregisterpopup(){
	registerpopupcontainer.hide();
}
function identifyEscKeyPressedEvent(keyEvent)
{               
	var pressedKeyValue = keyEvent.keyCode;
	if(pressedKeyValue == 27)
	{
		closeregisterpopup();
	}
}